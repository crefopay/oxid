<?php

namespace Crefopay\Payments\Extend\Model;

use OxidEsales\Eshop\Core\Registry;

class CrefopayUser extends CrefopayUser_parent
{
    protected function onLogin($sUser, $sPassword)
    {
        parent::onLogin($sUser, $sPassword);

        // Reset allowed payment-methods and risk class after login:
        $oSession = Registry::getSession();
        $oSession->deleteVariable('crefopayAllowedPaymentMethods');
        $oSession->deleteVariable('crefopayRiskClass');
    }
}
