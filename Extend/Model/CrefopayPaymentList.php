<?php

namespace Crefopay\Payments\Extend\Model;

use Crefopay\Payments\Core\CrefopayHelpers;

class CrefopayPaymentList extends CrefopayPaymentList_parent
{
    protected function _getFilterSelect($sShipSetId, $dPrice, $oUser)
    {
        $sql = parent::_getFilterSelect($sShipSetId, $dPrice, $oUser);

        if (!CrefopayHelpers::allowOtherPaymentsEnabled()) {
            // Filter all payment types without crefopay-paymenthod-id:

            $sTable = getViewName('oxpayments');
            $repThis = "{$sTable}.oxactive='1'";
            $withThis = $repThis . ' AND LENGTH(CREFOPAY_PAYMENTMETHODID)>0';
            $sql = str_replace($repThis, $withThis, $sql);
        }

        return $sql;
    }
}
