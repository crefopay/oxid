<?php

namespace Crefopay\Payments\Extend\Model;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Core\Registry;

class CrefopayPaymentGateway extends CrefopayPaymentGateway_parent
{
    /**
     * @param float $dAmount
     * @param \OxidEsales\Eshop\Application\Model\Order|CrefopayOrder $oOrder
     * @return bool
     */
    public function executePayment($dAmount, &$oOrder)
    {
        /** @var CrefopayPayment|Payment $oPayment */
        $oPayment = oxNew(Payment::class);
        if ($oPayment->load($oOrder->oxorder__oxpaymenttype->value)) {
            if ($oPayment->isCrefopayPaymentMethod()) {
                return $this->executeCrefopayPayment($dAmount, $oOrder);
            }
        }

        return parent::executePayment($dAmount, $oOrder);
    }

    /**
     * @param $dAmount
     * @param $oOrder CrefopayOrder|Order
     * @return bool|mixed|null
     * @throws \Exception
     * @noinspection PhpParameterByRefIsNotUsedAsReferenceInspection
     */
    protected function executeCrefopayPayment($dAmount, &$oOrder)
    {
        $oCrefopayTransaction = CrefopayTransaction::getActiveTransaction();

        if (CrefopayHelpers::getConfigBool('CrefoPayPreAssignOrderNr')) {
            $oOrder->crefopayAssignOrderNr();
            $oOrder->crefopayAssignOrderDate();
            $oOrder->save();
        }

        // Force an order-id, if none set yet:
        if (!$oOrder->getId()) {
            $oOrder->setId();
        }

        $oCrefopayTransaction->setOxidOrderId($oOrder->getId());
        $oCrefopayTransaction->save();

        $oxordernr = $oOrder->getFieldData('OXORDERNR');
        if ($oxordernr) {
            $oCrefopayTransaction->setMerchantReference($oOrder->oxorder__oxordernr->value);
        }

        if ($oOrder->crefopayForceOrderCreation) {
            CrefopayHelpers::getLogger()->debug('executeCrefopayPayment() returns true due to forcing');
            return true;
        }

        // Serialize order to transaction, in case we need to restore it later:
        $oSession = Registry::getSession();
        $oCrefopayTransaction->setObjectsToSerialize($oOrder, $oSession->getBasket(), $oSession->getUser());
        $oCrefopayTransaction->save();
        $reserveResult = $oCrefopayTransaction->reserve();
        if (is_string($reserveResult)) {
            // Redirect to external portal required:
            CrefopayHelpers::getLogger()->debug('executeCrefopayPayment() redirects to: ' . $reserveResult);

            // Remove temporary order:
            $oOrder->delete();

            // Mark transaction as redirect to payment provider:
            $oCrefopayTransaction->setRedirectStatus(CrefopayTransaction::REDIRECTSTATUS_REDIRECTED);
            $oCrefopayTransaction->save();

            $sRedirectUrl = $reserveResult;
            Registry::getUtils()->redirect($sRedirectUrl, false);
            exit;
        }

        $this->_iLastErrorNo = Order::ORDER_STATE_PAYMENTERROR;

        CrefopayHelpers::getLogger()->debug('executeCrefopayPayment() result: ' . print_r($reserveResult, true));

        return $reserveResult;
    }
}
