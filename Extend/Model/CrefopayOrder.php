<?php

namespace Crefopay\Payments\Extend\Model;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Core\CrefopayLogger;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;

class CrefopayOrder extends CrefopayOrder_parent
{
    public $sCrefopayLog = '';

    private $bIsCrefopayCallbackFinalize = false;

    /**
     * This is set to true if the payment-state should not get checked
     * @var bool
     */
    public $crefopayForceOrderCreation = false;

    /**
     * Validates the delivery address
     *
     * @param \OxidEsales\Eshop\Application\Model\User $oUser
     */
    public function validateDeliveryAddress($oUser)
    {
        if ($this->bIsCrefopayCallbackFinalize) {
            // Was already checked in initial execute()
            // This is called from callback and does not have the sDeliveryAddressMD5-parameter
            return;
        }

        return parent::validateDeliveryAddress($oUser);
    }

    public function crefopaySetFinalizeCalledFromCallback($bIsCrefopayCallbackFinalize)
    {
        $this->bIsCrefopayCallbackFinalize = $bIsCrefopayCallbackFinalize;
        $this->crefopayAssignOrderDate();
    }

    public function crefopayGetTransaction()
    {
        return CrefopayTransaction::getActiveTransaction();
    }

    public function crefopayGetOxidPayment()
    {
        $sPaymentId = $this->oxorder__oxpaymenttype->value;
        $oPayment = oxNew(Payment::class);
        $oPayment->load($sPaymentId);
        return $oPayment;
    }

    /**
     * Sets oxordernr if not set yet
     */
    public function crefopayAssignOrderNr()
    {
        $ordernr = '';
        if ($this->oxorder__oxordernr) {
            $ordernr = trim($this->oxorder__oxordernr->value, "' \"");
        }
        if (!$ordernr) {
            $this->_setNumber();
        }
    }

    /**
     * Sets oxorderdate if not set to a valid value yet
     * @return bool Returns if oxorderdate was invalid before calling this function
     */
    public function crefopayAssignOrderDate()
    {
        $bAssignDate = false;

        $oxorderdate = trim($this->oxorder__oxorderdate->value, "' \"");
        if (!$oxorderdate) {
            $bAssignDate = true;
        } elseif (stripos($oxorderdate, '0000-') === 0) {
            $bAssignDate = true;
        }

        if ($bAssignDate) {
            $this->oxorder__oxorderdate = new Field(date("Y-m-d H:i:s"));
        }

        return $bAssignDate;
    }

    public function finalizeOrder(\OxidEsales\Eshop\Application\Model\Basket $oBasket, $oUser, $blRecalculatingOrder = false)
    {
        CrefopayHelpers::getLogger()->debug('Entering finalizeOrder()');

        $this->setCrefopayCreateSource(null);

        $iFinalizeOrderResult = parent::finalizeOrder($oBasket, $oUser, $blRecalculatingOrder);
        CrefopayHelpers::getLogger()->debug('finalizeOrder() returned: ' . (int)$iFinalizeOrderResult);

        if (!$this->oxorder__oxordernr->value) {
            CrefopayHelpers::getLogger()->error('Order was created without oxordernr?');
            $sLoggingIdent = CrefopayHelpers::getLogger()->getLoggingIdent();

            $errmsg = "Order with ID '" . print_r($this->getId(), true) . "' was created without oxordernr\r\n";
            $errmsg .= "Crefopay-Transaction: " . print_r(Registry::getSession()->getVariable('crefopayOrderId'), true) . "\r\n";
            $errmsg .= "Crefopay-Transaction-Logfile-Ident: " . $sLoggingIdent . "\r\n";
            if ($iFinalizeOrderResult === self::ORDER_STATE_MAILINGERROR) {
                $errmsg .= "finalizeOrder() returned: ORDER_STATE_MAILINGERROR (0)\r\n";
            } elseif ($iFinalizeOrderResult === self::ORDER_STATE_OK) {
                $errmsg .= "finalizeOrder() returned: ORDER_STATE_OK (1)\r\n";
            } elseif ($iFinalizeOrderResult === self::ORDER_STATE_PAYMENTERROR) {
                $errmsg .= "finalizeOrder() returned: ORDER_STATE_PAYMENTERROR (2)\r\n";
            } elseif ($iFinalizeOrderResult === self::ORDER_STATE_ORDEREXISTS) {
                $errmsg .= "finalizeOrder() returned: ORDER_STATE_ORDEREXISTS (3)\r\n";
            } elseif ($iFinalizeOrderResult === self::ORDER_STATE_INVALIDDELIVERY) {
                $errmsg .= "finalizeOrder() returned: ORDER_STATE_INVALIDDELIVERY (4)\r\n";
            } elseif ($iFinalizeOrderResult === self::ORDER_STATE_INVALIDPAYMENT) {
                $errmsg .= "finalizeOrder() returned: ORDER_STATE_INVALIDPAYMENT (5)\r\n";
            } elseif ($iFinalizeOrderResult === self::ORDER_STATE_BELOWMINPRICE) {
                $errmsg .= "finalizeOrder() returned: ORDER_STATE_BELOWMINPRICE (8)\r\n";
            } else {
                $errmsg .= "finalizeOrder() returned unknown value: " . (int)$iFinalizeOrderResult . "\r\n";
            }
            $errmsg .= "\r\n";
            $errmsg .= "Order-Log:\r\n";
            $errmsg .= $this->sCrefopayLog;

            $oSpecialLogger = oxNew(CrefopayLogger::class);
            $oSpecialLogger->setLoggingIdent('without_oxordernr.log');
            $oSpecialLogger->error($errmsg);
        } else {
            // Set CrefoPay merchantReference:
            $cpTransaction = CrefopayTransaction::getTransactionByCrefopayOrderId(Registry::getSession()->getVariable('crefopayOrderId'));
            if (!empty($cpTransaction)) {
                if ($iFinalizeOrderResult=== \OxidEsales\EshopCommunity\Application\Model\Order::ORDER_STATE_OK)
                {
                    $cpTransaction->clearFieldsAfterOrderCreation();
                }
                $cpTransaction->setMerchantReference($this->oxorder__oxordernr->value);
            } else {
                CrefopayHelpers::getLogger()->error('Missing transaction data: merchantReference could not be set');
            }
        }

        return $iFinalizeOrderResult;
    }

    /**
     * Returns what the source of the order creation was
     * @return string
     */
    public function getCrefopayCreateSource()
    {
        return (string)$this->getFieldData('CP_CREATESOURCE');
    }

    /**
     * 'backend': Order was created from oxid-backend
     * 'MNS': Order was created because paid was received via MNS-Callback and created by the crefopay-cronjob
     * 'thankyou': Order was created when the customer returned from external page (like sofortüberweisung)
     * 'regular': Order was created when the customer finalized the order in frontend (without involvment of external payment page)
     * @param $createSource
     * @return void
     */
    public function setCrefopayCreateSource($createSource = null)
    {
        if (!$createSource) {
            // Use existing value if createSource is not set:
            $createSource = $this->getCrefopayCreateSource();
        }

        if (!$createSource) {
            // Use default value if still not set:
            $createSource = 'regular';
        }

        $createdbyuserid = '';
        $oCurrentUser = Registry::getSession()->getUser();
        if ($oCurrentUser) {
            $createdbyuserid = $oCurrentUser->getId();
        }

        $this->oxorder__cp_createsource = new Field($createSource);
        $this->oxorder__cp_createdbyuserid = new Field($createdbyuserid);
    }

    /**
     * Returns the id of the user who created the order
     * (might differ from the customer if the order was created from backend)
     * @return string
     */
    public function getCrefopayCreatedByUserId()
    {
        return $this->getFieldData('CP_CREATEDBYUSERID');
    }

    /**
     * Returns the user object (or false if not set) of the user who created the order
     * (might differ from the customer if the order was created from backend)
     * @return false|User
     */
    public function getCrefopayCreatedByUser()
    {
        $userId = $this->getCrefopayCreatedByUserId();
        if ($userId) {
            $oUser = oxNew(User::class);
            if ($oUser->load($userId)) {
                return $oUser;
            }
        }

        return false;
    }
}
