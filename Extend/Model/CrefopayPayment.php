<?php

namespace Crefopay\Payments\Extend\Model;

use Crefopay\Payments\Core\CrefopayHelpers;

class CrefopayPayment extends CrefopayPayment_parent
{
    /**
     * Returns the Crefopay Payment-Method-Identifier
     * see https://docs.crefopay.de/api/#paymentmethods
     */
    public function getCrefopayPaymentMethodId()
    {
        // crefopay_paymentmethodid is initialized by CrefopayEvents::onActivate()

        return (string)$this->oxpayments__crefopay_paymentmethodid->value;
    }

    /**
     * Returns true if this is a crefopay payment-method
     * @return bool
     */
    public function isCrefopayPaymentMethod()
    {
        // has this payment-object a crefopay-paymentmethod-id set?
        if ($this->getCrefopayPaymentMethodId()) {
            return true;
        }

        return false;
    }

    public function isCrefopayCreditCardLogoEnabled($logoIdentifier)
    {
        if ($logoIdentifier === 'cvv') {
            return CrefopayHelpers::getConfigBool('CrefoPayCvvLogo', true);
        }
        if ($logoIdentifier === 'mc') {
            return CrefopayHelpers::getConfigBool('CrefoPayMcLogo', true);
        }
        if ($logoIdentifier === 'visa') {
            return CrefopayHelpers::getConfigBool('CrefoPayVisaLogo', true);
        }

        return false;
    }

    public function getCrefopayPrepaidPeriod()
    {
        return (int)CrefopayHelpers::getConfigParam('CrefoPayPrepaidPeriod', 14);
    }

    public function getCrefopayBillPeriod()
    {
        return (int)CrefopayHelpers::getConfigParam('CrefoPayBillPeriod', 30);
    }
}
