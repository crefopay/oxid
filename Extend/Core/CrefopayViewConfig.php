<?php

namespace Crefopay\Payments\Extend\Core;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Models\CrefopayTransaction;
use Exception;
use OxidEsales\Eshop\Application\Model\Country;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\GenericImport\GenericImport;
use OxidEsales\Eshop\Core\Module\Module;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\Theme;
use Random\RandomException;
use RuntimeException;

class CrefopayViewConfig extends CrefopayViewConfig_parent
{
    public function getActiveCrefopayTransaction()
    {
        return CrefopayTransaction::getActiveTransaction();
    }

    /**
     * Converts idents like 'bankAccountHolder' to 'CREFOPAY_BANKACCOUNTHOLDER'
     * (Used in templates)
     * @param $rawIdent
     * @return string
     */
    public function crefopayGetTranslationIdent($rawIdent)
    {
        $ident = $rawIdent;
        if (stripos($ident, 'CREFOPAY_') !== 0) {
            $ident = 'CREFOPAY_' . $rawIdent;
        }

        $ident = Str::getStr()->strtoupper($ident);
        return trim($ident);
    }

    /**
     * slimmed down jQuery library
     * @return string
     * @throws \oxFileException
     */
    public function getCrefopaySlimLibraryUrl()
    {
        return $this->getModuleUrl(CrefopayHelpers::getModuleId(), '/out/src/js/crefopay_slim.js');
    }

    /**
     * Returns true if the themes jQuery should be used
     * @return bool
     */
    public function useCrefopayModuleJQuery()
    {
        return CrefopayHelpers::getConfigBool('CrefoPayUseModuleJQuery', false);
    }

    public function getCrefopayLibraryUrl()
    {
        return CrefopayHelpers::getCrefopayLibraryUrl('securefields');
    }

    public function getCrefopayTransaction()
    {
        return CrefopayTransaction::getActiveTransaction(true, true);
    }

    public function getCrefopaySecureFieldsConfigurationJS($sViewName)
    {
        $oLang = Registry::getLang();

        $rv = [];
        $rv['shopPublicKey'] = CrefopayHelpers::getShopPublicKey();

        $oTrans = CrefopayTransaction::getActiveTransaction(true, true);
        $oTrans->refreshTransaction();
        if ($oTrans->getCrefopayTransactionStatus() !== 'NEW') {
            $oTrans->createTransaction();
        }
        if ($oTrans->isSaved()) {
            $rv['orderID'] = $oTrans->getCrefopayOrderId();
        } else {
            $rv['orderID'] = '';
        }

        $configuration = [];
        $configuration['url'] = CrefopayHelpers::getEndpointUrl('securefields');
        $configuration['placeholders'] = [
            "accountHolder" => $oLang->translateString('CREFOPAY_PLACEHOLDER_ACCOUNTHOULDER'),
            "number" => "0000111122223333",
            "cvv" => "CVV"
        ];

        $rv['configuration'] = $configuration;

        // Pass translations for secure fields error messages:
        {
            $crefopayTranslations = [];
            $oModule = oxNew(Module::class);
            $sLangFile = $oModule->getModuleFullPath(CrefopayHelpers::getModuleId());
            if (is_dir($sLangFile)) {
                // 'de'-langfile is only used for getting the relevant keys
                // actual translation for current language is read below
                $sLangFile .= '/translations/de/cppayments_lang.php';
                $aLang = [];
                include($sLangFile);

                foreach ($aLang as $sTranslationIdent => $unused) {
                    if (stripos($sTranslationIdent, 'CREFOPAY_JSERROR_') === 0) {
                        $sTranslation = $oLang->translateString($sTranslationIdent);
                        if ($oLang->isTranslated()) {
                            $crefopayTranslations[$sTranslationIdent] = $sTranslation;
                        }
                    }
                }
            }
        }

        $js = "";
        $js .= "/** BEGIN crefopay **/\r\n";
        $js .= "window.crefopayConfig=" . json_encode($rv, JSON_PRETTY_PRINT) . ";\r\n";
        $js .= "window.crefopayTranslations=" . json_encode($crefopayTranslations, JSON_PRETTY_PRINT) . ";\r\n";
        $js .= "window.crefopayAllowedPaymentMethods=" . json_encode($oTrans->getAllowedCrefopayPaymentMethods(), JSON_PRETTY_PRINT) . ";\r\n";

        if ($sViewName === 'order') {
            $sJavascriptFile = 'crefopay_securefields_order.js';
        } elseif ($sViewName === 'payment') {
            $sJavascriptFile = 'crefopay_securefields_payment.js';
        } else {
            throw new StandardException('Invalid viewName: ' . print_r($sViewName, true));
        }

        // We embed this file here instead of using oxscript as 'priority' does only effect
        // 'include'-parameters
        $jsFile = $this->getModulePath(CrefopayHelpers::getModuleId(), 'out/src/js/' . $sJavascriptFile);
        if (($jsFile) && (file_exists($jsFile))) {
            $js .= file_get_contents($jsFile) . "\r\n";
        } else {
            $js .= "/** File missing: out/src/js/" . $sJavascriptFile . " **/";
        }
        $js .= "/** END crefopay **/\r\n";

        return $js;
    }

    public function getCrefopayConfigParam($sVarName, $defaultValue = null)
    {
        return CrefopayHelpers::getConfigParam($sVarName, $defaultValue);
    }

    /**
     * Called from crefopay_checkout_thankyou_info.tpl
     * @return void
     */
    public function crefopayResetSession()
    {
        CrefopayHelpers::forceNewTransaction();
    }

    /**
     * If $logo is 'CC3D_MC.png' then the following paths are tried:
     * '/source/modules/cppayments/out/src/img/CC3D_MC-{LANGISO2}.png'
     * '/source/modules/cppayments/out/src/img/CC3D_MC.png'
     * '/source/out/{THEMEID}/img/crefopay/CC3D_MC-{LANGISO2}.png'
     * '/source/out/{THEMEID}/img/crefopay/CC3D_MC.png'
     * '/source/out/{PARENT-THEMEID}/img/crefopay/CC3D_MC-{LANGISO2}.png'
     * '/source/out/{PARENT-THEMEID}/img/crefopay/CC3D_MC.png'
     * @param $logo
     * @return string
     */
    public function getCrefopayPaymentLogo($logo)
    {
        $tryPaths = [];

        $iLang = Registry::getLang()->getTplLanguage();
        $lang_iso2 = Registry::getLang()->getLanguageAbbr($iLang);

        $logo_filename_withoutext = pathinfo($logo, PATHINFO_FILENAME);
        $logo_filename_onlyext = pathinfo($logo, PATHINFO_EXTENSION);

        // Add theme folders:
        $sThemeId = $this->getActiveTheme();
        $endlessLoopProtectionCounter = 0;
        while (($sThemeId) && ($endlessLoopProtectionCounter < 5)) {
            $endlessLoopProtectionCounter++;

            $oTheme = oxNew(Theme::class);
            if ($oTheme->load($this->getActiveTheme())) {
                $sCrefopayThemePathRelative =   '/out/' . $oTheme->getId() . '/img/crefopay/';
                $tryPaths[] = $sCrefopayThemePathRelative;
                $sThemeId = $oTheme->getInfo('parentTheme');
            } else {
                // exit loop
                $sThemeId = '';
            }
        }

        // Add module folder:
        $oModule = oxNew(\OxidEsales\Eshop\Core\Module\Module::class);
        $sModulePath = $oModule->getModulePath(CrefopayHelpers::getModuleId());
        $sModuleImagePath = '/modules/' . $sModulePath . '/out/src/img/';
        $tryPaths[] = $sModuleImagePath;

        $sUseRelativeFilename = '';
        $aTriedFilenames = []; // for debugging only
        foreach ($tryPaths as $tryPath) {
            // with language-identifier:
            $tryRelativeFilename = $tryPath . $logo_filename_withoutext . '-' . $lang_iso2 . '.' . $logo_filename_onlyext;
            $tryLocalFilename = getShopBasePath() . $tryRelativeFilename;
            $aTriedFilenames[] = $tryRelativeFilename;
            if (file_exists($tryLocalFilename)) {
                $sUseRelativeFilename = $tryRelativeFilename;
                break;
            }

            // without language-identifier:
            $tryRelativeFilename = $tryPath . $logo;
            $tryLocalFilename = getShopBasePath() . $tryRelativeFilename;
            $aTriedFilenames[] = $tryRelativeFilename;
            if (file_exists($tryLocalFilename)) {
                $sUseRelativeFilename = $tryRelativeFilename;
                break;
            }
        }

        if ($sUseRelativeFilename) {
            $shopUrl = Registry::getConfig()->getShopUrl();
            $shopUrl = rtrim($shopUrl, '/');
            return $shopUrl . $sUseRelativeFilename;
        }

        return ''; // not found
    }

    public function getCrefopayGooglePayData()
    {
        $shopName=Registry::getConfig()->getActiveShop()->getFieldData('OXNAME');
        $shopName=html_entity_decode($shopName,ENT_QUOTES,'UTF-8');

        $oCurrency=Registry::getConfig()->getActShopCurrencyObject();
        $currencyCode='';
        if ($oCurrency) {
            $currencyCode = $oCurrency->name;
        }
        if (!$currencyCode)
        {
            // Fallback
            $currencyCode='EUR';
        }

        $rv=[];

        if (CrefopayHelpers::isLiveMode())
        {
            $rv['environment']='PRODUCTION';
        }
        else{
            $rv['environment']='TEST';
        }

        // '_' is prepended to force it to be encoded as string by json_encode
        // gets stripped away in js
        $rv['totalPrice']='_'.number_format(Registry::getSession()->getBasket()->getPrice()->getBruttoPrice(),2,'.','');
        $rv['countryCode']='DE'; // TODO: Should this be fixed to 'DE'?
        $rv['currencyCode']=$currencyCode;
        $rv['merchantName']=$shopName;

        $rv['gateway'] = 'lynck';
        $rv['gatewayMerchantId'] = (string)CrefopayHelpers::getMerchantId();
        $rv['allowedCardAuthMethods']=["PAN_ONLY"];
        $rv['allowedCardNetworks']=["VISA","MASTERCARD"];

        return json_encode($rv);
    }

    /**
     * @throws RandomException
     * @throws Exception
     */
    public function getCrefopayApplePayData(): string
    {
        try {
            $applePayData = [];

            $session = Registry::getSession();
            $lang = Registry::getLang();

            $applePayData['shopPublicKey'] = CrefopayHelpers::getConfigParam('CrefoPayShopPublicKey');
            $applePayData['orderNo'] = CrefopayTransaction::getActiveTransaction()->getCrefopayOrderId();

            $applePayTotal = [];
            $applePayTotal['label'] = 'test'; //TODO: use real info
            $applePayTotal['amount'] = $session->getBasket()->getPrice()->getPrice();

            $deliveryCountryId = $session->getUser()->getActiveCountry();
            $deliveryCountry = oxNew(Country::class);
            if (!$deliveryCountry->load($deliveryCountryId)) {
                throw new RuntimeException('delivery country failed to load');
            }

            $applePaySessionRequest = [];
            $applePaySessionRequest['countryCode'] = $deliveryCountry->oxcountry__oxisoalpha2->value;
            $applePaySessionRequest['currencyCode'] = $session->getBasket()->getBasketCurrency()->name;
            $applePaySessionRequest['total'] = $applePayTotal;

            $applePayData['sessionRequest'] = $applePaySessionRequest;

            $applePayConfiguration = [];
            $applePayConfiguration['stagingSystemUrl'] = CrefopayHelpers::isLiveMode() ? 'https://api.crefopay.de/' : 'https://sandbox.crefopay.de/';
            $applePayConfiguration['nonce'] = bin2hex(random_bytes(16));
            $applePayConfiguration['buttonstyle'] = 'black';
            $applePayConfiguration['locale'] = $lang->getLanguageAbbr();
            $applePayConfiguration['type'] = 'pay';

            $applePayData['configuration'] = $applePayConfiguration;
        } catch (RuntimeException $exception) {
            $translatedError = Registry::getLang()->translateString('CREFOPAY_APPLEPAY_INIT_ERROR');

            Registry::getUtilsView()->addErrorToDisplay($translatedError, false, false, '', 'payment');
            Registry::getUtils()->redirect('?cl=payment');
        }

        header('Content-Type: application/json');
        return (string) json_encode($applePayData);
    }

    public function getCrefopayApplePayTranslations(): string
    {
        $lang = Registry::getLang();

        header('Content-Type: application/json');
        return (string) json_encode(['CREFOPAY_APPLEPAY_AUTHENTICATION_FAILED' => $lang->translateString('CREFOPAY_APPLEPAY_AUTHENTICATION_FAILED')]);
    }
}
