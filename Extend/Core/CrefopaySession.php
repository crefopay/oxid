<?php

namespace Crefopay\Payments\Extend\Core;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Extend\Model\CrefopayOrder;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Core\Registry;

class CrefopaySession extends CrefopaySession_parent
{
    public function crefopayRestoreSession(CrefopayTransaction $cpTransaction)
    {
        $_POST[$this->getForcedName()] = $cpTransaction->getOxidSessionId();
        $this->_forceSessionStart();
        $this->_setSessionId($cpTransaction->getOxidSessionId());
        if (!$this->_sessionStart()) {
            CrefopayHelpers::getLogger()->error("_sessionStart() failed. Probably some other module already started the session?");
            return false;
        }

        $this->setVariable("sessionagent", Registry::getUtilsServer()->getServerVar('HTTP_USER_AGENT'));
        $this->_checkCookies(Registry::getUtilsServer()->getOxCookie('sid_key'), $this->getVariable("sessioncookieisset"));

        if ($this->getId() === $cpTransaction->getOxidSessionId()) {
            CrefopayHelpers::getLogger()->error("Original session '" . $cpTransaction->getOxidSessionId() . "' was restored!");

            /** @var Order|CrefopayOrder $oOrder */
            $aSerializedObjects = $cpTransaction->getSerializedObjects();
            $oBasket = $aSerializedObjects['basket'];
            $oUser = $aSerializedObjects['user'];

            if (!$oUser) {
                CrefopayHelpers::getLogger()->debug('crefopayFinalizeOrder() has invalid user: ' . print_r($oUser, true));
                return false;
            }

            CrefopayHelpers::getLogger()->debug('crefopayFinalizeOrder() uses user: ' . print_r($oUser->getId(), true));

            $oSession = Registry::getSession();
            $oSession->setVariable('crefopayOrderId', $cpTransaction->getCrefopayOrderId());
            $oSession->setUser($oUser);
            $oSession->setBasket($oBasket);

            return true;
        }

        CrefopayHelpers::getLogger()->error("Original session '" . $cpTransaction->getOxidSessionId() . "' was not restored! (Oxid generated sid: '" . $this->getId() . "')");
        return false;
    }
}
