<?php

namespace Crefopay\Payments\Extend\Controller;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Extend\Model\CrefopayPayment;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;

class CrefopayOrderController extends CrefopayOrderController_parent
{
    private function crefopayvalidateorderResult($bSuccess, $msg)
    {
        $rv = array();
        $rv['success'] = $bSuccess ? 1 : 0;
        $rv['msg'] = $msg;

        Registry::getUtils()->setHeader('Content-Type: application/json');
        echo json_encode($rv, JSON_PRETTY_PRINT);
        exit;
    }

    public function execute()
    {
        $bIsCrefopayPaymentMethod = false;

        /** @var CrefopayPayment $oPayment */
        $oPayment = $this->getPayment();
        if ($oPayment instanceof Payment) {
            if ($oPayment->isCrefopayPaymentMethod()) {
                $bIsCrefopayPaymentMethod = true;
            }
        }

        if ($bIsCrefopayPaymentMethod) {
            $cpTransaction = CrefopayTransaction::getActiveTransaction(true);
            if ($cpTransaction) {
                $cpTransaction->updateFromCallbackData('order');
            }

            if (!$cpTransaction->enterLock()) {
                CrefopayHelpers::getLogger()->debug('finalizeCrefopayOrder: already entered for this order by another process - skipping');
                return;
            }

            $rv = parent::execute();
            $cpTransaction->exitLock();

            return $rv;
        }

        return parent::execute();
    }

    public function crefopayvalidateorder()
    {
        if (!Registry::getSession()->checkSessionChallenge()) {
            $this->crefopayvalidateorderResult(false, 'stoken');
        }

        if (!$this->_validateTermsAndConditions()) {
            $this->crefopayvalidateorderResult(false, 'terms-and-conditions');
        }

        // additional check if we really really have a user now
        $oUser = $this->getUser();
        if (!$oUser) {
            $this->crefopayvalidateorderResult(false, 'login');
        }

        // get basket contents
        $oBasket = Registry::getSession()->getBasket();
        if (!$oBasket) {
            throw new StandardException('getBasket() failed');
        }
        if ($oBasket->getProductsCount()) {
            try {
                $oOrder = oxNew(\OxidEsales\Eshop\Application\Model\Order::class);

                if ($iOrderState = $oOrder->validateOrder($oBasket, $oUser)) {
                    $this->crefopayvalidateorderResult(false, 'order-state');
                }
            } catch (\Exception $ex) {
                $this->crefopayvalidateorderResult(false, 'ex - ' . $ex->getMessage());
            }
        }

        $this->crefopayvalidateorderResult(true, 'ok');
    }
}
