<?php

namespace Crefopay\Payments\Extend\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;

class CrefopayModulConfiguration extends CrefopayModulConfiguration_parent
{
    /**
     * The original-function is private and cannot be extended :(
     * @return string
     */
    private function crefopayGetSelectedModuleId()
    {
        $moduleId = $this->_sEditObjectId;
        if (!$moduleId) {
            $moduleId = Registry::getRequest()->getRequestEscapedParameter('oxid');
        }
        if (!$moduleId) {
            $moduleId = Registry::getSession()->getVariable('saved_oxid');
        }

        if ($moduleId === null) {
            throw new \InvalidArgumentException('Module id not found.');
        }

        return $moduleId;
    }
}
