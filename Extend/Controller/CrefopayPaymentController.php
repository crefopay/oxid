<?php

namespace Crefopay\Payments\Extend\Controller;

use Crefopay\Payments\Core\CrefopayConstants;
use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Core\CrefopayMapper;
use Crefopay\Payments\Extend\Model\CrefopayPayment;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Core\Registry;

class CrefopayPaymentController extends CrefopayPaymentController_parent
{
    public function failure()
    {
        CrefopayTransaction::getActiveTransaction()->forceNewTransaction();
        CrefopayHelpers::getLogger()->warn('failure() called: The payment was probably cancelled by the customer: ' . print_r($_GET, true));
        CrefopayHelpers::redirectOnError('payment', 0, 'CREFOPAY_ERROR_CANCELLED');
        exit;
    }

    public function crefopayIsBirthdateQuestionEnabled()
    {
        if (CrefopayHelpers::getDOBUsage() === CrefopayConstants::DOBUSAGE_DONTASK) {
            return false;
        }

        return true;
    }

    public function crefopayIsBirthdateMissing()
    {
        $oUser = Registry::getSession()->getUser();

        if (CrefopayHelpers::isB2BEnabledAndBusinessUser($oUser)) {
            // not required for business transactions
            return false;
        }

        $sBirthDate = $oUser->oxuser__oxbirthdate->value;
        $birthDateTimestamp = strtotime($sBirthDate);
        if (CrefopayHelpers::isDateOfBirthValid($birthDateTimestamp)) {
            return false;
        }

        return true;
    }

    public function crefopayGetPreselectedSalutation()
    {
        $oUser = Registry::getSession()->getUser();

        $oxsal = CrefopayHelpers::getSalutationFromSession();
        if (!$oxsal) {
            $oxsal = $oUser->oxuser__oxsal->value;
        }

        return $oxsal;
    }

    public function crefopayIsSalutationMissing()
    {
        $oUser = Registry::getSession()->getUser();
        $oxsal = $oUser->oxuser__oxsal->value;

        if (CrefopayHelpers::isB2BEnabledAndBusinessUser($oUser)) {
            // not required for business transactions
            return false;
        }

        $salutionWarning = '';
        if (CrefopayMapper::mapSalutationToCrefopay($oxsal, $salutionWarning)) {
            return false;
        }

        return true;
    }

    public function validatePayment()
    {
        $oSession = $this->getSession();
        if (!($sPaymentId = Registry::getRequest()->getRequestParameter('paymentid'))) {
            $sPaymentId = $oSession->getVariable('paymentid');
        }

        if ($sPaymentId) {
            /** @var CrefopayPayment $oPaymentMethod */
            $oPaymentMethod = oxNew(Payment::class);
            if ($oPaymentMethod->load($sPaymentId)) {
                if ($oPaymentMethod->isCrefopayPaymentMethod()) {
                    $cpTransaction = CrefopayTransaction::getActiveTransaction(true);
                    if ($cpTransaction) {
                        $cpTransaction->updateFromCallbackData('payment');
                    }
                }
            }
        }

        return parent::validatePayment();
    }

    public function crefopay_remove_userpaymentinstrument()
    {
        if (Registry::getSession()->checkSessionChallenge()) {
            $crefopay_userpaymentinstrumentid = Registry::getRequest()->getRequestParameter('crefopay_userpaymentinstrumentid');

            $request = new \CrefoPay\Library\Request\DeleteUserPaymentInstrument(CrefopayHelpers::getAPIConfig());
            $request->setPaymentInstrumentID($crefopay_userpaymentinstrumentid);

            CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\DeleteUserPaymentInstrument::class, $request, true);
        }

        return;
    }

    public function getCrefopayCreditCardTypeTitle($ccType)
    {
        $oLang = Registry::getLang();
        $sIdent = 'CREFOPAY_CC_TYPE_' . $ccType;

        $sTranslated = $oLang->translateString($sIdent);
        if ($oLang->isTranslated()) {
            return $sTranslated;
        }

        return htmlentities($ccType, ENT_QUOTES, 'UTF-8');
    }

    public function crefopayShowUserInstruments()
    {
        $oUser = Registry::getSession()->getUser();
        if (!$oUser->hasAccount()) {
            // guest users should not never get this feature
            return false;
        }

        return CrefopayHelpers::getConfigBool('CrefoPayAllowOtherUserInstruments', true);
    }

    /**
     * Called from cl=payment via ajax before registerPayment() to update an existing transaction
     * @return void
     */
    public function crefopay_refresh()
    {
        header("Content-Type: application/json");

        $paymentid = (string)Registry::getRequest()->getRequestParameter('paymentid');

        $oSession = Registry::getSession();
        if (!$oSession->isSessionStarted()) {
            $oSession->start();
        }
        $oBasket = $oSession->getBasket();
        $oBasket->setPayment($paymentid);
        $oBasket->calculateBasket(true);
        $oSession->setVariable('paymentid', $paymentid);
        $oSession->freeze();

        $rv = [];
        $oTransaction = CrefopayTransaction::getActiveTransaction(true);
        $rv['ok'] = 1;
        $rv['cporderid'] = $oTransaction->getCrefopayOrderId();
        $rv['is_new_transaction'] = CrefopayTransaction::$activeTransactionIsNew;

        echo json_encode($rv, JSON_PRETTY_PRINT);
        exit;
    }
}
