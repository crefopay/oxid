<?php

namespace Crefopay\Payments\Extend\Controller;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Core\CrefopayOrderCreator;
use Crefopay\Payments\Extend\Core\CrefopaySession;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Session;

class CrefopayThankYouController extends CrefopayThankYouController_parent
{
    public function init()
    {
        // skip init() if fnc != 'callback'
        if (Registry::getRequest()->getRequestParameter('fnc') !== 'callback') {
            parent::init();
        }
    }

    public function render()
    {
        // Remove active crefopay transaction from session:
        // so that a new order starts with a fresh transaction and does not try to reuse the already finished
        // transaction
        // We load it first so that it stays available for the rest of this controller:
        CrefopayTransaction::getActiveTransaction();

        $oSession = Registry::getSession();
        $oSession->deleteVariable('crefopayOrderId');
        $oSession->regenerateSessionId();

        return parent::render();
    }

    public function callback()
    {
        ob_start();

        CrefopayHelpers::getLogger()->debug('thankyou-callback reached');

        $cpOrderId = Registry::getRequest()->getRequestParameter('orderID');
        $isredir = (int)Registry::getRequest()->getRequestParameter('isredir');

        $cpTransaction = CrefopayTransaction::getTransactionByCrefopayOrderId($cpOrderId);
        if ($cpTransaction) {
            $cpTransaction->setRedirectStatus(CrefopayTransaction::REDIRECTSTATUS_RETURNED);
            $cpTransaction->save();

            /** @var Session|CrefopaySession $oSession */
            $oSession = Registry::getSession();

            if ($isredir === 1) {
                // Try to restore the session from cp-order-id:
                if (!$oSession->crefopayRestoreSession($cpTransaction)) {
                    CrefopayHelpers::getLogger()->error("callback: the session for cpOrderId '" . $cpOrderId . "' could not be recovered :(");
                    header("Content-Type: text/plain; charset=UTF-8");
                    echo Registry::getLang()->translateString('CREFOPAY_SHOPSESSIONBROKEN') . $cpOrderId;
                    exit();
                }
            } else {
                if ($oSession->getId() !== $cpTransaction->getOxidSessionId()) {
                    CrefopayHelpers::getLogger()->debug('callback: the browser dropped the session, trying to restore');
                    // Browser did drop the shop session during payment-provider-redirect
                    // Redirect to this fnc again, but with isredir=1
                    $urlParams = $_REQUEST;
                    $urlParams['isredir'] = 1;

                    $url = Registry::getConfig()->getCurrentShopUrl();
                    $url = Registry::getUtilsUrl()->processUrl($url, true, $urlParams);
                    Registry::getUtils()->redirect($url, false);
                    exit;
                }
            }
        } else {
            // The given orderID is not a valid cp-order-id :(
            CrefopayHelpers::getLogger()->error("The given cpOrderId '" . print_r($cpOrderId, true) . "' is invalid/unknown");
            CrefopayHelpers::redirectOnError('payment', 2002, 'Invalid crefopay order-id');
            exit;
        }

        // Session was restored if this is reached
        CrefopayHelpers::getLogger()->debug('callback: session is restored/still valid, calling finalizeCrefopayOrder()');
        $oOrderCreator = oxNew(CrefopayOrderCreator::class);
        $oOrderCreator->finalizeCrefopayOrder($cpTransaction, 'thankyou');
        $oSession->setVariable('sess_challenge', $cpTransaction->getOxidOrderId());

        if (CrefopayHelpers::getConfigBool('CrefoPayAutoCapture', false)) {
            $oOrder = oxNew(Order::class);
            if ($oOrder->load($cpTransaction->getOxidOrderId())) {
                $cpTransaction->addAutoCapture();
            }
        }

        //finish redirect
        CrefopayHelpers::getLogger()->debug('callback: redirecting to thankyou-page');
        $oConfig = Registry::getConfig();
        Registry::getUtils()->redirect($oConfig->getShopCurrentURL() . '&cl=thankyou', false);
        exit;
    }

    public function crefopayGetPaymentMethod()
    {
        $oPayment = oxNew(Payment::class);
        $sPaymentId = $this->getBasket()->getPaymentId();
        $oPayment->load($sPaymentId);

        return $oPayment;
    }
}
