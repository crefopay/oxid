<?php

// This file is not actually used by OXID
// Only intended to let phpStorm detect the correct types

namespace Crefopay\Payments\Extend\Controller {

    class CrefopayOrderController_parent extends \OxidEsales\Eshop\Application\Controller\OrderController
    {

    }

    class CrefopayPaymentController_parent extends \OxidEsales\Eshop\Application\Controller\PaymentController
    {

    }

    class CrefopayThankYouController_parent extends \OxidEsales\Eshop\Application\Controller\ThankYouController
    {

    }
}

namespace Crefopay\Payments\Extend\Core {

    use OxidEsales\Eshop\Core\Session;

    class CrefopayViewConfig_parent extends \OxidEsales\Eshop\Core\ViewConfig
    {

    }

    class CrefopaySession_parent extends Session
    {

    }
}

namespace Crefopay\Payments\Extend\Model {

    use OxidEsales\Eshop\Application\Model\PaymentList;
    use OxidEsales\Eshop\Application\Model\User;

    class CrefopayOrder_parent extends \OxidEsales\Eshop\Application\Model\Order
    {

    }

    class CrefopayPaymentGateway_parent extends \OxidEsales\Eshop\Application\Model\PaymentGateway
    {

    }

    class CrefopayPayment_parent extends \OxidEsales\Eshop\Application\Model\Payment
    {

    }

    class CrefopayPaymentList_parent extends PaymentList
    {

    }

    class CrefopayUser_parent extends User
    {

    }
}

namespace Crefopay\Payments\Extend\Controller\Admin {

    class CrefopayModulConfiguration_parent extends \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration
    {
    }
}