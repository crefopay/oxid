<?php

namespace Crefopay\Payments\Core;

use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;

class CrefopayEvents
{
    public static function onActivate()
    {
        try {
            self::onActivateInternal();
        } catch (\Exception $ex) {
            echo $ex->getMessage() . "\r\n";
            echo nl2br($ex->getTraceAsString()) . "\r\n";

            exit;
        }
    }

    private static function onActivateInternal()
    {
        $oLang = Registry::getLang();

        /** @var DbMetaDataHandler $oDbMeta */
        $oDbMeta = oxNew(DbMetaDataHandler::class);

        $bUpdateViewsRequired = false;

        if (!$oDbMeta->fieldExists('crefopay_paymentmethodid', 'oxpayments')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE oxpayments ADD COLUMN `crefopay_paymentmethodid` VARCHAR(15) NOT NULL DEFAULT '';");

            $bUpdateViewsRequired = true;
        }

        if (!$oDbMeta->tableExists('crefopay_transactions')) {
            $sql = "CREATE TABLE `crefopay_transactions` (
                `OXID` VARCHAR(32) NOT NULL COLLATE 'latin1_general_ci',
                `OXSESSIONID` TEXT NULL DEFAULT '' COMMENT 'Oxid Session-ID (\'sid\'-cookie)' COLLATE 'utf8_unicode_ci',
                `OXUSERID` VARCHAR(32) NULL DEFAULT '' COMMENT 'Oxid User-ID (oxuser.oxid)' COLLATE 'utf8_unicode_ci',
                `OXORDERID` VARCHAR(32) NULL COMMENT 'Oxid Order-ID (oxorder.OXID)' COLLATE 'utf8_unicode_ci',
                `CPUSERID` VARCHAR(50) NULL DEFAULT '' COMMENT 'Crefopay User-ID' COLLATE 'utf8_unicode_ci',
                `CPORDERID` VARCHAR(25) NULL DEFAULT '' COMMENT 'Crefopay Order-ID' COLLATE 'utf8_unicode_ci',
                `CPMERCHANTREFERENCE` VARCHAR(50) NULL DEFAULT '' COMMENT 'Gets set to oxorder.oxorderno on transaction finish' COLLATE 'utf8_unicode_ci',
                `CPRISKCLASS` INT(11) NULL DEFAULT '1' COMMENT '0=Trusted, 1=Default, 2=High-Risk',
                `CPCREATED` DATETIME NULL DEFAULT current_timestamp() COMMENT 'Time the transaction was created',
                `CPCUSTOMERDATA` LONGTEXT NULL DEFAULT '' COMMENT 'Customer-data associated with this transaction' COLLATE 'utf8_unicode_ci',
                `CPALLOWEDPAYMENTMETHODS` LONGTEXT NULL COMMENT 'List of allowed payment methods' COLLATE 'utf8_unicode_ci',
                `CPPAYMENTINSTRUMENTID` VARCHAR(50) NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
                `CPTRANSACTIONSTATUS` VARCHAR(25) NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
                `CPADDITIONALDATA` LONGTEXT NULL COLLATE 'utf8_unicode_ci',
                `CPTRANSACTIONJSON` LONGTEXT NULL DEFAULT '' COMMENT 'Used to detect basket changes' COLLATE 'utf8_unicode_ci',
                `CPPAYMENTMETHOD` VARCHAR(50) NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
                `CPPAYMENTREFERENCE` VARCHAR(100) NULL COLLATE 'utf8_unicode_ci',
                `CPORDERSTATUS` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `CPREDIRECTSTATUS` INT(11) NULL DEFAULT '0',
                `OXORDERSERIALIZED` LONGTEXT NULL COMMENT 'serialized and base64 encoded Order-object' COLLATE 'utf8_unicode_ci',
                `CPISCAPTURED` TINYINT(1) NULL DEFAULT '0',
                `CPUPDATED` TIMESTAMP NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                PRIMARY KEY (`OXID`) USING BTREE) COLLATE='utf8_unicode_ci' ENGINE=InnoDB;";

            DatabaseProvider::getDb()->execute($sql);
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD INDEX `idx_cporderid` (`CPORDERID`);");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD INDEX `idx_oxorderid` (`OXORDERID`);");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD INDEX `idx_transactionstatus` (`CPTRANSACTIONSTATUS`);");
        }

        if (!$oDbMeta->tableExists('crefopay_mns')) {
            $sql = "CREATE TABLE `crefopay_mns` (
                `OXID` VARCHAR(32) NOT NULL COLLATE 'latin1_general_ci',
                `merchantID` VARCHAR(32) NULL COLLATE 'utf8_unicode_ci',
                `storeID` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `orderID` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `captureID` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `merchantReference` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `paymentReference` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `userID` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `amount` INT(11) NULL,
                `currency` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `transactionStatus` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `orderStatus` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `additionalData` LONGTEXT NULL COLLATE 'utf8_unicode_ci',
                `timestamp` DATETIME NULL,
                `version` VARCHAR(50) NULL COLLATE 'utf8_unicode_ci',
                `is_processed` TINYINT(1) NULL DEFAULT '0',
                PRIMARY KEY (`OXID`) USING BTREE) COLLATE='utf8_unicode_ci' ENGINE=InnoDB;";

            DatabaseProvider::getDb()->execute($sql);
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_mns` ADD INDEX `idx_processed` (`is_processed`);");
        }

        if (!$oDbMeta->fieldExists('OXORDERNR', 'crefopay_transactions')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD COLUMN `OXORDERNR` int(11) NOT NULL DEFAULT '0';");
            $bUpdateViewsRequired = true;
        }

        // Issuer of credit card:
        if (!$oDbMeta->fieldExists('CPCCISSUER', 'crefopay_transactions')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD COLUMN `CPCCISSUER` varchar(15) NOT NULL DEFAULT '';");
            $bUpdateViewsRequired = true;
        }

        // Issuer of credit card:
        if (!$oDbMeta->fieldExists('CREFOPAY_USERRISKCLASS', 'oxuser')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `oxuser` ADD COLUMN `CREFOPAY_USERRISKCLASS` tinyint NOT NULL DEFAULT '1';");
            $bUpdateViewsRequired = true;
        }

        if (!$oDbMeta->tableExists('crefopay_captures')) {
            $sql = "CREATE TABLE IF NOT EXISTS `crefopay_captures` (
              `OXID` varchar(32) NOT NULL COMMENT '=Capture-ID',
              `CPORDERID` varchar(32) NOT NULL,
              `AMOUNT` double NOT NULL DEFAULT '0',
              `ISPAID` tinyint(1) NOT NULL DEFAULT '0',
              `PAIDDATE` datetime NOT NULL,
              `ISPAID_PROCESSED` tinyint(1) NOT NULL DEFAULT '0',
              `MERCHANTREF` varchar(150) NOT NULL DEFAULT '',
              `TSTAMP_ADDED` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `TSTAMP_UPDATED` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`OXID`),
              KEY `IDX_PAID` (`ISPAID`,`ISPAID_PROCESSED`) USING BTREE,
              KEY `IDX_CPORDERID` (`CPORDERID`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            DatabaseProvider::getDb()->execute($sql);
        }

        if (!$oDbMeta->tableExists('crefopay_checkorderexists')) {
            $sql = "CREATE TABLE IF NOT EXISTS `crefopay_checkorderexists` (
              `CPORDERID` varchar(32) NOT NULL,
              `TSTAMP_ADDED` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `ORDER_STATE` int(11) NOT NULL DEFAULT '0' COMMENT '0: Not checked yet\r\n1 = Already created\r\n2 = Gets created from callback just now (or failed while doing that)\r\n3 = Was created from callback',
              `TSTAMP_UPDATED` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`CPORDERID`),
              KEY `IDX_ORDER_STATE` (`ORDER_STATE`),
              KEY `TSTAMP_ADDED` (`TSTAMP_ADDED`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            DatabaseProvider::getDb()->execute($sql);
        }

        if (!$oDbMeta->fieldExists('CPPAIDDATE', 'crefopay_transactions')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD `CPPAIDDATE` DATETIME NULL DEFAULT NULL;");
            $bUpdateViewsRequired = true;
        }

        if (!$oDbMeta->fieldExists('CAPTURESTATE', 'crefopay_captures')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_captures` ADD `CAPTURESTATE` INT NOT NULL DEFAULT '0' AFTER `CPORDERID`;");
            $bUpdateViewsRequired = true;
        }

        if (!$oDbMeta->fieldExists('CP_CREATESOURCE', 'oxorder')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `oxorder` ADD `CP_CREATESOURCE` VARCHAR(15) NOT NULL DEFAULT '';");
            $bUpdateViewsRequired = true;
        }

        if (!$oDbMeta->fieldExists('CP_CREATEDBYUSERID', 'oxorder')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `oxorder` ADD `CP_CREATEDBYUSERID` VARCHAR(32) NOT NULL DEFAULT '';");
            $bUpdateViewsRequired = true;
        }

        if (!$oDbMeta->fieldExists('CPRISKDATA', 'crefopay_transactions')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD `CPRISKDATA` LONGTEXT NOT NULL DEFAULT '';");

            // Fix collations:
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_captures` CHANGE COLUMN `OXID` `OXID` VARCHAR(32) NOT NULL COMMENT '=Capture-ID' COLLATE 'latin1_general_ci';");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` CHANGE COLUMN `OXUSERID` `OXUSERID` VARCHAR(32) NULL DEFAULT '' COMMENT 'Oxid User-ID (oxuser.oxid)' COLLATE 'latin1_general_ci';");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` CHANGE COLUMN `OXORDERID` `OXORDERID` VARCHAR(32) NULL DEFAULT NULL COMMENT 'Oxid Order-ID (oxorder.OXID)' COLLATE 'latin1_general_ci';");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` CHANGE COLUMN `CPUSERID` `CPUSERID` VARCHAR(50) NULL DEFAULT '' COMMENT 'Crefopay User-ID' COLLATE 'latin1_general_ci'");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` CHANGE COLUMN `CPORDERID` `CPORDERID` VARCHAR(25) NULL DEFAULT '' COMMENT 'Crefopay Order-ID' COLLATE 'latin1_general_ci';");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_checkorderexists` CHANGE COLUMN `CPORDERID` `CPORDERID` VARCHAR(32) NOT NULL COLLATE 'latin1_general_ci';");
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_captures` CHANGE COLUMN `CPORDERID` `CPORDERID` VARCHAR(32) NOT NULL COLLATE 'latin1_general_ci';");

            $bUpdateViewsRequired = true;
        }

        if (!$oDbMeta->fieldExists('CPPAYMENTTOKEN', 'crefopay_transactions')) {
            DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ADD `CPPAYMENTTOKEN` LONGTEXT NOT NULL DEFAULT '';");

            try{
                DatabaseProvider::getDb()->execute("ALTER TABLE `crefopay_transactions` ROW_FORMAT=COMPRESSED;");
            }
            catch(\Exception $ex)
            {
                // Already has ROW_FORMAT=COMPRESSED
            }

            $bUpdateViewsRequired = true;
        }

        if ($bUpdateViewsRequired) {
            $oDbMeta->updateViews();
            self::clearTmp();
        }

        $cpPaymentMethods = CrefopayHelpers::getPaymentMethodsFromJSON();
        foreach ($cpPaymentMethods as $cpPaymentMethod) {
            $bWasLoaded = false;

            $oPaymentMethod = oxNew(Payment::class);
            if ($oPaymentMethod->load($cpPaymentMethod['oxid'])) {
                $bWasLoaded = true;
            }

            $oPaymentMethod->setId($cpPaymentMethod['oxid']);
            if (!$bWasLoaded) { // update only if module is first activated
                $oPaymentMethod->oxpayments__oxdesc = new Field($cpPaymentMethod['oxdesc']['de']);
                $oPaymentMethod->oxpayments__oxactive = new Field(1);
                $oPaymentMethod->oxpayments__oxtoamount = new Field(1000000);
            }
            $oPaymentMethod->oxpayments__crefopay_paymentmethodid = new Field($cpPaymentMethod['crefopay_paymentmethodid']);
            $oPaymentMethod->save();

            // Description in various languages:
            if (!$bWasLoaded) { //  update only if module is first activated
                $aAllLanguageIds = $oLang->getAllShopLanguageIds();
                foreach ($aAllLanguageIds as $iLangId => $sLangAbbr) {
                    if (isset($cpPaymentMethod['oxdesc'][$sLangAbbr])) {
                        $oPaymentMethod->setLanguage($iLangId);
                        $oPaymentMethod->oxpayments__oxdesc = new Field($cpPaymentMethod['oxdesc'][$sLangAbbr]);
                        $oPaymentMethod->save();
                    }
                }
            }
        }

        self::clearTmp();
    }

    public static function onDeactivate()
    {
        self::clearTmp();
    }

    private static function clearTmp()
    {
        $sTmpDir = Registry::getConfig()->getConfigParam('sCompileDir');
        if (is_dir($sTmpDir)) {
            $aFiles = glob($sTmpDir . '/*{.php,.txt}', GLOB_BRACE);
            $aFiles = array_merge($aFiles, glob($sTmpDir . '/smarty/*.php'));
            foreach ($aFiles as $sFile) {
                @unlink($sFile);
            }
        }
    }
}
