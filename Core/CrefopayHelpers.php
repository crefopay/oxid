<?php

namespace Crefopay\Payments\Core;

use Exception;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use RuntimeException;
use CrefoPay\Library\Api\AbstractApi;
use CrefoPay\Library\Config;
use CrefoPay\Library\Request\AbstractRequest;
use CrefoPay\Library\Response\AbstractResponse;
use CrefoPay\Library\Response\FailureResponse;
use CrefoPay\Library\Validation\Validation;

class CrefopayHelpers
{
    /**
     * @return string
     */
    public static function getModuleId()
    {
        return 'cppayments';
    }

    /**
     * @var CrefopayLogger
     */
    private static $oCrefopayLogger = null;

    /**
     * @return CrefopayLogger
     * @throws Exception
     */
    public static function getLogger()
    {
        $oLogger = self::$oCrefopayLogger;
        if ($oLogger === null) {
            $oLogger = oxNew(CrefopayLogger::class);
            self::$oCrefopayLogger = $oLogger;
        }

        if ($oLogger instanceof CrefopayLogger) {
            return $oLogger;
        }

        throw new StandardException('Failed to initialize logger');
    }

    public static function getRandomString($iLen)
    {
        $randBytes = random_bytes($iLen);
        $randString = bin2hex($randBytes);

        return substr($randString, 0, $iLen);
    }

    public static function getConfigBool($sVarName, $bDefaultValue = false)
    {
        $rv = self::getConfigParam($sVarName, $bDefaultValue);
        if ($rv) {
            return true;
        }

        return false;
    }

    public static function getConfigParam($sVarName, $defaultValue = null)
    {
        $oConfig = Registry::getConfig();
        $rv = $oConfig->getShopConfVar($sVarName, null, 'module:' . self::getModuleId());
        if ($rv !== null) {
            return $rv;
        }

        return $defaultValue;
    }

    public static function setConfigParam($sVarType, $sVarName, $varValue)
    {
        $oConfig = Registry::getConfig();
        $oConfig->saveShopConfVar($sVarType, $sVarName, $varValue, null, 'module:' . self::getModuleId());

        self::updateModuleSettingsYAML();
    }

    /**
     * @return bool
     */
    public static function isLiveMode()
    {
        return self::getConfigBool('CrefoPaySystemMode', false);
    }

    /**
     * @return bool
     */
    public static function allowOtherPaymentsEnabled()
    {
        return self::getConfigBool('CrefoPayAllowOtherPayments', true);
    }

    public static function getCrefopayLibraryUrl($libraryType, $liveEndpoint = null)
    {
        if ($liveEndpoint === null) {
            $liveEndpoint = self::isLiveMode();
        }

        $modeKey = ($liveEndpoint) ? 'live' : 'sandbox';

        $cpData = self::getCrefopayJSON();

        return $cpData['libraries'][$modeKey][$libraryType];
    }

    public static function getEndpointUrl($endpointType, $liveEndpoint = null)
    {
        if ($liveEndpoint === null) {
            $liveEndpoint = self::isLiveMode();
        }

        $modeKey = ($liveEndpoint) ? 'live' : 'sandbox';

        $cpData = self::getCrefopayJSON();

        return $cpData['endpoints'][$modeKey][$endpointType];
    }

    private static $crefopayJSON;

    public static function getCrefopayJSON()
    {
        if (self::$crefopayJSON === null) {
            $jsonFilename = __DIR__ . '/../crefopay.json';
            if (!file_exists($jsonFilename)) {
                throw new RuntimeException("File 'crefopay.json' not found!");
            }

            $jsonString = file_get_contents($jsonFilename);

            self::$crefopayJSON = json_decode($jsonString, true);
            if (!is_array(self::$crefopayJSON)) {
                throw new RuntimeException("Failed to parse '" . $jsonFilename . "': " . json_last_error_msg());
            }
        }

        return self::$crefopayJSON;
    }

    public static function getPaymentMethodsFromJSON()
    {
        $cpData = self::getCrefopayJSON();

        return $cpData['paymentMethods'];
    }

    /**
     * @return Config
     * @throws Exception
     */
    public static function getAPIConfig()
    {
        // see https://repo.crefopay.de/crefopay/clientlibrary/-/wikis/Configuration-object

        $oLogger = self::getLogger();
        if (!$oLogger) {
            throw new \RuntimeException('Failed to init logger');
        }

        $logLocationMain = $oLogger->getLogFilename();
        $logLocationRequest = $oLogger->getLogFilename();
        $logEnabled = ($oLogger->getLogLevel() === 0); // 0 = loglevel 'debug'

        return new Config(
            [
                'merchantID' => self::getMerchantId(),
                'merchantPassword' => self::getMerchantPassword(),
                'storeID' => self::getStoreId(),
                'logLocationMain' => $logLocationMain,
                'logLocationRequest' => $logLocationRequest,
                'defaultLocale' => CrefopayMapper::getCrefopayLocale(),
                'baseUrl' => self::getEndpointUrl('api'),
                'logEnabled' => $logEnabled,
                'sendRequestWithSalt' => true
            ]
        );
    }

    public static function getMerchantId()
    {
        return self::getConfigParam('CrefoPayMerchantId');
    }

    public static function getMerchantPassword()
    {
        return self::getConfigParam('CrefoPayPrivateKey');
    }

    public static function getStoreId()
    {
        return self::getConfigParam('CrefoPayStoreId');
    }

    public static function getShopPublicKey()
    {
        return self::getConfigParam('CrefoPayShopPublicKey');
    }

    /**
     * @return bool
     */
    public static function isB2BEnabled()
    {
        return self::getConfigParam('CrefoPayB2BEnabled') ? true : false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public static function isB2BEnabledAndBusinessUser(User $user)
    {
        if (!self::isB2BEnabled()) {
            return false;
        }

        if ($user->oxuser__oxcompany->value) {
            return true;
        }

        return false;
    }

    private static function singleLineDump($item)
    {
        if (is_null($item)) {
            return '(null)';
        }
        if (is_array($item)) {
            $rv = '[';
            $x = 0;
            foreach ($item as $key => $singleItem) {
                if ($x > 0) {
                    $rv .= ', ';
                }
                $rv .= json_encode($key);
                $rv .= ':';
                $rv .= self::singleLineDump($singleItem);
                $x++;
            }
            $rv .= ']';

            return $rv;
        }

        if (is_scalar($item)) {
            if (is_string($item)) {
                return "'" . $item . "'";
            }
            return '' . $item;
        }

        if ($item instanceof \CrefoPay\Library\Request\Objects\AbstractObject) {
            return json_encode($item->toArray());
        }
        if ($item instanceof \CrefoPay\Library\Request\Attributes\ObjectArray) {
            return json_encode($item->toArray());
        }

        return '(' . get_class($item) . ')';
    }

    public static function logCrefopayApiRequest(AbstractRequest $request)
    {
        $requestClassName = get_class($request);
        $requestJSON = "[" . $requestClassName . "] " . self::singleLineDump($request->toArray());
        self::getLogger()->debug($requestJSON);
    }

    public static function logCrefopayApiResponse($response, $exception = null)
    {
        $responseJSON = '';

        if (is_string($response)) {
            $responseJSON .= "[string] " . $response;
        }

        if ($response instanceof AbstractResponse) {
            $responseClassName = get_class($response);

            $resultCode = $response->getData('resultCode');
            if ($resultCode === null) {
                $resultCode = '(no resultcode)';
            }

            $responseJSON .= "[" . $responseClassName . "][" . $resultCode . "] " . self::singleLineDump($response->getAllData(true));
        }

        if ($exception instanceof Exception) {
            $responseJSON .= '- exception: ' . $exception->getMessage() . " - " . $exception->getTraceAsString();
        }

        self::getLogger()->debug($responseJSON);
    }

    public static function addErrorToDisplay($crefopayErrorCode, $crefopayErrorMessageIdent = null, $aValidationMessages = [], $destinationController = 'payment')
    {
        $oLang = Registry::getLang();

        $sTranslatedError = '';
        if (count($aValidationMessages) > 0) {
            $x = 0;
            foreach ($aValidationMessages as $sValidationMessage) {
                if ($x > 0) {
                    $sTranslatedError .= "<br>";
                }
                $sTranslatedError .= htmlentities($sValidationMessage, ENT_QUOTES, 'UTF-8');
                $x++;
            }
        } else {
            if (!$crefopayErrorMessageIdent) {
                $crefopayErrorMessageIdent = 'CREFOPAY_UNKNOWN_ERROR';
                if ($crefopayErrorCode >= 0) {
                    $crefopayErrorMessageIdent = 'CREFOPAY_ERROR_' . $crefopayErrorCode;
                }
            }

            $sTranslatedError = $oLang->translateString($crefopayErrorMessageIdent);
            if (!$oLang->isTranslated()) {
                self::getLogger()->debug("No specific translation for '" . $sTranslatedError . "' available");

                $sTranslatedError = $oLang->translateString('CREFOPAY_UNKNOWN_ERROR');
            }
        }

        $sTranslatedError = '(' . $crefopayErrorCode . ') ' . $sTranslatedError;

        Registry::getUtilsView()->addErrorToDisplay($sTranslatedError, false, false, '', $destinationController);
    }

    public static function redirectOnError($destination, $crefopayErrorCode, $crefopayErrorMessage = null, $aValidationMessages = [])
    {
        self::addErrorToDisplay($crefopayErrorCode, $crefopayErrorMessage, $aValidationMessages, $destination);

        // redirect to payment-selection page:
        $oSession = Registry::getSession();
        $dstUrl = Registry::getConfig()->getShopCurrentUrl();
        $dstUrl .= '&cl=' . rawurlencode($destination);

        $dstUrl = $oSession->processUrl($dstUrl);
        Registry::getUtils()->redirect($dstUrl);
        exit;
    }

    public static function getResultCodeFromResponse($response)
    {
        if ($response instanceof AbstractResponse) {
            $resultCode = $response->getData('resultCode');
            if ($resultCode === null) {
                return -1;
            }

            return (int)$resultCode;
        }

        return -1; // Unknown
    }

    private static function getValidationMessageTranslation($errorMessageEn, $iLang = null)
    {
        $oLang = Registry::getLang();
        if ($iLang === null) {
            $iLang = $oLang->getBaseLanguage();
        }

        $ident = Str::getStr()->strtoupper($errorMessageEn);
        $ident = str_replace(' ', '_', $ident);
        $ident = str_replace('/', '_', $ident);
        $ident = str_replace(',', '_', $ident);
        while (stripos($ident, '__') !== false) {
            $ident = str_replace('__', '_', $ident);
        }

        $iso2 = Str::getStr()->strtoupper($oLang->getLanguageAbbr($iLang));

        if ($iso2 === 'EN') {
            return $errorMessageEn;
        }

        $cpIdent = 'CREFOPAY_VMSG_' . $ident;

        $sTranslated = $oLang->translateString($cpIdent);
        if ($oLang->isTranslated()) {
            return $sTranslated;
        }

        // Log missing translation:
        {
            /** @var CrefopayLogger $oTranslationLogger */
            $oTranslationLogger = oxNew(CrefopayLogger::class);
            $oTranslationLogger->setLoggingIdent('translations', false);
            $oTranslationLogger->error("Missing translation for: [" . $iso2 . "] '" . $cpIdent . "'", false);
        }

        return $errorMessageEn;
    }

    public static function validateRequest(AbstractRequest $request)
    {
        $aValidationMessages = [];

        $validation = new Validation();
        $validation->getValidator($request);
        $aValidationResult = $validation->performValidation();

        foreach ($aValidationResult as $objName => $aObjectValidationFields) {
            foreach ($aObjectValidationFields as $fieldName => $errorMessages) {
                foreach ($errorMessages as $errorMessage) {
                    $aValidationMessages[] = self::getValidationMessageTranslation($errorMessage);
                }
            }
        }

        return $aValidationMessages;
    }

    /**
     * @param                 $apiClassName
     * @param AbstractRequest $request
     * @param                 $dontThrowException
     * @return AbstractResponse|FailureResponse|\CrefoPay\Library\Response\SuccessResponse|null
     */
    public static function executeApiCall($apiClassName, AbstractRequest $request, $dontThrowException)
    {
        self::logCrefopayApiRequest($request);
        try {
            /** @var AbstractApi $api */
            $api = new $apiClassName(self::getAPIConfig(), $request);
            $response = $api->sendRequest();
            self::logCrefopayApiResponse($response, null);

            return $response;
        } catch (Exception $ex) {
            $response = null;

            $responseRaw = $api->getResponseRaw();

            self::logCrefopayApiResponse($responseRaw, $ex);

            if (is_string($responseRaw)) {
                try {
                    $rawResponseData = json_decode($responseRaw, true);
                    if (is_array($rawResponseData)) {
                        $response = new FailureResponse(self::getAPIConfig(), $rawResponseData);
                    }
                } catch (Exception $unused) {
                }
            }

            if (!($response instanceof AbstractResponse)) {
                $response = null;
            }

            self::logCrefopayApiResponse($response, $ex);

            $resultCode = self::getResultCodeFromResponse($response);
            if (!$dontThrowException) {
                $message = '';
                if ($response) {
                    $message = $response->getData('message');
                }

                $aValidationMessages = self::validateRequest($request);

                if ($resultCode === 1002) {
                    if (stripos($message, 'PaymentMethod not correct') !== false) {
                        // Convert to different error message for user display:
                        // 2000 = The payment method is not available. Please use another payment method.
                        $resultCode = 2000;
                    }
                }

                $destination = 'start';
                if ($apiClassName === \CrefoPay\Library\Api\Reserve::class) {
                    $destination = 'payment';

                    // Throw away current transaction:
                    $oSession = Registry::getSession();
                    $oSession->deleteVariable('crefopayOrderId');
                    $oSession->deleteVariable('sess_challenge');
                    $oSession->regenerateSessionId();
                    $oSession->freeze();
                }

                self::redirectOnError($destination, $resultCode, null, $aValidationMessages);
            }

            return $response;
        }
    }

    public static function getTransactionStates()
    {
        return ["NEW", "ACKNOWLEDGEPENDING", "FRAUDPENDING", "MERCHANTPENDING", "CIAPENDING", "EXPIRED", "CANCELLED", "FRAUDCANCELLED", "DONE"];
    }

    /**
     * @param $oxuser
     * @return string
     */
    public static function getCrefopayUserIdFromOxidUser($oxuser)
    {
        if (self::isB2BEnabledAndBusinessUser($oxuser)) {
            return $oxuser->getId() . '_B2B';
        }

        return $oxuser->getId() . '_B2C';
    }

    public static function isDateOfBirthValid($tstamp)
    {
        if (!$tstamp) {
            return false;
        }

        $minDOB = mktime(0, 0, 0, 1, 1, 1200);
        $maxDOB = time();

        return ($tstamp >= $minDOB) && ($tstamp <= $maxDOB);
    }

    public static function forceNewTransaction()
    {
        $oSession = Registry::getSession();
        $prevSessionId = $oSession->getId();
        $oSession->deleteVariable('crefopayOrderId');
        $oSession->deleteVariable('sess_challenge');
        $oSession->regenerateSessionId();
        $newSessionId = $oSession->getId();

        $oLogger = CrefopayHelpers::getLogger();
        if (!$oLogger) {
            return;
        }
        $oLogger->debug('forcing new transaction+session. New session-id: ' . $newSessionId);
        $oLogger->setLoggingIdent($newSessionId);
        $oLogger->debug('forced new transaction+session. Previous session-id was: ' . $prevSessionId);
    }

    public static function updateModuleSettingsYAML()
    {
        $containerFactoryClass = "\\OxidEsales\\EshopCommunity\\Internal\\Container\\ContainerFactory";

        if (class_exists($containerFactoryClass)) {
            // Newer Oxid versions
            $container = $containerFactoryClass::getInstance()->getContainer();

            /** @var OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ModuleConfigurationDaoBridgeInterface $moduleConfigurationDaoBridge */
            $moduleConfigurationDaoBridge = $container->get('OxidEsales\EshopCommunity\Internal\Framework\Module\Configuration\Bridge\ModuleConfigurationDaoBridgeInterface');

            if ($moduleConfigurationDaoBridge) {
                $moduleConfiguration = $moduleConfigurationDaoBridge->get(CrefopayHelpers::getModuleId());

                $aSettings = $moduleConfiguration->getModuleSettings();
                foreach ($aSettings as $oSetting) {
                    $val = CrefopayHelpers::getConfigParam($oSetting->getName());
                    $oSetting->setValue($val);
                }
                $moduleConfigurationDaoBridge->save($moduleConfiguration);
            } else {
                Registry::getLogger()->error('ModuleConfigurationDaoBridgeInterface not found!');
            }
        }
    }

    /**
     * Truncates a string in the middle if needed to maxLen
     * (Places '...' in the middle)
     * @param $str
     * @param $maxLen
     * @return mixed|string
     * @throws StandardException
     */
    public static function truncateString($str, $maxLen)
    {
        if ($maxLen < 3) {
            throw new StandardException('maxLen must be >=3');
        }

        $oStr = Str::getStr();

        $len = $oStr->strlen($str);
        if ($len <= $maxLen) {
            return $str;
        }

        $aChars = [];
        for ($i = 0; $i < $len; $i++) {
            $aChars[] = $oStr->substr($str, $i, 1);
        }

        $keep = true;
        while ($keep) {
            $cnt = count($aChars);
            if ($cnt > ($maxLen - 3)) {
                $mid = (int)$cnt / 2;
                unset($aChars[$mid]);
                $aChars = array_values($aChars);
            } else {
                $keep = false;
            }
        }

        $cnt = count($aChars);
        $mid = (int)$cnt / 2;

        $rv = '';
        for ($i = 0; $i < $mid; $i++) {
            $rv .= $aChars[$i];
        }
        $rv .= "...";
        for ($i = $mid + 1; $i < $cnt; $i++) {
            $rv .= $aChars[$i];
        }

        return $rv;
    }

    public static function htmlDecode($str)
    {
        return html_entity_decode($str, ENT_QUOTES, 'UTF-8');
    }

    public static function getSalutationFromSession()
    {
        $oSession = Registry::getSession();
        $cpSalutation = $oSession->getVariable('cppayments_salutation');
        if ($cpSalutation) {
            return $cpSalutation;
        }

        return '';
    }

    public static function setSalutationInSession($cpSalutation)
    {
        $oSession = Registry::getSession();
        $oSession->setVariable('cppayments_salutation', $cpSalutation);
    }

    public static function setDOBInSession($iDOB)
    {
        $oSession = Registry::getSession();
        $oSession->setVariable('cppayments_dob', $iDOB);
    }

    public static function getDOBFromSession()
    {
        return Registry::getSession()->getVariable('cppayments_dob');
    }

    /**
     * Returns wether date-of-birth should be queried for and wether dob should be saved to oxuser
     * see CrefopayConstants for possible values
     * @return int
     */
    public static function getDOBUsage()
    {
        return (int)self::getConfigParam('CrefoPayDOBUsage', CrefopayConstants::DOBUSAGE_ASK_AND_SAVE);
    }
}
