<?php

namespace Crefopay\Payments\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;

class CrefopayLogger
{
    private $logfile;
    private $iLogLevel;
    private $logStrackTrace = true;

    private $sLoggingIdent = 'nosession';

    private $iSilence = 0;

    public function beginSilence()
    {
        $this->iSilence++;
    }

    public function endSilence()
    {
        $this->iSilence--;
    }

    public function setLoggingIdent($loggingIdent, $bWithDate = true)
    {
        $this->sLoggingIdent = $loggingIdent;
        self::$isFirstLogInRequest = true;

        $this->logfile = $this->getCrefopayLogFolder();
        if ($bWithDate) {
            $this->logfile .= date("Y-m-d");
            $this->logfile .= '/';
        }

        $this->logfile .= $this->getSafeFilename($loggingIdent);
        $this->logfile .= '.log';
    }

    private function getSafeFilename($filename)
    {
        // Whitelisted characters
        $aAllowedChars = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz-");

        $oStr = Str::getStr();
        $rv = '';

        // Replace non whitelisted characters with their hex representation:
        $iLen = $oStr->strlen($filename);
        for ($i = 0; $i < $iLen; $i++) {
            $c = $oStr->substr($filename, $i, 1);

            if (in_array($c, $aAllowedChars)) {
                $rv .= $c;
            } else {
                $rv .= '-x' . bin2hex($c) . '-';
            }
        }

        // Remove duplicate '--':
        while ($oStr->strpos($rv, '--') !== false) {
            $rv = str_replace('--', '-', $rv);
        }
        $rv = trim($rv, "-");

        return $rv;
    }

    public function getCrefopayLogFolder()
    {
        $logfolder = getShopBasePath() . '/log/crefopay/';
        if (!is_dir($logfolder)) {
            if (!mkdir($logfolder) && !is_dir($logfolder)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $logfolder));
            }
        }

        return $logfolder;
    }

    public function getLoggingIdent()
    {
        return $this->sLoggingIdent;
    }

    public function __construct()
    {
        $sSessionId = 'nosession';
        $oSession = Registry::getSession();
        if ($oSession->isSessionStarted()) {
            $sSessionId = $oSession->getId();
        }

        $this->setLoggingIdent($sSessionId);

        /*
         * 0 = Debug
         * 1 = Warn
         * 2 = Error
         */
        $this->iLogLevel = (int)CrefopayHelpers::getConfigParam('CrefoPayLogLevel', 1);
    }

    public function getLogLevel()
    {
        return $this->iLogLevel;
    }

    public function getLogFilename()
    {
        return $this->logfile;
    }

    private static $isFirstLogInRequest = true;

    private function log($level, $line, $bLogStackTrace = null)
    {
        if ($this->iSilence > 0) {
            // Logger is silenced
            return;
        }

        if ($bLogStackTrace === null) {
            $bLogStackTrace = $this->logStrackTrace;
        }

        $line_str = $line;
        if (is_object($line)) {
            $line_str = print_r($line, true);
        }

        if ($level >= $this->iLogLevel) {
            $ex = new \Exception();
            $trace = $ex->getTraceAsString();
            $trace = str_replace("\r\n", "\n", $trace);
            $trace = str_replace("\r", "\n", $trace);
            $trace = str_replace("\n", "\\n", $trace);

            $linePrefix = "[" . date("Y-m-d H:i:s") . "] ";
            $logdata = "";

            if (self::$isFirstLogInRequest) {
                self::$isFirstLogInRequest = false;

                $url = "";
                if (isset($_SERVER['REQUEST_URI'])) {
                    $url = (string)$_SERVER['REQUEST_URI'];

                    $qmPos = strpos($url, '?');
                    if ($qmPos > 0) {
                        $url = substr($url, 0, $qmPos);
                    }
                }
                if (isset($_REQUEST)) {
                    $url .= '?';
                    $url .= http_build_query($_REQUEST);
                }

                $logdata .= "\r\n";
                $logdata .= $linePrefix . " ____________________________________ URL: " . $url . "____________________________________\r\n";
            }

            $logdata .= $linePrefix . rtrim($line_str) . "\r\n";
            if ($bLogStackTrace) {
                $logdata .= $linePrefix;
                $logdata .= str_pad(" ", 120, " ");
                $logdata .= rtrim($trace) . "\r\n";
            }

            $logdir = dirname($this->logfile);
            if (!is_dir($logdir)) {
                if (!mkdir($logdir, 0777, true) && !is_dir($logdir)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $logdir));
                }
            }

            file_put_contents($this->logfile, $logdata, FILE_APPEND);
        }
    }

    public function debug($debug, $bLogStackTrace = null)
    {
        $this->log(0, $debug, $bLogStackTrace);
    }

    public function warn($warn, $bLogStackTrace = null)
    {
        $this->log(1, $warn, $bLogStackTrace);
    }

    public function error($err, $bLogStackTrace = null)
    {
        $this->log(2, $err, $bLogStackTrace);
    }

    public function removeLogsOlderThan($minTimestamp)
    {
        $logsFolder = $this->getCrefopayLogFolder();
        $this->removeLogsOlderThanInternal($logsFolder, $minTimestamp);
    }

    private function removeLogsOlderThanInternal($logsFolder, $minTimestamp)
    {
        $filesLeft = 0; // Counts the number of the files that got not deleted yet

        if (is_dir($logsFolder)) {
            $subdirs = [];

            if ($dh = opendir($logsFolder)) {
                while (($fsEntry = readdir($dh)) !== false) {
                    if (($fsEntry === '.') || ($fsEntry === '..')) {
                        continue;
                    }

                    $fullPath = $logsFolder . $fsEntry;

                    if (filetype($fullPath) === 'dir') {
                        $subdirs[] = $fullPath . '/';
                    } elseif (filetype($fullPath) === 'file') {
                        $fext = pathinfo($fullPath, PATHINFO_EXTENSION);
                        if ($fext === 'log') {
                            if (filemtime($fullPath) < $minTimestamp) {
                                @unlink($fullPath);
                            } else {
                                $filesLeft++;
                            }
                        } else {
                            $filesLeft++;
                        }
                    }
                }
                closedir($dh);

                foreach ($subdirs as $subdir) {
                    $subFilesLeft = $this->removeLogsOlderThanInternal($subdir, $minTimestamp);
                    $filesLeft += $subFilesLeft;
                    if ($subFilesLeft === 0) {
                        // not files left in this subdirectory
                        @rmdir($subdir);
                    }
                }
            }
        }

        return $filesLeft;
    }
}
