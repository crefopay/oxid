<?php

namespace Crefopay\Payments\Core;

use Crefopay\Payments\Extend\Model\CrefopayOrder;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;

class CrefopayOrderCreator
{
    public function finalizeCrefopayOrder(CrefopayTransaction $cpTransaction, $createSource)
    {
        $logger = CrefopayHelpers::getLogger();
        $logger->debug('finalizeCrefopayOrder() for "' . $cpTransaction->getId() . '" reached via "' . $createSource . '"');

        if (!$cpTransaction->enterLock()) {
            $logger->debug('finalizeCrefopayOrder: already entered for this order by another process - skipping');
            return;
        }

        $oxidOrderId = $cpTransaction->getOxidOrderId();
        if ($oxidOrderId) {
            $logger->debug('finalizeCrefopayOrder: Checking if oxorderid "' . $oxidOrderId . '" actually exists');
            $oTestOrder = oxNew(Order::class);
            if ($oTestOrder->exists($oxidOrderId)) {
                // Order was already created (maybe the mns-processor was faster or browser reload?)
                $logger->debug('finalizeCrefopayOrder: order was previously created - skipping');
                $cpTransaction->exitLock();
                return;
            }
        }

        /** @var Order|CrefopayOrder $oOrder */
        $aSerializedObjects = $cpTransaction->getSerializedObjects();
        $oOrder = $aSerializedObjects['order'];
        $oBasket = $aSerializedObjects['basket'];
        $oUser = $aSerializedObjects['user'];

        if ($createSource === 'backend') {
            $oOrder->crefopayForceOrderCreation = true;
        }

        if (!$oUser) {
            $logger->debug('crefopayFinalizeOrder() has invalid user: ' . print_r($oUser, true));
        }

        if ($oUser != null) {
            $logger->debug('crefopayFinalizeOrder() uses user: ' . print_r($oUser->getId(), true));
        }

        $oSession = Registry::getSession();

        $prev_isAdmin = Registry::getConfig()->isAdmin();
        $prev_user = $oSession->getUser();
        $prev_basket = $oSession->getBasket();

        $oSession->setVariable('crefopayOrderId', $cpTransaction->getCrefopayOrderId());
        $oSession->setUser($oUser);
        $oSession->setBasket($oBasket);
        Registry::getConfig()->setAdminMode(false);

        // Make active transaction available:
        CrefopayTransaction::getActiveTransaction();

        if (!$oBasket) {
            $logger->error('Basket-Object is invalid');
            if ($createSource === 'backend') {
                throw new \Exception("Basket-Object is invalid!");
            }
            $cpTransaction->exitLock();
            CrefopayHelpers::redirectOnError('start', 0, 'CREFOPAY_ERROR_INVALID_BASKET');
            return;
        }

        // finalizing ordering process (validating, storing order into DB, executing payment, setting status ...)
        $logger->debug('crefopayFinalizeOrder() calls finalizeOrder()');
        $oSession->setVariable('sess_challenge', $oOrder->getId());

        // skip some checks already done on first finalizeOrder():
        $oOrder->crefopaySetFinalizeCalledFromCallback(true);
        $iFinalizeOrderResult = $oOrder->finalizeOrder($oBasket, $oUser);
        if ($iFinalizeOrderResult === Order::ORDER_STATE_OK) {
            $logger->debug("finalizeOrder() succeeded, oxordernr: " . print_r($oOrder->oxorder__oxordernr->value, true));
            $iSuccess = true;
        } else {
            $logger->error("finalizeOrder() failed for cpOrderId: " . $cpTransaction->getCrefopayOrderId() . ' with code: ' . (int)$iFinalizeOrderResult);
            $iSuccess = false;
        }

        // performing special actions after user finishes order (assignment to special user groups)
        $logger->debug('finalizeCrefopayOrder: calling onOrderExecute()');
        $oUser->onOrderExecute($oBasket, $iSuccess);

        $oBasket->setOrderId($oOrder->getId());

        // Update transaction:
        $logger->debug('finalizeCrefopayOrder: Assigning transaction to orderid: ' . $oOrder->getId() . ', oxordernr: ' . $oOrder->oxorder__oxordernr->value);
        $cpTransaction->setMerchantReference($oOrder->oxorder__oxordernr->value);
        $cpTransaction->setOxidOrderId($oOrder->getId());
        $cpTransaction->save();

        if ($createSource === 'backend') {
            $oSession->setUser($prev_user);
            $oSession->setBasket($prev_basket);
            Registry::getConfig()->setAdminMode($prev_isAdmin);
        }

        if ($oOrder->getId()) {
            $oOrder->setCrefopayCreateSource($createSource);
            $oOrder->save();
        }

        $logger->debug('finalizeCrefopayOrder returns with:' . print_r($iSuccess, true));

        $cpTransaction->exitLock();

        return $iSuccess;
    }
}
