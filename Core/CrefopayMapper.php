<?php

namespace Crefopay\Payments\Core;

use OxidEsales\Eshop\Core\Registry;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\Request\Objects\Person;

class CrefopayMapper
{
    public static function filterOxidSalutation($oxsal)
    {
        $aOxidPossibleValues = ['MR', 'MRS', 'DIV'];
        if (in_array($oxsal, $aOxidPossibleValues)) {
            return $oxsal;
        }

        return '';
    }

    public static function mapSalutationToCrefopay($oxsal, &$salutationWarning)
    {
        $salutationWarning = '';
        if (!$oxsal) {
            $salutationWarning = "oxsal is empty";
            return null;
        }

        $oLang = Registry::getLang();

        $aPossibleValues = [
            'MR' => Person::SALUTATIONMALE,
            'MRS' => Person::SALUTATIONFEMALE,
            'DIV' => Person::SALUTATIONVARIOUS
        ];

        foreach ($aPossibleValues as $possibleOxsal => $cpValue) {
            if ($oxsal === $possibleOxsal) {
                return $cpValue;
            }

            $possibleOxsal2 = $oLang->translateString($oxsal);
            if ($oLang->isTranslated() && $oxsal === $possibleOxsal2) {
                return $cpValue;
            }
        }

        $salutationWarning = "Could not map '" . print_r($oxsal, true) . "' - expected 'MR' or 'MRS'";

        return null;
    }

    public static function getCrefopayLocale($iLangId = null)
    {
        $langMapping = [
            'de' => Codes::LOCALE_DE,
            'en' => Codes::LOCALE_EN,
            'es' => Codes::LOCALE_ES,
            'fi' => Codes::LOCALE_FI,
            'fr' => Codes::LOCALE_FR,
            'it' => Codes::LOCALE_IT,
            'nl' => Codes::LOCALE_NL,
            'tu' => Codes::LOCALE_TU,
            'ru' => Codes::LOCALE_RU,
            'pt' => Codes::LOCALE_PT,
        ];

        $oLang = Registry::getLang();
        $langAbbr = $oLang->getLanguageAbbr($iLangId);

        if (isset($langMapping[$langAbbr])) {
            return $langMapping[$langAbbr];
        }

        // Fallback:
        return $langMapping['en'];
    }
}
