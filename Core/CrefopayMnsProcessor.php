<?php

namespace Crefopay\Payments\Core;

use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use CrefoPay\Library\Mns\ProcessorInterface;

class CrefopayMnsProcessor implements ProcessorInterface
{
    private $saveData = [];

    public function sendData($additionalData, $amount, $captureID, $captureStatus, $currency, $merchantID, $merchantReference, $orderID, $orderStatus, $paymentReference, $storeID, $subscriptionId, $timestamp, $transactionBalance, $transactionStatus, $userID, $version)
    {
        // incoming timestamp is in ms
        $unixTimestampSeconds = intval($timestamp / '1000');

        $this->saveData['OXID'] = Registry::getUtilsObject()->generateUId();
        $this->saveData['merchantID'] = $merchantID;
        $this->saveData['storeID'] = $storeID;
        $this->saveData['orderID'] = $orderID;
        $this->saveData['captureID'] = $captureID;
        $this->saveData['merchantReference'] = $merchantReference;
        $this->saveData['paymentReference'] = $paymentReference;
        $this->saveData['userID'] = $userID;
        $this->saveData['amount'] = $amount;
        $this->saveData['currency'] = $currency;
        $this->saveData['transactionStatus'] = $transactionStatus;
        $this->saveData['orderStatus'] = $orderStatus;
        $this->saveData['additionalData'] = $additionalData;
        $this->saveData['timestamp'] = date("Y-m-d H:i:s", $unixTimestampSeconds);
        $this->saveData['version'] = $version;

        CrefopayHelpers::getLogger()->debug('received mns: ' . json_encode($this->saveData));
    }

    public function run()
    {
        $sqlArgs = [];
        $sql = "INSERT INTO crefopay_mns SET ";
        $x = 0;
        foreach ($this->saveData as $fieldName => $fieldVal) {
            if ($x > 0) {
                $sql .= ", ";
            }
            $sql .= '`' . $fieldName . '`=';
            $sql .= "?";
            $sqlArgs[] = $fieldVal;
            $x++;
        }

        DatabaseProvider::getDb()->execute($sql, $sqlArgs);
        CrefopayHelpers::getLogger()->debug('mns: run() called!');
    }

    public function processMNS()
    {
        $sql = "SELECT * FROM crefopay_mns WHERE is_processed=0 ORDER BY timestamp DESC LIMIT 10";
        $rows = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->getAll($sql);
        foreach ($rows as $row) {
            $cpOrderId = $row['orderID'];
            $captureID = $row['captureID'];

            CrefopayHelpers::getLogger()->debug("MNS: Processing '" . $row['OXID'] . "' for cpOrderId='" . $cpOrderId . "', captureId='" . $captureID . "'");

            try {
                if ($cpOrderId) {
                    $cpTransaction = CrefopayTransaction::getTransactionByCrefopayOrderId($cpOrderId);
                    if ($cpTransaction) {
                        if ($cpTransaction->hasBasketZeroSum()) {
                            CrefopayHelpers::getLogger()->debug("MNS: Ignoring for cpOrderId: '" . $cpOrderId . "' - was zero basket");
                        } else {
                            CrefopayHelpers::getLogger()->debug("MNS: Updating cpOrderId: '" . $cpOrderId . "'");
                            $cpTransaction->refreshTransaction(); // This also updates the transaction state
                            $cpTransaction->setCrefopayPaymentReference($row['paymentReference']);
                            $cpTransaction->setCrefopayOrderStatus($row['orderStatus']);
                            if ($row['orderStatus'] === 'PAID') {
                                $cpTransaction->setPaidDateSQL($row['timestamp']);
                            }
                            $cpTransaction->save();

                            if (CrefopayTransactionStatus::shouldOrderExistStatus($cpTransaction->getCrefopayTransactionStatus())) {
                                $sql = "INSERT INTO crefopay_checkorderexists SET CPORDERID=?, TSTAMP_ADDED=NOW(), ORDER_STATE=0 ON DUPLICATE KEY UPDATE TSTAMP_UPDATED=NOW()";
                                DatabaseProvider::getDb()->execute($sql, [
                                    $cpOrderId
                                ]);
                            }

                            if ($row['orderStatus'] === 'PAID') {
                                $oCapture = $cpTransaction->getCaptureByCaptureId($captureID);
                                if ($oCapture) {
                                    $oCapture->setIsPaid(true);
                                    $oCapture->setPaidDateSQL($row['timestamp']);
                                    $oCapture->markAsPaid();
                                    $oCapture->save();

                                    CrefopayHelpers::getLogger()->debug("Set paid-date for captureID '" . $captureID . "' for cp-order '" . $cpTransaction->getCrefopayOrderId() . "' to " . $row['timestamp']);
                                } else {
                                    CrefopayHelpers::getLogger()->debug("Failed to set paid-date for captureID '" . $captureID . "' for cp-order '" . $cpTransaction->getCrefopayOrderId() . "' - captureId not found!");
                                }

                                $cpTransaction->markAsPaid($row['timestamp']);
                            }
                        }
                    } else {
                        CrefopayHelpers::getLogger()->debug('MNS: No transaction found for cpOrderId: ' . $cpOrderId);
                    }
                } else {
                    CrefopayHelpers::getLogger()->debug('MNS: Invalid cpOrderId: ' . print_r($cpOrderId, true));
                }
            } catch (\Exception $ex) {
                CrefopayHelpers::getLogger()->error("MNS: Processing failed for oxid='" . $row['OXID'] . "',cpOrderId='" . $cpOrderId . "'");
                CrefopayHelpers::getLogger()->error($ex);
            }

            DatabaseProvider::getDb()->execute(
                "UPDATE crefopay_mns SET is_processed=1 WHERE OXID=?",
                array(
                (string)$row['OXID']
            )
            );
        }
    }

    public function checkForMissingOrders()
    {
        // Check for missing orders with a 15min delay:
        $sql = "SELECT * FROM crefopay_checkorderexists WHERE TSTAMP_ADDED<(NOW() - INTERVAL 15 MINUTE) AND ORDER_STATE=0 ORDER BY TSTAMP_ADDED ASC LIMIT 10";
        $rows = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->getAll($sql);
        foreach ($rows as $row) {
            $cpOrderId = $row['CPORDERID'];

            $cpTransaction = CrefopayTransaction::getTransactionByCrefopayOrderId($cpOrderId);
            if ($cpTransaction) {
                $oxidOrderId = $cpTransaction->getOxidOrderId();
                CrefopayHelpers::getLogger()->debug("CP-Order '" . $cpTransaction->getCrefopayOrderId() . "' should exist: " . print_r($oxidOrderId, true));

                $bOrderNeedsToBeCreated = false;
                if (!$oxidOrderId) {
                    $bOrderNeedsToBeCreated = true;
                    CrefopayHelpers::getLogger()->debug("CP-Order '" . $cpTransaction->getCrefopayOrderId() . "' has no oxorderid assigned.");
                } else {
                    $oOrder = oxNew(Order::class);
                    if (!$oOrder->exists($oxidOrderId)) {
                        $bOrderNeedsToBeCreated = true;
                        CrefopayHelpers::getLogger()->debug("CP-Order '" . $cpTransaction->getCrefopayOrderId() . "' does not exist: " . print_r($oxidOrderId, true));
                    } else {
                        CrefopayHelpers::getLogger()->debug("CP-Order '" . $cpTransaction->getCrefopayOrderId() . "' does exist: " . print_r($oxidOrderId, true));
                    }
                }

                if ($bOrderNeedsToBeCreated) {
                    // Mark as creating:
                    DatabaseProvider::getDb()->execute("UPDATE crefopay_checkorderexists SET ORDER_STATE=2 WHERE CPORDERID=?", [$cpOrderId]);

                    CrefopayHelpers::getLogger()->debug("Creating order '" . print_r($oxidOrderId, true) . "' for CP-Order '" . $cpTransaction->getCrefopayOrderId());
                    $this->createOxidOrder($cpTransaction);
                    if ($cpTransaction->getCrefopayOrderStatus() === 'PAID') {
                        $cpTransaction->markAsPaid($cpTransaction->getPaidDateTS());
                    }

                    // Mark as created:
                    DatabaseProvider::getDb()->execute("UPDATE crefopay_checkorderexists SET ORDER_STATE=3 WHERE CPORDERID=?", [$cpOrderId]);
                } else {
                    // Mark as already created before:
                    DatabaseProvider::getDb()->execute("UPDATE crefopay_checkorderexists SET ORDER_STATE=1 WHERE CPORDERID=?", [$cpOrderId]);
                }
            } else {
                CrefopayHelpers::getLogger()->debug("Failed to load transaction for cporder '" . print_r($cpOrderId, true) . "'");

                // Mark as failed:
                DatabaseProvider::getDb()->execute("UPDATE crefopay_checkorderexists SET ORDER_STATE=-1 WHERE CPORDERID=?", [$cpOrderId]);
            }
        }
    }

    private function createOxidOrder(CrefopayTransaction $cpTransaction)
    {
        // save current values:
        $oSession = Registry::getSession();
        $oSavedUser = $oSession->getUser();
        $oSavedBasket = $oSession->getBasket();

        // Create order:
        $oOrderCreator = oxNew(CrefopayOrderCreator::class);
        $oOrderCreator->finalizeCrefopayOrder($cpTransaction, 'MNS');

        // Restore previous values:
        $oSession = Registry::getSession();
        $oSession->setUser($oSavedUser);
        $oSession->setBasket($oSavedBasket);
        $oSession->deleteVariable('crefopayOrderId');
    }
}
