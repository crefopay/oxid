<?php

namespace Crefopay\Payments\Core;

class CrefopayRiskClass
{
    public const RiskTrusted = 0;

    public const RiskDefault = 1;

    public const RiskHigh = 2;
}
