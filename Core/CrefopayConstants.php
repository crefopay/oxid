<?php

namespace Crefopay\Payments\Core;

class CrefopayConstants
{
    public const DOBUSAGE_ASK_AND_SAVE = 0;

    public const DOBUSAGE_ASK_AND_DONTSAVE = 1;

    public const DOBUSAGE_DONTASK = 2;
}
