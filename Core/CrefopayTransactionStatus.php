<?php

namespace Crefopay\Payments\Core;

class CrefopayTransactionStatus
{
    /** @var string Internal status. Means that a transaction has been created within the CrefoPay system after receiving the user and basket data from the shop system during checkout */
    public const TransactionStatusNEW = 'NEW';

    /** @var string Internal status. The user has confirmed the purchase during checkout by pressing button "Buy" and in case of redirect payment methods CrefoPay is waiting for the response from the shop which contains the success-URL. */
    public const ACKNOWLEDGEPENDING = 'ACKNOWLEDGEPENDING';

    /** @var string This status indicates, that the transaction is suspect due to fraud suspicions and needs to be checked manually. */
    public const FRAUDPENDING = 'FRAUDPENDING';

    /** @var string The transaction was created successfully and CrefoPay is waiting for money to arrive in the merchant funds account. Relevant for cash in advance and PayPal with IPN. */
    public const CIAPending = 'CIAPENDING';

    /** @var string The transaction was successful. CrefoPay is waiting for the merchant to capture the transaction when delivering the order or immediately after checkout process. */
    public const MERCHANTPENDING = 'MERCHANTPENDING';

    /** @var string */
    public const INPROGRESS = 'INPROGRESS';

    /** @var string The transaction was not completed by the user within the duration of the basket-validity. */
    public const EXPIRED = 'EXPIRED';

    /** @var string The transaction was cancelled by the merchant */
    public const CANCELLED = 'CANCELLED';

    /** @var string The transaction was cancelled because of fraud suspicion by CrefoPay’s fraud team. */
    public const FRAUDCANCELLED = 'FRAUDCANCELLED';

    /** @var string The transaction has been completed. A transaction will change into this status if all of its captures have the status paid. */
    public const DONE = 'DONE';

    /**
     * Returns true if the oxid order should be created in that state
     * @param $cpTransactionStatus
     * @return bool
     */
    public static function shouldOrderExistStatus($cpTransactionStatus)
    {
        $validStates = [
            self::ACKNOWLEDGEPENDING,
            self::CIAPending,
            self::DONE,
            self::INPROGRESS,
            self::MERCHANTPENDING
        ];

        return in_array($cpTransactionStatus, $validStates);
    }
}
