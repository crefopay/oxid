<?php

namespace Crefopay\Payments\Models;

use CrefoPay\Library\Request\Objects\BasketItem;
use OxidEsales\Eshop\Core\Registry;
use CrefoPay\Library\Request\AbstractRequest;
use CrefoPay\Library\Request\Attributes\ObjectArray;
use CrefoPay\Library\Request\Objects\AbstractObject;

class CrefopayTransactionJsonGenerator
{
    public static function generateJSON($cpOrderId)
    {
        $oBasket = Registry::getSession()->getBasket();
        if (!$oBasket) {
            return false;
        }

        $oBasket->calculateBasket();

        $referenceTransaction = oxNew(CrefopayTransaction::class);
        $referenceRequest = $referenceTransaction->createTransactionRequest(true, $cpOrderId);
        if (!$referenceRequest) {
            return false;
        }

        $rv = [];
        $rv['userId'] = self::serializeToArray($referenceRequest->getUserId());
        $rv['billingRecipient'] = self::serializeToArray($referenceRequest->getBillingRecipient());
        $rv['billingAddress'] = self::serializeToArray($referenceRequest->getBillingAddress());
        $rv['shippingRecipient'] = self::serializeToArray($referenceRequest->getShippingRecipient());
        $rv['shippingAddress'] = self::serializeToArray($referenceRequest->getShippingAddress());
        $rv['companyData'] = self::serializeToArray($referenceRequest->getCompanyData());
        $rv['amount'] = self::serializeToArray($referenceRequest->getAmount());

        $aBasketItems = [];
        $basketContents = $referenceRequest->getBasketItems();

        /**
         * @var string $sBasketItemKey
         * @var BasketItem $oBasketItem
         */
        foreach ($basketContents as $oBasketItem) {
            $aBasketItem = [];
            $aBasketItem['text'] = $oBasketItem->getBasketItemText();
            $aBasketItem['price'] = $oBasketItem->getBasketItemAmount()->getNetAmount();
            $aBasketItem['count'] = $oBasketItem->getBasketItemCount();

            $aBasketItems[] = $aBasketItem;
        }

        usort($aBasketItems, function ($a, $b) {
            $cmpFields = ['text', 'price', 'count'];

            foreach ($cmpFields as $cmpField) {
                $cmpval = strcmp($a[$cmpField], $b[$cmpField]);
                if ($cmpval !== 0) {
                    return $cmpval;
                }
            }

            return 0;
        });
        $rv['basketItems'] = $aBasketItems;

        return json_encode($rv);
    }

    private static function serializeToArray($obj)
    {
        if ($obj === null) {
            return '';
        }

        if ($obj instanceof ObjectArray) {
            $rv = [];
            $items = $obj->toArray();
            foreach ($items as $item) {
                $rv[] = self::serializeToArray($item);
            }
            return $rv;
        }

        if (($obj instanceof AbstractRequest) || ($obj instanceof AbstractObject)) {
            $rv = [];

            $skipKeys = ["salt"];

            $items = $obj->toArray();
            $keys = array_keys($items);
            sort($keys);
            foreach ($keys as $key) {
                if (!in_array($key, $skipKeys)) {
                    $rv[$key] = self::serializeToArray($items[$key]);
                }
            }

            return $rv;
        }

        if (is_scalar($obj)) {
            return $obj;
        }

        if (is_object($obj)) {
            throw new \RuntimeException("Unhandled class: " . get_class($obj));
        }

        throw new \RuntimeException("unhandled type: " . gettype($obj));
    }
}
