<?php

namespace Crefopay\Payments\Models;

use CrefoPay\Library\Request\Objects\SolvencyData;
use CrefoPay\Library\Response\Unserializer\Handler\RiskData;
use CrefoPay\Library\Validation\Validation;
use Crefopay\Payments\Core\CrefopayConstants;
use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Core\CrefopayMapper;
use Crefopay\Payments\Core\CrefopayRiskClass;
use Crefopay\Payments\Core\CrefopayTransactionStatus;
use Crefopay\Payments\Extend\Model\CrefopayOrder;
use Crefopay\Payments\Extend\Model\CrefopayPayment;
use DateTime;
use Exception;
use OxidEsales\Eshop\Application\Model\Basket;
use OxidEsales\Eshop\Application\Model\BasketItem;
use OxidEsales\Eshop\Application\Model\Country;
use OxidEsales\Eshop\Application\Model\DeliverySet;
use OxidEsales\Eshop\Application\Model\Order;
use OxidEsales\Eshop\Application\Model\Payment;
use OxidEsales\Eshop\Application\Model\State;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\Counter;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Model\BaseModel;
use OxidEsales\Eshop\Core\Price;
use OxidEsales\Eshop\Core\Registry;
use CrefoPay\Library\Basket\BasketItemType;
use CrefoPay\Library\Integration\Type;
use CrefoPay\Library\Request\AbstractRequest;
use CrefoPay\Library\Request\Capture;
use CrefoPay\Library\Request\CreateTransaction;
use CrefoPay\Library\Request\GetTransactionPaymentInstruments;
use CrefoPay\Library\Request\GetTransactionStatus;
use CrefoPay\Library\Request\GetUserPaymentInstrument;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\Company;
use CrefoPay\Library\Request\Objects\PaymentInstrument;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\Reserve;
use CrefoPay\Library\Request\UpdateTransactionData;
use CrefoPay\Library\Response\FailureResponse;
use CrefoPay\Library\Response\SuccessResponse;

class CrefopayTransaction extends BaseModel
{
    /**
     * Not redirected to payment provider yet or not required
     */
    public const REDIRECTSTATUS_NONE = 0;

    /**
     * Customer was redirected to payment provider (eg paypal)
     * but was not redirect back to shop yet
     */
    public const REDIRECTSTATUS_REDIRECTED = 1;

    /**
     * Customer was redirect to payment provider and has returned from payment provider
     */
    public const REDIRECTSTATUS_RETURNED = 2;

    public function __construct()
    {
        parent::__construct();
        $this->init('crefopay_transactions');

        $this->_aSkipSaveFields[] = 'cpcreated';
        $this->_aSkipSaveFields[] = 'cpupdated';
    }

    private function getRawField($sFieldName)
    {
        $sLongFieldName = $this->_getFieldLongName($sFieldName);

        if (isset($this->{$sLongFieldName})) {
            $fieldData = $this->{$sLongFieldName};
            if ($fieldData instanceof Field) {
                $val = $fieldData->rawValue;

                // Fix for MariaDB empty default-value issue with some oxid versions:
                // (causes quotes to be saved instead of empty string)
                if (is_string($val)) {
                    $val = trim($val, " \"'"); // remove surrounding quotes
                }

                return $val;
            }
        }

        return null;
    }

    private function setRawField($sFieldName, $sFieldValue)
    {
        $sLongFieldName = $this->_getFieldLongName($sFieldName);

        $oField=new Field($sFieldValue);
        $oField->rawValue=$sFieldValue;
        unset($oField->value);

        $this->{$sLongFieldName} = $oField;
    }

    public static function getTransactionByOxidOrderId($oxorderid)
    {
        if ($oxorderid) {
            $oxid = DatabaseProvider::getDb()->getOne(
                "SELECT OXID FROM crefopay_transactions WHERE OXORDERID=?",
                array(
                    (string)$oxorderid
                )
            );

            if ($oxid) {
                /** @var CrefopayTransaction $cpTransaction */
                $cpTransaction = oxNew(__CLASS__);
                if ($cpTransaction->load($oxid)) {
                    return $cpTransaction;
                }
            }
        }

        return false;
    }

    public static function getTransactionByCrefopayOrderId($cpOrderId)
    {
        /** @var CrefopayTransaction $cpTransaction */
        $cpTransaction = oxNew(__CLASS__);

        $oxid = DatabaseProvider::getDb()->getOne(
            "SELECT OXID FROM crefopay_transactions WHERE CPORDERID=?",
            array(
                (string)$cpOrderId
            )
        );

        if ($oxid && $cpTransaction->load($oxid)) {
            return $cpTransaction;
        }

        return false;
    }

    /** @var null|CrefopayTransaction */
    private static $activeTransaction;

    public static $activeTransactionIsNew = false;

    public static function getActiveTransaction($bUpdateTransaction = false, $bIgnoreErrors = false)
    {
        $oSession = Registry::getSession();
        if (!$oSession->isSessionStarted()) {
            $oSession->start();
        }

        self::$activeTransactionIsNew = true;

        if (self::$activeTransaction === null) {
            $cpOrderId = $oSession->getVariable('crefopayOrderId');
            if ($cpOrderId) {
                CrefopayHelpers::getLogger()->debug('Trying to load transaction for cp-order-id ' . $cpOrderId . ' (stored in session)');
                self::$activeTransaction = self::getTransactionByCrefopayOrderId($cpOrderId);
            } else {
                CrefopayHelpers::getLogger()->debug('No cp-order-id ' . $cpOrderId . ' stored in session!');
            }
        } else {
            CrefopayHelpers::getLogger()->debug('Trying to load previously loaded transaction');
        }

        $useCrefopayOrderId = null;

        if (self::$activeTransaction instanceof self) {
            if (self::$activeTransaction->getCrefopayOrderId()) {
                $bActiveTransactionIsOutdated = self::$activeTransaction->isTransactionOutdated();
                if ($bActiveTransactionIsOutdated) {
                    if (self::$activeTransaction->getCrefopayTransactionStatus() === CrefopayTransactionStatus::TransactionStatusNEW) {
                        // re-use crefopay-orderid
                        $useCrefopayOrderId = self::$activeTransaction->getCrefopayOrderId();
                    }
                }

                self::$activeTransaction->setOxidSessionId($oSession->getId());

                $bUseExistingTransaction = true;
                if ($bUpdateTransaction) {
                    $bUseExistingTransaction = !$bActiveTransactionIsOutdated;
                }

                if ($bUseExistingTransaction) {
                    CrefopayHelpers::getLogger()->debug('Existing transaction is usable');
                    self::$activeTransactionIsNew = false;

                    return self::$activeTransaction;
                }

                CrefopayHelpers::getLogger()->debug('Existing transaction is outdated');
            } else {
                CrefopayHelpers::getLogger()->debug('Existing transaction has no order-id');
            }
        } else {
            CrefopayHelpers::getLogger()->debug('No existing transaction');
        }

        CrefopayHelpers::getLogger()->debug('creating new transaction');
        self::$activeTransaction = oxNew(__CLASS__);
        self::$activeTransaction->createTransaction($bIgnoreErrors, $useCrefopayOrderId);
        $oSession->setVariable('crefopayOrderId', self::$activeTransaction->getCrefopayOrderId());

        return self::$activeTransaction;
    }

    public function isSaved()
    {
        return $this->getId() ? true : false;
    }

    /**
     * @param Order|CrefopayOrder $oOrder
     * @param Basket $oBasket
     * @param User $oUser
     */
    public function setObjectsToSerialize(Order $oOrder, Basket $oBasket, User $oUser)
    {
        // Force orderid:
        if (!$oOrder->getId()) {
            $oOrder->setId();
        }

        $serData = [
            'order' => $oOrder,
            'basket' => $oBasket,
            'user' => $oUser
        ];

        $sSerializedOrder = serialize($serData);
        $sSerializedOrderBase64 = base64_encode($sSerializedOrder);

        $this->setRawField('OXORDERID', $oOrder->getId());
        $this->setRawField('OXORDERSERIALIZED', $sSerializedOrderBase64);
    }

    public function clearFieldsAfterOrderCreation()
    {
        // Only to save database space
        $this->setRawField('OXORDERSERIALIZED', null);
        $this->setRawField('CPTRANSACTIONJSON', '');
    }

    public function getSerializedObjects()
    {
        $sSerializedBase64 = $this->getRawField('OXORDERSERIALIZED');
        if ($sSerializedBase64) {
            $sSerializedOrder = base64_decode($sSerializedBase64);
            if ($sSerializedOrder) {
                // This should get restricted for security reasons
                // but we cannot as we cannot know here which classes may be
                // involed in the object :(
                // Should not be a real issue as this cannot be accessed from outside
                $aUnserialized = unserialize($sSerializedOrder);
                if (isset($aUnserialized['order'])) {
                    $aUnserialized['order']->setId($this->getOxidOrderId());
                }

                return $aUnserialized;
            }

            CrefopayHelpers::getLogger()->error('Failed to unserialize: ' . print_r($sSerializedOrder, true));
        } else {
            CrefopayHelpers::getLogger()->error('no data  to unserialize');
        }

        return false;
    }

    public function setRedirectStatus($redirectStatus)
    {
        $this->setRawField('CPREDIRECTSTATUS', $redirectStatus);
    }

    public function getRedirectStatus()
    {
        return $this->getRawField('CPREDIRECTSTATUS');
    }

    public function getCustomerNameForBackend()
    {
        $jsonDataStr = $this->getCrefopayTransactionJSON();
        $jsonData = @json_decode($jsonDataStr, true);
        if ($jsonData) {
            return $jsonData['billingRecipient'];
        }

        return '';
    }

    public function getCrefopayTransactionJSON()
    {
        return $this->getRawField('CPTRANSACTIONJSON');
    }

    private function setCrefopayTransactionJSON($cpTransactionJSON)
    {
        $this->setRawField('CPTRANSACTIONJSON', $cpTransactionJSON);
    }

    /**
     * @return bool
     */
    public function isTransactionOutdated()
    {
        CrefopayHelpers::getLogger()->beginSilence();
        $transactionJSON = CrefopayTransactionJsonGenerator::generateJSON($this->getCrefopayOrderId());
        CrefopayHelpers::getLogger()->endSilence();

        if (!$transactionJSON) {
            return true;
        }
        CrefopayHelpers::getLogger()->debug("cpTransJson: " . $this->getCrefopayTransactionJSON() . ", generierte JSON: " . $transactionJSON);
        if ($this->getCrefopayTransactionJSON() !== $transactionJSON) {
            return true;
        }

        return false;
    }

    public function setOxidSessionId($oxidSessionId)
    {
        if ($this->getOxidSessionId() !== $oxidSessionId) {
            $this->setRawField('OXSESSIONID', $oxidSessionId);
            $this->save();
        }
    }

    public function getOxidSessionId()
    {
        return $this->getRawField('OXSESSIONID');
    }

    public function getOxidOrderId()
    {
        return $this->getRawField('OXORDERID');
    }

    public function getOrderFromDb()
    {
        $oxorderid = $this->getOxidOrderId();
        if ($oxorderid) {
            $oOrder = oxNew(Order::class);
            if ($oOrder->load($oxorderid)) {
                return $oOrder;
            }
        }

        return false;
    }

    public function setOxidOrderId($oxorderid)
    {
        CrefopayHelpers::getLogger()->debug("setOxidOrderId() for '" . $this->getId() . "' to '" . $oxorderid . "'");
        $this->setRawField('OXORDERID', $oxorderid);
    }

    public function getOxidOrderNr()
    {
        $oxordernr = (int)$this->getRawField('OXORDERNR');
        if ($oxordernr) {
            return $oxordernr;
        }

        // Fallback:
        $oxorderid = $this->getOxidOrderId();
        if ($oxorderid) {
            $oOrder = oxNew(Order::class);
            if ($oOrder->load($oxorderid)) {
                $this->setOxidOrderNr($oOrder->oxorder__oxordernr->value);
                $this->save();

                return (int)$oOrder->oxorder__oxordernr->value;
            }
        }

        return 0;
    }

    public function setOxidOrderNr($oxordernr)
    {
        CrefopayHelpers::getLogger()->debug("setOxidOrderNr() for '" . $this->getId() . "' to '" . $oxordernr . "'");

        $this->setRawField('OXORDERNR', $oxordernr);
    }

    public function setOxidUserId($oxuserid)
    {
        $this->setRawField('OXUSERID', $oxuserid);
    }

    public function getOxidUserId()
    {
        return (string)$this->getRawField('OXUSERID');
    }

    public function setCrefopayUserId($cpUserId)
    {
        $this->setRawField("CPUSERID", $cpUserId);
    }

    public function getCrefopayTransactionStatus()
    {
        $cpTransactionStatus = $this->getRawField("CPTRANSACTIONSTATUS");
        if (!$cpTransactionStatus) {
            $this->setCrefopayTransactionStatus('NEW');

            return 'NEW';
        }

        return $cpTransactionStatus;
    }

    public function setCrefopayTransactionStatus($cpTransactionStatus)
    {
        if ($cpTransactionStatus) {
            $prevStatus = $this->getRawField("CPTRANSACTIONSTATUS");
            if ($cpTransactionStatus !== $prevStatus) {
                CrefopayHelpers::getLogger()->debug("setCrefopayTransactionStatus(): Setting '" . $cpTransactionStatus . "', was: '" . $prevStatus . "'");
                $this->setRawField("CPTRANSACTIONSTATUS", $cpTransactionStatus);

                if ($cpTransactionStatus === 'ACKNOWLEDGEPENDING') {
                    $sOxidOrderId = $this->getOxidOrderId();
                    if ($sOxidOrderId) {
                        $oOrder = oxNew(Order::class);
                        if ($oOrder->load($sOxidOrderId)) {
                            $this->setMerchantReference($oOrder->oxorder__oxordernr->value);
                        }
                    }
                }
            }
        }
    }

    /**
     * Additional information will be set only in case of PREPAID, BILL and BILL_SECURE Payments
     * (Should only be called for the result of reserve())
     * @param $cpAdditionalData
     * @throws Exception
     */
    public function setCrefopayAdditionalData($cpAdditionalData)
    {
        if (is_array($cpAdditionalData)) {
            $this->setRawField("CPADDITIONALDATA", json_encode($cpAdditionalData));
        } else {
            throw new \RuntimeException('expected cpAdditionalData-argument to be an array!');
        }
    }

    public function getCrefopayAdditionalData()
    {
        $json = $this->getRawField('CPADDITIONALDATA');
        if ($json) {
            return json_decode($json, true);
        }

        return [];
    }

    /**
     * @param $cpUserData Person
     */
    public function setCrefopayCustomerData(Person $cpUserData)
    {
        $cpUserDataJSON = json_encode($cpUserData->toArray(), JSON_PRETTY_PRINT);

        $this->setRawField('CPCUSTOMERDATA', $cpUserDataJSON);
    }

    /**
     * @param Price|float $oPrice
     * @return Amount
     */
    private function toAmount($price)
    {
        if ($price instanceof Price) {
            /** @var Price $oPrice */
            $oPrice = $price;
            $inclTaxCents = $oPrice->getBruttoPrice() * 100;
            $exclTaxCents = $oPrice->getNettoPrice() * 100;

            $inclTaxCents = (int)round($inclTaxCents, 0);
            $exclTaxCents = (int)round($exclTaxCents, 0);

            return new Amount($inclTaxCents, $exclTaxCents);
        }

        if (is_float($price)) {
            $inclTaxCents = $price * 100;
            $inclTaxCents = (int)round($inclTaxCents, 0);

            return new Amount($inclTaxCents);
        }

        throw new \RuntimeException("Expected float or Price-object!");
    }

    private function generateCrefopayOrderId()
    {
        /** @var Counter $oCounter */
        $oCounter = oxNew(Counter::class);
        $cpOrderId = $oCounter->getNext('cppayments_orderid');

        $cpOrderIdPrefix = CrefopayHelpers::getConfigParam('CrefoPayOrderPrefix');
        if ($cpOrderIdPrefix) {
            $cpOrderId = $cpOrderIdPrefix . $cpOrderId;
        }

        $fill_len = 19 - strlen($cpOrderId);
        if ($fill_len > 0) {
            $cpOrderId .= '_' . CrefopayHelpers::getRandomString($fill_len);
        }

        return $cpOrderId;
    }

    public function refreshTransaction()
    {
        if ($this->getCrefopayOrderId()) {
            // getTransactionStatus()
            {
                $request = new GetTransactionStatus(CrefopayHelpers::getAPIConfig());
                $request->setOrderID($this->getCrefopayOrderId());
                $request->setReturnRiskData(true);

                $response = CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\GetTransactionStatus::class, $request, true);
                if ($response instanceof SuccessResponse) {
                    $transactionStatus = $response->getData('transactionStatus');
                    if ($transactionStatus) {
                        $this->setCrefopayTransactionStatus($transactionStatus);
                        $this->save();
                    }

                    /** @var RiskData $riskData */
                    $riskData = $response->getData('riskData');

                    if (is_array($riskData)) {
                        $this->setCrefopayRiskData($riskData);
                    } else {
                        $this->setCrefopayRiskData([]);
                    }
                }

                if ($response instanceof FailureResponse) {
                    $resultCode = (int)$response->getData('resultCode');

                    $aExpiredCodes = [2003, 2004];
                    if (in_array($resultCode, $aExpiredCodes)) {
                        CrefopayHelpers::getLogger()->debug("Changing transaction to expired for '" . $this->getCrefopayOrderId() . "' due to resultCode: " . $resultCode);
                        $this->setCrefopayTransactionStatus('EXPIRED');
                        $this->save();
                    }
                }
            }

            // getTransactionPaymentInstruments
            $transactionStatus = $this->getCrefopayTransactionStatus();
            if ($transactionStatus === 'NEW') {
                $request = new GetTransactionPaymentInstruments(CrefopayHelpers::getAPIConfig());
                $request->setOrderID($this->getCrefopayOrderId());

                $response = CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\GetTransactionPaymentInstruments::class, $request, true);
                if ($response instanceof SuccessResponse) {
                    $allowedPaymentMethods = $response->getData('allowedPaymentMethods');
                    $this->setAllowedCrefopayPaymentMethods($allowedPaymentMethods);
                    $this->save();
                }
            }

            if ($transactionStatus !== 'NEW') {
                $paymentMethod = $this->getCrefopayPaymentMethod();
                if (($paymentMethod === 'CC') || ($paymentMethod === 'CC3D')) {
                    if (!$this->getCrefopayCCIssuer()) {
                        $request = new \CrefoPay\Library\Request\GetUserPaymentInstrument(CrefopayHelpers::getAPIConfig());
                        $request->setUserID($this->getCrefopayUserId());

                        $response = CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\GetUserPaymentInstrument::class, $request, true);
                        if ($response instanceof SuccessResponse) {
                            /** @var PaymentInstrument[] $paymentInstruments */
                            $paymentInstruments = $response->getData('paymentInstruments');
                            if (is_array($paymentInstruments)) {
                                foreach ($paymentInstruments as $paymentInstrument) {
                                    if ($paymentInstrument->getPaymentInstrumentType() === 'CREDITCARD') {
                                        $this->setCrefopayCCIssuer($paymentInstrument->getIssuer());
                                        $this->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function createTransaction($bIgnoreErrors = false, $useCrefopayOrderId = null)
    {
        $request = $this->createTransactionRequest(false, $useCrefopayOrderId);

        if ($request instanceof AbstractRequest) {
            $response = CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\CreateTransaction::class, $request, $bIgnoreErrors);
            if ($response instanceof SuccessResponse) {
                $this->setCrefopayTransactionJSON(CrefopayTransactionJsonGenerator::generateJSON($this->getCrefopayOrderId()));

                $allowedPaymentMethods = $response->getData('allowedPaymentMethods');
                $this->setAllowedCrefopayPaymentMethods($allowedPaymentMethods);

                $this->save();

                return true;
            }
        }

        $this->setId(null);

        return false;
    }

    public function createTransactionRequest($prepareOnly, $cpOrderId)
    {
        $oSession = Registry::getSession();
        $oUser = $oSession->getUser();
        if (!$oUser) {
            return false;
        }

        $oBasket = $oSession->getBasket();
        if (!$oBasket) {
            return false;
        }

        if ($cpOrderId === null) {
            $cpOrderId = $this->generateCrefopayOrderId();
        }

        if (!$prepareOnly) {
            $this->setId(md5($cpOrderId));
            $this->setCrefopayOrderId($cpOrderId);
            $this->setOxidSessionId($oSession->getId());
            $this->setOxidUserId($oUser->getId());
            $this->setCrefopayUserId(CrefopayHelpers::getCrefopayUserIdFromOxidUser($oUser));
            $this->setCrefopayCustomerData($this->getUserData($oUser));
        }

        $this->setCrefopayTransactionStatus('NEW');
        $this->setIsCaptured(0);

        $this->setCrefopayRiskClass(CrefopayRiskClass::RiskDefault);
        if ($oUser->oxuser__crefopay_userriskclass instanceof Field) {
            $this->setCrefopayRiskClass((int)$oUser->oxuser__crefopay_userriskclass->value);
        }

        // Has the session a risk class?
        $sessionRiskClass = $oSession->getVariable('crefopayRiskClass');
        if ($sessionRiskClass !== null) {
            $this->setCrefopayRiskClass($sessionRiskClass);
        }

        $request = new CreateTransaction(CrefopayHelpers::getAPIConfig());
        $request->setContext('ONLINE');
        $request->setOrderID($cpOrderId);
        $request->setUserID(CrefopayHelpers::getCrefopayUserIdFromOxidUser($oUser));
        $request->setIntegrationType(Type::INTEGRATION_TYPE_SECURE_FIELDS);
        $request->setUserRiskClass($this->getCrefopayRiskClass());

        if (CrefopayHelpers::getConfigBool('CrefoPayAutoCapture', false)) {
            $request->setAutoCapture(true);
        } else {
            $request->setAutoCapture(false);
        }

        $request->setLocale(CrefopayMapper::getCrefopayLocale());

        if (CrefopayHelpers::isB2BEnabledAndBusinessUser($oUser)) {
            $companyData = $this->getCompanyData($oUser);
            $request->setUserType(\CrefoPay\Library\User\Type::USER_TYPE_BUSINESS);
            $request->setCompanyData($companyData);
            $request->setBillingRecipient($companyData->getCompanyName());
        } else {
            $request->setUserType(\CrefoPay\Library\User\Type::USER_TYPE_PRIVATE);
            $request->setUserData($this->getUserData($oUser));
            $request->setBillingRecipient($this->getBillingRecipient($oUser));
        }

        $iCrefoPayBasketValRaw = (int)CrefopayHelpers::getConfigParam('CrefoPayBasketVal');
        $iCrefoPayBasketVal = max(1, $iCrefoPayBasketValRaw); // should be at least 1
        if ($iCrefoPayBasketValRaw !== $iCrefoPayBasketVal) {
            // autofix:
            CrefopayHelpers::setConfigParam('str', 'CrefoPayBasketVal', $iCrefoPayBasketVal);
        }

        $basketValidity = $iCrefoPayBasketVal . CrefopayHelpers::getConfigParam('CrefoPayBasketValUnit');
        if ($basketValidity) {
            $request->setBasketValidity($basketValidity);
        }

        $this->addBasketItems($request, $oBasket, $cpOrderId);

        $request->setBillingAddress($this->getBillingAddress($oUser));

        $deladrid = Registry::getSession()->getVariable('deladrid');
        if ($deladrid) {
            /** @var \OxidEsales\Eshop\Application\Model\Address $oDeliveryAddress */
            $oDeliveryAddress = oxNew(\OxidEsales\Eshop\Application\Model\Address::class);
            if ($oDeliveryAddress->load($deladrid)) {
                $request->setShippingRecipient($this->getShippingRecipient($oDeliveryAddress));
                $request->setShippingAddress($this->getShippingAddress($oDeliveryAddress));
            }
        }

        // pre-validation of phone-number:
        $oValidatorFactory = new Validation();
        $oValidation = $oValidatorFactory->getValidator($request);
        $validationResult = $oValidation->performValidation();
        foreach ($validationResult as $objName => $subValidatonResult) {
            foreach ($subValidatonResult as $fieldName => $errorData) {
                if ($fieldName === 'phoneNumber') {
                    // Phone-number is not in a format crefopay understands
                    // simply remove it (only optional field)
                    $request->getUserData()->setPhoneNumber('');
                }
            }
        }

        return $request;
    }

    /**
     * @param $request CreateTransaction
     * @param $oBasket Basket
     * @param $cpOrderId string
     */
    protected function addBasketItems($request, $oBasket, $cpOrderId)
    {
        $request->setAmount($this->toAmount($oBasket->getPrice()));
        $fTotalAmountOxid = $request->getAmount()->getAmount();

        $oLang = Registry::getLang();

        $bCrefoPayTransmitBasket = CrefopayHelpers::getConfigBool('CrefoPayTransmitBasket', false);

        if (!$bCrefoPayTransmitBasket) {
            $shopTitle = Registry::getConfig()->getActiveShop()->oxshops__oxname->rawValue;

            $cpBasketItem = new \CrefoPay\Library\Request\Objects\BasketItem();
            $cpBasketItem->setBasketItemAmount($this->toAmount($oBasket->getPrice()));
            $cpBasketItem->setBasketItemCount(1);
            $cpBasketItem->setBasketItemID('ORDER');
            $cpBasketItem->setBasketItemText(
                CrefopayHelpers::truncateString($cpOrderId . ' - ' . $shopTitle, 500)
            );
            $cpBasketItem->setBasketItemType(BasketItemType::BASKET_ITEM_TYPE_DEFAULT);
            $request->addBasketItem($cpBasketItem);
        } else {
            // Basket-Items:
            $aActualBasketItems = [];
            {
                $aBasketItems = $oBasket->getContents();

                /**
                 * @var string $sBasketItemKey
                 * @var BasketItem $oBasketItem
                 */
                foreach ($aBasketItems as $sBasketItemKey => $oBasketItem) {
                    $cpBasketItem = new \CrefoPay\Library\Request\Objects\BasketItem();
                    $cpBasketItem->setBasketItemAmount($this->toAmount($oBasketItem->getPrice()));
                    $cpBasketItem->setBasketItemCount($oBasketItem->getAmount());
                    $cpBasketItem->setBasketItemText(
                        CrefopayHelpers::truncateString(
                            CrefopayHelpers::htmlDecode($oBasketItem->getTitle()),
                            500
                        )
                    );
                    $cpBasketItem->setBasketItemType(BasketItemType::BASKET_ITEM_TYPE_DEFAULT);
                    $request->addBasketItem($cpBasketItem);

                    $aActualBasketItems[] = $cpBasketItem;
                }
            }

            // Discounts:
            {
                $oVoucherDiscount = $oBasket->getVoucherDiscount();
                if (($oVoucherDiscount) && ($oVoucherDiscount->getPrice() > 0)) {
                    $cpBasketItem = new \CrefoPay\Library\Request\Objects\BasketItem();
                    $cpBasketItem->setBasketItemAmount($this->toAmount($oVoucherDiscount));
                    $cpBasketItem->setBasketItemCount(1);
                    $cpBasketItem->setBasketItemText(
                        CrefopayHelpers::truncateString($oLang->translateString('COUPON'), 500)
                    );
                    $cpBasketItem->setBasketItemType(BasketItemType::BASKET_ITEM_TYPE_COUPON);
                    $request->addBasketItem($cpBasketItem);
                }
            }

            // Shipping:
            {
                $oDeliveryCostPrice = $oBasket->getDeliveryCost();
                if (($oDeliveryCostPrice) && ($oDeliveryCostPrice->getPrice() > 0)) {
                    $sDeliverySetTitle = $oLang->translateString('SHIPPING');
                    $sShippingId = $oBasket->getShippingId();

                    /** @var DeliverySet $oDeliverySet */
                    $oDeliverySet = oxNew(DeliverySet::class);
                    if ($oDeliverySet->load($sShippingId)) {
                        $sDeliverySetTitle = CrefopayHelpers::htmlDecode($oDeliverySet->getFieldData('OXTITLE'));
                    }

                    $cpBasketItem = new \CrefoPay\Library\Request\Objects\BasketItem();
                    $cpBasketItem->setBasketItemAmount($this->toAmount($oDeliveryCostPrice));
                    $cpBasketItem->setBasketItemCount(1);
                    $cpBasketItem->setBasketItemText(CrefopayHelpers::truncateString($sDeliverySetTitle, 500));
                    $cpBasketItem->setBasketItemType(BasketItemType::BASKET_ITEM_TYPE_SHIPPINGCOST);
                    $request->addBasketItem($cpBasketItem);
                }
            }

            // Payment Costs:
            {
                $oPaymentCosts = $oBasket->getPaymentCost();
                if (($oPaymentCosts) && ($oPaymentCosts->getPrice() > 0)) {
                    $cpBasketItem = new \CrefoPay\Library\Request\Objects\BasketItem();
                    $cpBasketItem->setBasketItemAmount($this->toAmount($oPaymentCosts));
                    $cpBasketItem->setBasketItemCount(1);
                    $cpBasketItem->setBasketItemText(
                        CrefopayHelpers::truncateString($oLang->translateString('PAYMENT_METHOD'), 500)
                    );
                    $cpBasketItem->setBasketItemType(BasketItemType::BASKET_ITEM_TYPE_DEFAULT);
                    $request->addBasketItem($cpBasketItem);
                }
            }

            // Wrapping Costs:
            {
                $oWrappingCost = $oBasket->getWrappingCost();
                if (($oWrappingCost) && ($oWrappingCost->getPrice() > 0)) {
                    $cpBasketItem = new \CrefoPay\Library\Request\Objects\BasketItem();
                    $cpBasketItem->setBasketItemAmount($this->toAmount($oWrappingCost));
                    $cpBasketItem->setBasketItemCount(1);
                    $cpBasketItem->setBasketItemText(
                        CrefopayHelpers::truncateString($oLang->translateString(
                            $oLang->translateString('GIFT_WRAPPING_GREETING_CARD')
                        ), 500)
                    );
                    $cpBasketItem->setBasketItemType(BasketItemType::BASKET_ITEM_TYPE_DEFAULT);
                    $request->addBasketItem($cpBasketItem);
                }
            }

            // Giftcard Costs:
            {
                $oGiftCardCosts = $oBasket->getGiftCardCost();
                if (($oGiftCardCosts) && ($oGiftCardCosts->getPrice() > 0)) {
                    $cpBasketItem = new \CrefoPay\Library\Request\Objects\BasketItem();
                    $cpBasketItem->setBasketItemAmount($this->toAmount($oGiftCardCosts));
                    $cpBasketItem->setBasketItemCount(1);
                    $cpBasketItem->setBasketItemText(CrefopayHelpers::truncateString(
                        $oLang->translateString('GIFT_WRAPPING_GREETING_CARD'),
                        500
                    ));
                    $cpBasketItem->setBasketItemType(BasketItemType::BASKET_ITEM_TYPE_DEFAULT);
                    $request->addBasketItem($cpBasketItem);
                }
            }

            // Try to autofix mismatch between oxid basket total sum and crefopay basketitems sum
            {
                // Calculate total of crefopay basket:
                $fTotalSumBasketItems = 0;
                /** @var \CrefoPay\Library\Request\Objects\BasketItem $cpBasketItem */
                foreach ($request->getBasketItems() as $cpBasketItem) {
                    if ($cpBasketItem->getBasketItemType() === BasketItemType::BASKET_ITEM_TYPE_COUPON) {
                        $fTotalSumBasketItems -= $cpBasketItem->getBasketItemAmount()->getAmount();
                    } else {
                        $fTotalSumBasketItems += $cpBasketItem->getBasketItemAmount()->getAmount();
                    }
                }

                // Check if total of crefopay basket mismatches the total of oxids basket:
                // (This can for example happen if oxid rounds different to crefopay)
                $fTotalSumBasketDiff = $fTotalAmountOxid - $fTotalSumBasketItems;

                if (abs($fTotalSumBasketDiff) > 0) {
                    $aActualBasketItems = array_reverse($aActualBasketItems);

                    $diffLeft = $fTotalSumBasketDiff;

                    // If the basket total mismatches we intentionally adjust the amount of the last basketitem(s)
                    // to introduce an error to get the crefopay basket to the same total amount as the oxid basket
                    foreach ($aActualBasketItems as $oCpBasketItem) {
                        $oCurrentAmount = $oCpBasketItem->getBasketItemAmount();
                        $fBrutPrice = $oCurrentAmount->getAmount();
                        $fNetPrice = $oCurrentAmount->getNetAmount();
                        $fTaxFactor = 1;
                        if ($fNetPrice > 0) {
                            $fTaxFactor = $fBrutPrice / $fNetPrice;
                        }

                        $fBrutPrice += $diffLeft;
                        $fBrutPrice = (int)$fBrutPrice;
                        $fNetPrice = $fBrutPrice / $fTaxFactor;
                        $fNetPrice = (int)$fNetPrice;

                        if ($fBrutPrice >= 0) {
                            $oCurrentAmount->setAmount($fBrutPrice);
                            $oCurrentAmount->setNetAmount($fNetPrice);

                            $diffLeft = 0;
                            break;
                        }
                    }

                    if (abs($diffLeft) > 0) {
                        throw new StandardException('[crefopay] Failed to adjust total order amount');
                    }
                }
            }
        }
    }

    public function setMerchantReference($merchantReference)
    {
        if ($merchantReference) {
            $this->setOxidOrderNr($merchantReference);
            $this->save();

            $request = new UpdateTransactionData(CrefopayHelpers::getAPIConfig());
            $request->setOrderID($this->getCrefopayOrderId());
            $request->setMerchantReference($merchantReference);

            CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\UpdateTransactionData::class, $request, true);
        }
    }

    public function getCrefopayOrderId()
    {
        return $this->getRawField('CPORDERID');
    }

    public function setCrefopayOrderId($cpOrderId)
    {
        $this->setRawField('CPORDERID', $cpOrderId);
    }

    public function getCrefopayUserId()
    {
        return $this->getRawField('CPUSERID');
    }

    /**
     * Undocumented function
     *
     * @param User $oxuser
     * @return string
     */
    protected function getCustomerEmail(User $oxuser): string
    {
        $email = $oxuser->oxuser__oxusername->value;

        if (strlen($email) > 254) {
            $email = substr($email, -254);
        }

        return $email;
    }

    /**
     * @param User $oxuser
     * @return Person
     * @throws Exception
     */
    private function getUserData(User $oxuser)
    {
        $crefopayUser = new Person();
        $salutationWarning = '';
        if (!CrefopayHelpers::isB2BEnabledAndBusinessUser($oxuser)) {
            $oxsal = CrefopayHelpers::getSalutationFromSession();
            if (!$oxsal) {
                $oxsal = $oxuser->oxuser__oxsal->value;
            }
            $crefopaySalutation = '';
            if ($oxsal) {
                $crefopaySalutation = CrefopayMapper::mapSalutationToCrefopay($oxsal, $salutationWarning);
            }
            if ($crefopaySalutation) {
                $crefopayUser->setSalutation($crefopaySalutation);
            } else {
                CrefopayHelpers::getLogger()->debug('Could not determine salutation for user "' . $oxuser->getId() . ' ("' . $oxuser->oxuser__oxusername->value . '")" with message: "' . $salutationWarning . '" - Payment methods might be restricted!');
            }
        }

        $crefopayUser->setEmail($this->getCustomerEmail($oxuser));
        $crefopayUser->setSurname($oxuser->oxuser__oxlname->value);
        $crefopayUser->setName($oxuser->oxuser__oxfname->value);
        if ($oxuser->oxuser__oxfon->value) {
            $crefopayUser->setPhoneNumber($oxuser->oxuser__oxfon->value);
        }

        $aDateOfBirth = [];
        $aDateOfBirth[] = $oxuser->oxuser__oxbirthdate->value; // DOB from oxuser
        $aDateOfBirth[] = CrefopayHelpers::getDOBFromSession(); // DOB from session

        foreach ($aDateOfBirth as $birthdate) {
            if ($birthdate && !(strpos($birthdate, '0000') === 0)) {
                $birthdateTS = strtotime($birthdate);
                if (CrefopayHelpers::isDateOfBirthValid($birthdateTS)) {
                    $dt = new DateTime();
                    $dt->setTimestamp($birthdateTS);
                    $crefopayUser->setDateOfBirth($dt);
                    break;
                }
            }
        }
        if (!$crefopayUser->getDateOfBirth()) {
            CrefopayHelpers::getLogger()->debug("no valid birthdatefor user '" . $oxuser->getId() . "' ('" . $oxuser->oxuser__oxusername->value . "'), raw:  '" . print_r($aDateOfBirth, true) . "' - Payment methods might be restricted");
        }

        return $crefopayUser;
    }

    /**
     * @param $oxuser
     * @return Company
     */
    private function getCompanyData($oxuser)
    {
        $crefopayCompany = new Company();
        $crefopayCompany->setCompanyName($oxuser->oxuser__oxcompany->value);
        $crefopayCompany->setEmail($this->getCustomerEmail($oxuser));
        if ($oxuser->oxuser__oxustid->value) {
            $crefopayCompany->setCompanyVatID($oxuser->oxuser__oxustid->value);
        }

        return $crefopayCompany;
    }

    private function getBillingRecipient($oxuser)
    {
        $recipient = $oxuser->oxuser__oxfname->value . ' ' . $oxuser->oxuser__oxlname->value;

        return trim($recipient);
    }

    /**
     * @param User $oxuser
     * @return string
     */
    private function getShippingRecipient(\OxidEsales\Eshop\Application\Model\Address $oDeliveryAddress)
    {
        $recipient = $oDeliveryAddress->oxaddress__oxfname->value . ' ' . $oDeliveryAddress->oxaddress__oxlname->value;
        $recipient = trim($recipient);

        return $recipient;
    }

    /**
     * @param User $oxuser
     * @return Address
     */
    private function getShippingAddress(\OxidEsales\Eshop\Application\Model\Address $oDeliveryAddress)
    {
        // Different shipping address available?
        $crefopayShippingAddress = new Address();
        $crefopayShippingAddress->setStreet($oDeliveryAddress->oxaddress__oxstreet->value);
        $crefopayShippingAddress->setNo($oDeliveryAddress->oxaddress__oxstreetnr->value);
        $crefopayShippingAddress->setAdditional($oDeliveryAddress->oxaddress__oxaddinfo->value);
        $crefopayShippingAddress->setZip($oDeliveryAddress->oxaddress__oxzip->value);
        $crefopayShippingAddress->setCity($oDeliveryAddress->oxaddress__oxcity->value);

        $oStateId = $oDeliveryAddress->oxaddress__oxstateid->value;
        if ($oStateId) {
            $sStateId = oxNew(State::class);
            if ($sStateId->load($oStateId)) {
                $crefopayShippingAddress->setState($sStateId->oxstates__oxtitle->value);
            } else {
                CrefopayHelpers::getLogger()->warn("State with id '" . $sStateId . "' missing in oxstates!");
            }
        }

        $oCountry = oxNew(Country::class);
        if ($oCountry->load($oDeliveryAddress->oxaddress__oxcountryid->value)) {
            $crefopayShippingAddress->setCountry($oCountry->oxcountry__oxisoalpha2->value);
        } else {
            CrefopayHelpers::getLogger()->warn("Country with id '" . $oDeliveryAddress->oxaddress__oxcountryid->value . "' missing in oxcountry-table!");
        }

        return $crefopayShippingAddress;
    }

    private function getBillingAddress(User $oxuser)
    {
        $crefopayBillingAddress = new Address();
        $crefopayBillingAddress->setStreet($oxuser->oxuser__oxstreet->value);
        $crefopayBillingAddress->setNo($oxuser->oxuser__oxstreetnr->value);
        $crefopayBillingAddress->setAdditional($oxuser->oxuser__oxaddinfo->value);
        $crefopayBillingAddress->setZip($oxuser->oxuser__oxzip->value);
        $crefopayBillingAddress->setCity($oxuser->oxuser__oxcity->value);

        $sStateId = $oxuser->oxuser__oxstateid->value;
        if ($sStateId) {
            $oState = oxNew(State::class);
            if ($oState->load($sStateId)) {
                $crefopayBillingAddress->setState($oState->oxstates__oxtitle->value);
            } else {
                CrefopayHelpers::getLogger()->warn("State with id '" . $sStateId . "' could not be loaded. Missing in oxstates-table?");
            }
        }

        $oCountry = oxNew(Country::class);
        if ($oCountry->load($oxuser->oxuser__oxcountryid->value)) {
            $crefopayBillingAddress->setCountry($oCountry->oxcountry__oxisoalpha2->value);
        } else {
            CrefopayHelpers::getLogger()->warn("Country with id '" . $oxuser->oxuser__oxcountryid->value . "' could not be loaded. Missing in oxcountry-table?");
        }

        return $crefopayBillingAddress;
    }

    public function getAllowedCrefopayPaymentMethods()
    {
        $allowedPaymentMethods = Registry::getSession()->getVariable('crefopayAllowedPaymentMethods');
        if (is_array($allowedPaymentMethods)) {
            return $allowedPaymentMethods;
        }

        $val = $this->getRawField('CPALLOWEDPAYMENTMETHODS');
        if ($val) {
            return json_decode($val, true);
        }

        return [];
    }

    public function setCrefopayPaymentReference($paymentReference)
    {
        if ($paymentReference) {
            $this->setRawField('CPPAYMENTREFERENCE', $paymentReference);
        }
    }

    public function getCrefopayPaymentReference()
    {
        return $this->getRawField('CPPAYMENTREFERENCE');
    }

    public function setCrefopayOrderStatus($orderStatus)
    {
        if ($orderStatus) {
            $this->setRawField('CPORDERSTATUS', $orderStatus);
        }
    }

    public function setPaidDateSQL($paidDateSQL)
    {
        if ($paidDateSQL) {
            $this->setRawField('CPPAIDDATE', $paidDateSQL);
        }
    }

    public function setPaidDateTS($paidDateTS)
    {
        if ($paidDateTS) {
            $this->setRawField('CPPAIDDATE', date("Y-m-d H:i:s", $paidDateTS));
        }
    }

    public function getPaidDateTS()
    {
        return (int)$this->getRawField('CPPAIDDATE');
    }

    /**
     * @param $paidDate
     * @return void
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseErrorException
     */
    public function markAsPaid($paidDate = null)
    {
        if (!$paidDate) {
            $paidDate = date("Y-m-d H:i:s");
        }

        $this->setPaidDateTS(strtotime($paidDate));

        $sOxidOrderId = $this->getOxidOrderId();
        if (!$sOxidOrderId) {
            return;
        }
        $oOrder = oxNew(Order::class);
        if ($oOrder->load($sOxidOrderId)) {
            CrefopayHelpers::getLogger()->debug("Setting paid-date for cp-order " . $this->getCrefopayOrderId() . " to " . $paidDate);

            DatabaseProvider::getDb()->execute(
                "UPDATE oxorder SET OXPAID=? WHERE OXID=?",
                array(
                $paidDate,
                $sOxidOrderId
            )
            );

            $this->save();
        }
    }

    public function getCrefopayOrderStatus()
    {
        return $this->getRawField('CPORDERSTATUS');
    }

    public function setCrefopayPaymentMethod($paymentMethod)
    {
        $this->setRawField('CPPAYMENTMETHOD', $paymentMethod);
    }

    public function getCrefopayPaymentMethod()
    {
        return $this->getRawField('CPPAYMENTMETHOD');
    }

    public function setCrefopayPaymentToken($paymentToken)
    {
        $this->setRawField('CPPAYMENTTOKEN', $paymentToken);
    }

    public function getCrefopayPaymentToken()
    {
        return $this->getRawField('CPPAYMENTTOKEN', '');
    }

    public function setCrefopayPaymentInstrumentId($paymentInstrumentId)
    {
        $this->setRawField('CPPAYMENTINSTRUMENTID', $paymentInstrumentId);
    }

    public function getCrefopayPaymentInstrumentId()
    {
        return (string)$this->getRawField('CPPAYMENTINSTRUMENTID');
    }

    public function getCrefopayCCIssuer()
    {
        return $this->getRawField('CPCCISSUER');
    }

    public function setCrefopayCCIssuer($ccIssuer)
    {
        $this->setRawField('CPCCISSUER', $ccIssuer);
    }

    public function setAllowedCrefopayPaymentMethods($allowedPaymentMethods)
    {
        if (is_array($allowedPaymentMethods)) {
            $this->setRawField('CPALLOWEDPAYMENTMETHODS', json_encode($allowedPaymentMethods));

            if (count($allowedPaymentMethods) > 0) {
                Registry::getSession()->setVariable('crefopayAllowedPaymentMethods', $allowedPaymentMethods);
            }
        } else {
            throw new \RuntimeException('Expected allowedPaymentMethods-argument to be an array!');
        }
    }

    /**
     * Returns true if the basket sum was zero and a successful reserve()-call was faked
     * @return bool
     */
    public function hasBasketZeroSum()
    {
        $aAdditionalData = $this->getCrefopayAdditionalData();
        if (isset($aAdditionalData['BASKET_ZEROSUM'])) {
            return (bool)$aAdditionalData['BASKET_ZEROSUM'];
        }

        return false;
    }

    public function reserve()
    {
        $oBasket = Registry::getSession()->getBasket();
        if (!$oBasket) {
            CrefopayHelpers::redirectOnError('basket', -1, 'CREFOPAY_ERROR_INVALID_BASKET');
        }

        $this->refreshTransaction();

        $aDoneStates = ['acknowledgepending', 'done', 'merchantpending', 'ciapending', 'inprogress', 'fraudpending'];

        if (in_array(strtolower($this->getCrefopayTransactionStatus()), $aDoneStates)) {
            // Already paid
            CrefopayHelpers::getLogger()->debug('reserve(): Skipping - crefopay-status is already ' . $this->getCrefopayTransactionStatus());

            return true;
        }

        /** @var Payment|CrefopayPayment $oPayment */
        $oPayment = oxNew(Payment::class);
        $oPayment->load($oBasket->getPaymentId());
        if (!$oPayment->getCrefopayPaymentMethodId()) {
            throw new \RuntimeException("selected payment-type not a crefopay-payment type! ('" . print_r($oBasket->getPaymentId(), true) . "')");
        }

        if ($this->getCrefopayTransactionStatus() !== 'NEW') {
            Registry::getSession()->deleteVariable('crefopayOrderId'); // force a new transaction
            CrefopayHelpers::redirectOnError('payment', -1, 'CREFOPAY_ERROR_INVALIDTRANSACTIONSTATE');
        }

        // Fake successfull reserve() if the basket amount is zero:
        if ($oBasket->getBruttoSum() < 0.01) {
            $this->setCrefopayOrderStatus('PAID');
            $this->setCrefopayTransactionStatus('DONE');
            $this->setCrefopayAdditionalData([
                'BASKET_ZEROSUM' => 1,
                'FAKE_RESERVE' => 1
            ]);
            $this->save();
            $this->markAsPaid();

            return true;
        }

        $request = new Reserve(CrefopayHelpers::getAPIConfig());
        $request->setOrderID($this->getCrefopayOrderId());
        $request->setAmount($this->toAmount($oBasket->getPrice()));
        $request->setPaymentMethod($oPayment->getCrefopayPaymentMethodId());

        $paymentInstrumentId = $this->getCrefopayPaymentInstrumentID();
        if ($paymentInstrumentId) {
            $request->setPaymentInstrumentID($paymentInstrumentId);
        }

        $paymentToken=$this->getCrefopayPaymentToken();
        if ($paymentToken)
        {
            $request->setPaymentToken($paymentToken);
        }

        {
            // Copy sum from fresh transaction template in case the basket has changed after payment method selection:
            $referenceTransaction = $this->createTransactionRequest(true, null);
            $request->setAmount(clone $referenceTransaction->getAmount());
            $aBasketItems = $referenceTransaction->getBasketItems();
            foreach ($aBasketItems as $oBasketItem) {
                $request->addBasketItem(clone $oBasketItem);
            }
        }

        $additionalInformation = [];
        $oUser = Registry::getSession()->getUser();
        if ($oUser) {
            $userData = $this->getUserData($oUser);

            $crefopaySalutation = $userData->getSalutation();
            if ($crefopaySalutation) {
                $additionalInformation['salutation'] = $crefopaySalutation;
            }

            $dateOfBirth = $userData->getDateOfBirth();
            if ($dateOfBirth) {
                $additionalInformation['dateOfBirth'] = $dateOfBirth->format("Y-m-d");
            }
        }
        if (count($additionalInformation) > 0) {
            $request->setAdditionalInformation(json_encode($additionalInformation));
        }

        $aValidationMessages = CrefopayHelpers::validateRequest($request);
        if (count($aValidationMessages) > 0) {
            CrefopayHelpers::addErrorToDisplay(-1, null, $aValidationMessages);
            $this->forceNewTransaction();
            return false;
        }

        $response = CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\Reserve::class, $request, true);

        $additionalData = $response->getData('additionalData');
        if ($additionalData) {
            $this->setCrefopayAdditionalData($additionalData);
            $this->save();
        }

        $this->refreshTransaction();

        $resultCode = CrefopayHelpers::getResultCodeFromResponse($response);
        if ($resultCode === 0) {
            $this->addAutoCapture();

            // success
            return true;
        }
        if ($resultCode === 1) {
            // Payment method needs redirect to external website
            return $response->getData('redirectUrl');
        }

        CrefopayHelpers::addErrorToDisplay($resultCode);

        $aPaymentDeclinedDueFraudCheckResultCodes = [2017];
        if (in_array($resultCode, $aPaymentDeclinedDueFraudCheckResultCodes)) {
            // Payment declined by fraud check - Customer has to use another payment method
            // Set a higher risk-class?
            $this->setCrefopayRiskClass(CrefopayRiskClass::RiskHigh);
            Registry::getSession()->setVariable('crefopayRiskClass', CrefopayRiskClass::RiskHigh);
            $this->save();
        }

        $this->forceNewTransaction();

        return false;
    }

    public function addAutoCapture()
    {
        // if autocapture is set crefopay itself will capture with cp-orderID as captureID
        // We add an entry to the capture db table here for this special case:
        if (CrefopayHelpers::getConfigBool('CrefoPayAutoCapture', false)) {
            $captureId = $this->getCrefopayOrderId();
            $oCapture1 = $this->getCaptureByCaptureId($captureId);
            $oCapture2 = $this->getCaptureByCaptureId($this->getId()); // Fallback for older module version
            if ($oCapture1) {
                CrefopayHelpers::getLogger()->debug("[" . $this->getCrefopayOrderId() . "] is already captured: '" . print_r($oCapture1->getId(), true) . "'");
            } elseif ($oCapture2) {
                CrefopayHelpers::getLogger()->debug("[" . $this->getCrefopayOrderId() . "] is already captured: '" . print_r($oCapture2->getId(), true) . "'");
            } else {
                $oxorderid = $this->getOxidOrderId();
                if ($oxorderid) {
                    $oTmpOrder = oxNew(Order::class);
                    if ($oTmpOrder->load($oxorderid)) {
                        $this->addCapture($this->getCrefopayOrderId(), (float)$oTmpOrder->oxorder__oxtotalordersum->value, '', true);
                    }
                }
            }
        }
    }

    public function forceNewTransaction()
    {
        $this->refreshTransaction();

        CrefopayHelpers::forceNewTransaction();
    }

    public function setCrefopayRiskClass($riskClass)
    {
        $this->setRawField('CPRISKCLASS', $riskClass);
    }

    public function getCrefopayRiskClass()
    {
        return (int)$this->getRawField('CPRISKCLASS');
    }

    public function setCrefopayRiskData($riskData)
    {
        $this->setRawField('CPRISKDATA', json_encode($riskData, JSON_PRETTY_PRINT));

        $CrefoPayApplyRiskData = (int)CrefopayHelpers::getConfigParam('CrefoPayApplyRiskData', 0);
        if ($CrefoPayApplyRiskData > 0) {
            /** @var SolvencyData[] $solvencyData */
            $solvencyData = $riskData['solvencyData'];
            if (!is_array($solvencyData)) {
                $solvencyData = [];
            }

            /** @var SolvencyData|null $useSolvencyDataEntry */
            $useSolvencyDataEntry = null;

            if (count($solvencyData) > 0) {
                // Sort by date, newest to bottom:
                usort($solvencyData, function ($a, $b) {
                    /** @var $a SolvencyData */
                    /** @var $b SolvencyData */

                    return strtotime($a->getCheckDate()->getTimestamp() - strtotime($b->getCheckDate()->getTimestamp()));
                });

                foreach ($solvencyData as $solvencyDataEntry) {
                    if ($solvencyDataEntry->getScore()) {
                        $useSolvencyDataEntry = $solvencyDataEntry;
                        break;
                    }
                }
            }

            $sUserId = $this->getOxidUserId();
            if ($sUserId) {
                $oUser = oxNew(User::class);
                if ($oUser->load($sUserId)) {
                    if ($useSolvencyDataEntry) {
                        $useScore = (int)$useSolvencyDataEntry->getScore();
                    } else {
                        // Get oxids default score:
                        $oTmpUser = oxNew(User::class);
                        $useScore = $oTmpUser->getBoni();
                    }

                    $oxboni = (int)$oUser->getFieldData('OXBONI');
                    if ($oxboni !== $useScore) {
                        $oUser->_setFieldData('OXBONI', $useScore);
                        $oUser->save();

                        // Add remark:
                        $remarkText = "Crefopay changed OXBONI to " . $useScore . "\r\n";
                        if ($useSolvencyDataEntry) {
                            $remarkText .= "Solvency-Interface: " . $useSolvencyDataEntry->getSolvencyInterface() . "\r\n";
                            $remarkText .= "Score: " . $useSolvencyDataEntry->getScore() . "\r\n";
                            $remarkText .= "Date of Last Solvency Check Request: " . date("Y-m-d H:i:s", $useSolvencyDataEntry->getCheckDate()->getTimestamp()) . "\r\n";
                            $remarkText .= "'Last Request Date: " . date("Y-m-d H:i:s", $useSolvencyDataEntry->getLastRequestDate()->getTimestamp()) . "\r\n";
                        } else {
                            $remarkText .= "Crefopay: No Solvency-Data available-Interface. OXBONI set to default value (".$useScore.")\r\n";
                        }

                        $oRemark = oxNew(\OxidEsales\Eshop\Application\Model\Remark::class);
                        $oRemark->oxremark__oxtext = new \OxidEsales\Eshop\Core\Field($remarkText);
                        $oRemark->oxremark__oxparentid = new \OxidEsales\Eshop\Core\Field($oUser->getId());
                        $oRemark->oxremark__oxshopid = new \OxidEsales\Eshop\Core\Field(Registry::getConfig()->getShopId());
                        $oRemark->oxremark__oxtype = new \OxidEsales\Eshop\Core\Field("r");
                        $oRemark->save();
                    }
                }
            }
        }
    }

    public function getCrefopayRiskData()
    {
        $riskData = $this->getRawField('CPRISKDATA');
        if ($riskData) {
            $riskDataArray = @json_decode($riskData, true);
            if (is_array($riskDataArray)) {
                return $riskDataArray;
            }
        }

        return [];
    }

    public function getPaymentAdditionalData()
    {
        $additionalData = $this->getCrefopayAdditionalData();
        if (is_array($additionalData)) {
            return $additionalData;
        }

        return [];
    }

    /**
     * @return bool
     */
    public function hasPaymentAdditionalData()
    {
        $additionalData = $this->getPaymentAdditionalData();

        return count($additionalData) > 0;
    }

    public function getCrefopayCreated()
    {
        return $this->getRawField('CPCREATED');
    }

    public function getCrefopayUpdated()
    {
        return $this->getRawField('CPUPDATED');
    }

    public function setIsCaptured($iCaptureState)
    {
        $this->setRawField('CPISCAPTURED', $iCaptureState);
    }

    public function getIsCaptured()
    {
        return (int)$this->getRawField('CPISCAPTURED');
    }

    public function capture()
    {
        if ($this->getIsCaptured() > 0) {
            // already captured or capture failed before
            return;
        }

        $sOxidOrderId = $this->getOxidOrderId();
        if (!$sOxidOrderId) {
            CrefopayHelpers::getLogger()->error("Tried to capture an order without oxid-orderid - cp-order-id: '" . $this->getCrefopayOrderId() . "'");

            return;
        }

        /** @var Order $oOrder */
        $oOrder = oxNew(Order::class);
        if (!$oOrder->load($sOxidOrderId)) {
            CrefopayHelpers::getLogger()->error("Failed to capture cp-order-id '" . $this->getCrefopayOrderId() . "': Oxid-Order-ID is invalid/unloadable: '" . $sOxidOrderId . "'");
            $this->setIsCaptured(2);

            return;
        }

        $this->addCapture($this->getCrefopayOrderId(), (float)$oOrder->oxorder__oxtotalordersum->value);
    }

    public function getCaptureByCaptureId($captureId)
    {
        $oCapture = oxNew(CrefopayTransactionCapture::class);
        if ($oCapture->load($captureId)) {
            if ($oCapture->getCrefopayOrderId() === $this->getCrefopayOrderId()) {
                CrefopayHelpers::getLogger()->debug("Loaded captureID '" . $captureId . "'");

                return $oCapture;
            } else {
                CrefopayHelpers::getLogger()->debug("Loaded captureID '" . $captureId . "', but does belong to cp-order '" . $oCapture->getCrefopayOrderId() . "' instead of expected cp-order '" . $this->getCrefopayOrderId() . "'");
            }
        } else {
            CrefopayHelpers::getLogger()->debug("captureID '" . $captureId . "' doest not exist (yet)");
        }

        return false;
    }

    public function addCapture($captureId, $amount, $merchantReference = '', $bIsAutoCapture = false)
    {
        $this->refreshTransaction();

        $oCapture = $this->getCaptureByCaptureId($captureId);
        if ($oCapture) {
            CrefopayHelpers::getLogger()->error("'" . print_r($captureId, true) . "' is already captured!");
            return false;
        }

        $oCapture = oxNew(CrefopayTransactionCapture::class);
        $oCapture->setId($captureId);
        $oCapture->setAmount($amount);
        $oCapture->setCrefopayOrderId($this->getCrefopayOrderId());
        $oCapture->setMerchantReference($merchantReference);
        $oCapture->setCaptureState(CrefopayTransactionCapture::CAPTURESTATE_NOTCAPTURED);
        $oCapture->setIsPaid(false);
        $oCapture->setIsPaidProcessed(false);
        $oCapture->setTimestampAddedTS(time());

        if ($bIsAutoCapture) {
            CrefopayHelpers::getLogger()->debug("addCapture() (autocapture) for cp-order-id '" . $this->getCrefopayOrderId() . "', captureID: '" . $oCapture->getId() . "'");

            $oCapture->setCaptureState(CrefopayTransactionCapture::CAPTURESTATE_AUTOCAPTURE);
        } else {
            CrefopayHelpers::getLogger()->debug("addCapture() (regular) for cp-order-id '" . $this->getCrefopayOrderId() . "', captureID: '" . $oCapture->getId() . "'");

            $request = new Capture(CrefopayHelpers::getAPIConfig());
            $request->setOrderID($this->getCrefopayOrderId());
            $request->setAmount($this->toAmount((float)$amount));
            $request->setCaptureID($captureId);

            $response = CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\Capture::class, $request, true);
            if ($response instanceof FailureResponse) {
                CrefopayHelpers::getLogger()->error("Failed to capture cp-order-id '" . $this->getCrefopayOrderId() . "': " . $response->getData('resultCode') . ' - ' . $response->getData('message'));
                $this->setIsCaptured(CrefopayTransactionCapture::CAPTURESTATE_FAILED);
                $oCapture->setCaptureState(CrefopayTransactionCapture::CAPTURESTATE_FAILED);
            }
            if ($response instanceof SuccessResponse) {
                $this->setIsCaptured(CrefopayTransactionCapture::CAPTURESTATE_CAPTURED);
                $oCapture->setCaptureState(CrefopayTransactionCapture::CAPTURESTATE_CAPTURED);
            }
        }

        $oCapture->save();
        $this->save();
        $this->refreshTransaction();

        return true;
    }

    public function save()
    {
        // Only save if an id was set
        if ($this->getId()) {
            return parent::save();
        }

        return false;
    }

    /**
     * @var bool|PaymentInstrument[]
     */
    private $userPaymentInstrumentsLoaded = false;

    private function callGetUserPaymentInstruments()
    {
        if ($this->userPaymentInstrumentsLoaded === false) {
            $this->userPaymentInstrumentsLoaded = [];

            $oUser = Registry::getSession()->getUser();
            if ($oUser->isLoaded()) {
                $request = new GetUserPaymentInstrument(CrefopayHelpers::getAPIConfig());
                $request->setUserID($this->getCrefopayUserId());

                $response = CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\GetUserPaymentInstrument::class, $request, true);
                if ($response instanceof SuccessResponse) {
                    $this->userPaymentInstrumentsLoaded = $response->getData('paymentInstruments');
                }
            }
        }
    }

    public function getUserPaymentInstrumentsForPayment($cppayment_id)
    {
        $rv = [];

        $this->callGetUserPaymentInstruments();
        foreach ($this->userPaymentInstrumentsLoaded as $oUserPaymentInstrument) {
            if ($cppayment_id === 'DD') { // Direct-Debit
                if ($oUserPaymentInstrument->getPaymentInstrumentType() === 'BANKACCOUNT') {
                    $rv[] = $oUserPaymentInstrument;
                }
            } elseif (stripos($cppayment_id, 'CC') === 0) { // Direct-Debit
                if ($oUserPaymentInstrument->getPaymentInstrumentType() === 'CREDITCARD') {
                    $rv[] = $oUserPaymentInstrument;
                }
            }
        }

        return $rv;
    }

    public function updateFromCallbackData($fromWhere)
    {
        $crefopaycallbackdataJSON = Registry::getRequest()->getRequestParameter('crefopaycallbackdata');
        if ($crefopaycallbackdataJSON) {
            $crefopaycallbackdata = json_decode($crefopaycallbackdataJSON, true);
            $cpOrderID = $crefopaycallbackdata['orderNo'];

            CrefopayHelpers::getLogger()->debug("Received callback-data via " . $fromWhere . "-controller : " . json_encode($crefopaycallbackdata) . ', form-data: ' . json_encode($_REQUEST, true));

            if ($this->getCrefopayOrderId() === $cpOrderID) {
                // Reset values:
                $this->setCrefopayPaymentMethod('');
                $this->setCrefopayPaymentInstrumentId('');
                $this->setCrefopayPaymentToken('');

                if (isset($crefopaycallbackdata['paymentToken'])) {
                    $paymentToken = $crefopaycallbackdata['paymentToken'];
                    $this->setCrefopayPaymentToken($paymentToken);
                }

                if (isset($crefopaycallbackdata['paymentMethod'])) {
                    $paymentMethod = $crefopaycallbackdata['paymentMethod'];
                    $this->setCrefopayPaymentMethod($paymentMethod);
                }

                if (isset($crefopaycallbackdata['paymentInstrumentId'])) {
                    $paymentInstrumentId = $crefopaycallbackdata['paymentInstrumentId'];
                    $this->setCrefopayPaymentInstrumentId($paymentInstrumentId);
                }

                $oUser = Registry::getSession()->getUser();

                $paymentid = Registry::getRequest()->getRequestEscapedParameter('paymentid');

                $crefoPayDOB = Registry::getRequest()->getRequestEscapedParameter('crefoPayDOB');
                if (is_array($crefoPayDOB)) {
                    if (isset($crefoPayDOB[$paymentid])) {
                        CrefopayHelpers::setDOBInSession(null);

                        CrefopayHelpers::getLogger()->debug('Update dob to ' . print_r($crefoPayDOB[$paymentid], true));

                        $iDOB = strtotime($crefoPayDOB[$paymentid]);
                        if (CrefopayHelpers::isDateOfBirthValid($iDOB)) {
                            if (CrefopayHelpers::getDOBUsage() === CrefopayConstants::DOBUSAGE_ASK_AND_SAVE) {
                                $oUser->oxuser__oxbirthdate = new \OxidEsales\EshopCommunity\Core\Field(date("Y-m-d", $iDOB));
                                $oUser->save();
                            }

                            CrefopayHelpers::setDOBInSession($iDOB);
                        } else {
                            CrefopayHelpers::getLogger()->debug('dob is not valid');
                        }
                    }
                }

                $crefoPaySalutation = Registry::getRequest()->getRequestEscapedParameter('crefoPaySalutation');
                if (is_array($crefoPaySalutation)) {
                    if (isset($crefoPaySalutation[$paymentid])) {
                        CrefopayHelpers::setSalutationInSession($crefoPaySalutation[$paymentid]);
                        $oxsal = CrefopayMapper::filterOxidSalutation($crefoPaySalutation[$paymentid]);
                        if ($oxsal) {
                            $oUser->oxuser__oxsal = new Field($oxsal);
                            $oUser->save();
                        }
                    }
                }

                $this->save();
            }
        }
    }

    private $hasLock = false;
    private $lockFP = null;
    private $lockFilename = '';

    public function enterLock()
    {
        if ($this->hasLock) {
            // Already got the lock in this request
            return true;
        }

        $sLockFileDir = getShopBasePath() . '/../var/crefopay-locks/';
        if (!is_dir($sLockFileDir)) {
            if (!mkdir($sLockFileDir, 0777, true) && !is_dir($sLockFileDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $sLockFileDir));
            }
        }

        // The lock-file is to prevent running finalizeCrefopayOrder() for the same order in parallel
        // if the user comes back to the shop and the MNS-callback by chance happens at the same time
        $this->lockFilename = $sLockFileDir . '/crefopay_finalize-' . $this->getId() . '.lock';
        $this->lockFP = fopen($this->lockFilename, "wb");
        if (flock($this->lockFP, LOCK_EX | LOCK_NB)) {
            $this->hasLock = true;

            return true;
        }

        return false;
    }

    public function exitLock()
    {
        if ($this->hasLock) {
            flock($this->lockFP, LOCK_UN);
            fclose($this->lockFP);
            @unlink($this->lockFilename);

            $this->hasLock = false;
        }
    }
}
