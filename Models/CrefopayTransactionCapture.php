<?php

namespace Crefopay\Payments\Models;

use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Model\BaseModel;

class CrefopayTransactionCapture extends BaseModel
{
    public const CAPTURESTATE_NOTCAPTURED = 0;

    public const CAPTURESTATE_CAPTURED = 1;

    public const CAPTURESTATE_FAILED = 2;

    public const CAPTURESTATE_AUTOCAPTURE = 3;

    public function __construct()
    {
        parent::__construct();
        $this->init('crefopay_captures');

        $this->_aSkipSaveFields[] = 'tstamp_updated';
        $this->setUseSkipSaveFields(true);
    }

    public function setAmount($amount)
    {
        $this->setRawField('AMOUNT', (float)$amount);
    }

    public function setMerchantReference($merchantReference)
    {
        $this->setRawField('MERCHANTREF', (string)$merchantReference);
    }

    public function setCrefopayOrderId($cpOrderId)
    {
        $this->setRawField('CPORDERID', $cpOrderId);
    }

    public function getCrefopayOrderId()
    {
        return (string)$this->getFieldData('CPORDERID');
    }

    /**
     * @param $iCaptureState int see CrefopayTransactionCapture::CAPTURESTATE_ constants!
     * @return void
     */
    public function setCaptureState($iCaptureState)
    {
        $this->setRawField('CAPTURESTATE', (int)$iCaptureState);
    }

    public function setIsPaid($bIsPaid)
    {
        $this->setRawField('ISPAID', ($bIsPaid ? 1 : 0));
    }

    public function setIsPaidProcessed($bIsPaidProcessed)
    {
        $this->setRawField('ISPAID_PROCESSED', ($bIsPaidProcessed ? 1 : 0));
    }

    public function setPaidDateSQL($paidDate)
    {
        if (!$paidDate) {
            $paidDate = date("Y-m-d H:i:s");
        }

        $this->setRawField('PAIDDATE', $paidDate);
    }

    public function markAsPaid()
    {
        // Called after PAIDDATE got set
    }

    public function setTimestampAddedTS($unixTimestamp)
    {
        $this->setRawField('TSTAMP_ADDED', date("Y-m-d H:i:s", $unixTimestamp));
    }

    private function setRawField($sFieldName, $sFieldValue)
    {
        $sLongFieldName = $this->_getFieldLongName($sFieldName);

        $oField=new Field($sFieldValue);
        $oField->rawValue=$sFieldValue;
        unset($oField->value);

        $this->{$sLongFieldName} = $oField;
    }
}
