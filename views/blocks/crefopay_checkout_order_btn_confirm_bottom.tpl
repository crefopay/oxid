[{$smarty.block.parent}]
[{assign var="payment" value=$oView->getPayment()}]

[{if $payment}]
    [{assign var="crefopayPaymentMethodId" value=$payment->getCrefopayPaymentMethodId()}]
    [{assign var="includeCrefopayOrderJS" value=false}]
    [{if $crefopayPaymentMethodId eq 'PAYPAL'}][{assign var="includeCrefopayOrderJS" value=true}][{/if}]
    [{if $crefopayPaymentMethodId eq 'GOOGLE_PAY'}][{assign var="includeCrefopayOrderJS" value=true}][{/if}]
    [{if $crefopayPaymentMethodId eq 'APPLE_PAY'}][{assign var="includeCrefopayOrderJS" value=true}][{/if}]

    [{if $includeCrefopayOrderJS}]
        <div style="display:none;" class="crefopay-secure-fields">
                <input type="radio" data-crefopay="paymentMethod" name="paymentMethodSelect" value="[{$payment->getCrefopayPaymentMethodId()}]" checked>[{$payment->getCrefopayPaymentMethodId()}]</label>
        </div>
        <div class="js-crefopay-onemoment" style="position:fixed;left:0px;top:0px;width:100vw;height:100vh;z-index:90000;background-color: black;opacity:0.7;display:flex;justify-content: center;align-items: center;">
            <div style="color:white;">[{oxmultilang ident="CREFOPAY_ONEMOMENT"}]</div>
        </div>

            [{if $crefopayPaymentMethodId eq 'GOOGLE_PAY'}]
                <!-- google pay -->
                <script>
                    window.crefopayGooglePayData=[{$oViewConf->getCrefopayGooglePayData()}];

                    let paymentsClient=null;

                    // Hide regular submit button:
                    document.querySelector(".nextStep").style.display='none';

                    console.log(window.crefopayGooglePayData);

                    function getGooglePaymentsClient() {
                        if ( paymentsClient === null ) {
                            paymentsClient = new google.payments.api.PaymentsClient({environment: window.crefopayGooglePayData['environment']});
                        }
                        return paymentsClient;
                    }

                    function getGooglePaymentDataRequest() {
                        console.log("getGooglePaymentDataRequest");

                        return {
                            "apiVersion": 2,
                            "apiVersionMinor": 0,
                            "merchantInfo": {
                                "merchantName": window.crefopayGooglePayData['merchantName']
                            },
                            "allowedPaymentMethods": [
                                {
                                    "type": 'CARD',
                                    "parameters": {
                                        "allowedAuthMethods": window.crefopayGooglePayData['allowedCardAuthMethods'],
                                        "allowedCardNetworks": window.crefopayGooglePayData['allowedCardNetworks']
                                    },
                                    "tokenizationSpecification": {
                                        "type": 'PAYMENT_GATEWAY',
                                        "parameters": {
                                            'gateway': window.crefopayGooglePayData['gateway'],
                                            'gatewayMerchantId': window.crefopayGooglePayData['gatewayMerchantId']
                                        }
                                    }
                                }
                            ],
                            "transactionInfo": {
                                "totalPriceStatus": 'FINAL',
                                "totalPrice": window.crefopayGooglePayData['totalPrice'].substring(1),
                                "countryCode": window.crefopayGooglePayData['countryCode'],
                                "currencyCode": window.crefopayGooglePayData['currencyCode']
                            }
                        };
                    }

                    function processPayment(paymentData) {
                        console.log("processPayment");
                        console.log(paymentData);
                        let paymentToken = paymentData.paymentMethodData.tokenizationData.token;
                        window.setTimeout(function() {
                            window.crefopayFinalizeGooglePay(paymentToken);
                        },0);


                    }

                    function onGooglePaymentButtonClicked() {
                        CrefopaySlimjQuery(".js-crefopay-onemoment").show();
                        window.crefopayPrevalidate();
                    }

                    function onGooglePaymentButtonClickedContinue() {
                        console.log("onGooglePaymentButtonClicked");
                        let paymentDataRequest = getGooglePaymentDataRequest();
                        console.log(paymentDataRequest);

                        const paymentsClient = getGooglePaymentsClient();
                        paymentsClient.loadPaymentData(paymentDataRequest)
                            .then(function (paymentData) {
                                // handle the response
                                processPayment(paymentData);
                            })
                            .catch(function (err) {
                                // show error in developer console for debugging
                                console.log("google-pay canceled");
                                console.error(err);
                                window.crefopayGooglePayCanceled();
                            });
                    }
                    window.onGooglePaymentButtonClickedContinue=onGooglePaymentButtonClickedContinue;

                    // Called when google pay js has loaded:
                    function crefopay_on_googlepay_loaded()
                    {
                        const paymentsClient = getGooglePaymentsClient();

                        // Show Google Pay button:
                        let buttonOptions={
                            "buttonType":"checkout",
                            "onClick":onGooglePaymentButtonClicked
                        };

                        let buttonLocale=document.querySelector("html").getAttribute('lang');
                        if (buttonLocale)
                        {
                            buttonOptions['buttonLocale']=buttonLocale;
                        }

                        const button = paymentsClient.createButton(buttonOptions);
                        button.style.textAlign='right';
                        button.className='crefopayGooglePayButton';
                        document.querySelector(".nextStep").parentElement.appendChild(button);
                    }
                </script>
                <script async src="https://pay.google.com/gp/p/js/pay.js" onload="crefopay_on_googlepay_loaded()"></script>
                <!-- /google pay -->
            [{elseif $crefopayPaymentMethodId eq 'APPLE_PAY'}]
                <!-- apple pay -->
                <script>
                    window.crefopayApplePayData = [{$oViewConf->getCrefopayApplePayData()}]
                    window.crefopayApplePayTransaltions = [{$oViewConf->getCrefopayApplePayTranslations()}]

                    // Hide regular submit button:
                    document.querySelector(".nextStep").style.display='none';

                    //create Apple Pay button placeholder
                    const button = document.createElement('div')
                    button.setAttribute('data-crefopay-placeholder', 'appleButton')
                    button.style.textAlign='right';
                    button.className='crefopayApplePayButton';

                    document.querySelector(".nextStep").parentElement.appendChild(button);

                    let appleClient = null

                    console.log(window.crefopayApplePayTransaltions)

                    function getAppleClient() {
                        if(appleClient === null) {
                            appleClient = new AppleClient(
                                window.crefopayApplePayData['shopPublicKey'],
                                window.crefopayApplePayData['orderNo'],
                                window.crefopayApplePayData['sessionRequest'],
                                window.crefopayApplePayData['configuration'],
                                function (response) {
                                    if(response.resultCode > 0) {
                                        console.error(response.message)
                                    }
                                },
                                function () {
                                    alert(window.crefopayApplePayTransaltions['CREFOPAY_APPLEPAY_AUTHENTICATION_FAILED'])
                                },
                                function () {
                                    CrefopaySlimjQuery(".js-crefopay-onemoment").show();
                                    CrefopaySlimjQuery("#orderConfirmAgbBottom").submit()
                                }
                            )
                        }

                        console.log(appleClient)

                        return appleClient
                    }

                    function crefopay_on_applepay_loaded() {
                        const appleClient = getAppleClient()

                        document.addEventListener('crefopaySessionCreated', function () {
                            appleClient.createButton()
                        })
                    }
                </script>
                <script async src="https://sandbox.crefopay.de/libs/3.0/apple-client.js" onload="crefopay_on_applepay_loaded()"></script>
                <!-- /apple pay -->
            [{/if}]

        [{oxscript include=$oViewConf->getCrefopayLibraryUrl()}]
        [{if $oViewConf->useCrefopayModuleJQuery()}]
            [{oxscript include=$oViewConf->getCrefopaySlimLibraryUrl()}]
        [{else}]
            [{* Map existing jQuery to CrefopaySlimjQuery: *}]
            [{oxscript add="window.CrefopaySlimjQuery = jQuery;"}]
        [{/if}]
        [{oxscript add=$oViewConf->getCrefopaySecureFieldsConfigurationJS('order')}]
    [{/if}]
[{/if}]