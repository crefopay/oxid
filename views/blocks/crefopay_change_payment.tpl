[{oxstyle include=$oViewConf->getModuleUrl('cppayments','out/src/css/cppayments.css')}]

[{oxscript include=$oViewConf->getCrefopayLibraryUrl()}]
[{if $oViewConf->useCrefopayModuleJQuery()}]
    [{oxscript include=$oViewConf->getCrefopaySlimLibraryUrl()}]
[{else}]
    [{* Map existing jQuery to CrefopaySlimjQuery: *}]
    [{oxscript add="window.CrefopaySlimjQuery = jQuery;"}]
[{/if}]
[{oxscript add=$oViewConf->getCrefopaySecureFieldsConfigurationJS('payment')}]
[{assign var="oCrefopayTransaction" value=$oViewConf->getCrefopayTransaction()}]

<div style="display:none;">
    <form id="crefopay_refresh_ajaxform" action="[{$oViewConf->getSelfLink()}]" method="post">
        [{$oViewConf->getHiddenSid()}]
        <input type="hidden" name="cl" value="[{$oViewConf->getActiveClassName()}]">
        <input type="hidden" name="fnc" value="crefopay_refresh">
        <input type="hidden" name="paymentid" value="">
    </form>
</div>

[{block name="crefopay_onemoment_layer"}]
<div class="crefopay_onemoment_layer" style="display:none;">
    <div class="crefopay_onemoment_bg" style="position:fixed;left:0;top:0;width:100vw;height:100vh;background-color: black;opacity:0.8;z-index:10000"></div>

    <div class="crefopay_onemoment_txt" style="position:fixed;left:50vw;top:50vh;z-index:10001;color:white;transform:translate(-50%, -50%)">
        [{oxmultilang ident="CREFOPAY_ONEMOMENT"}]
    </div>
</div>
[{/block}]

<div class="js-crefopay-change-payment-loader">
    <div style="text-align: center;margin:20px;">[{oxmultilang ident="CREFOPAY_LOADING_PAYMENTMETHODS"}]</div>
</div>

<div class="js-crefopay-change-payment-error" style="display:none;">
    <div style="text-align: center;margin:20px;font-weight: bold;color:red;">[<span class="js-crefopay-change-payment-error-no"></span>] [{oxmultilang ident="CREFOPAY_LOADING_PAYMENTMETHODS_ERROR"}]</div>
</div>

<div class="js-crefopay-change-payment-container" style="display:none;">
    <input style="display:none;" type="radio" class="js-crefopay-selected-paymentinstrumentid" data-crefopay="paymentInstrument.id" value="" checked>
[{$smarty.block.parent}]
</div>