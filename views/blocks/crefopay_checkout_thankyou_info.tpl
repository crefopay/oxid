[{$smarty.block.parent}]

[{assign var="oOxidPayment" value=$order->crefopayGetOxidPayment($order)}]
[{if $oOxidPayment->isCrefopayPaymentMethod($paymentMethod)}]
    [{assign var="oCrefopayTransaction" value=$order->crefopayGetTransaction()}]
    [{if $oCrefopayTransaction && !$oCrefopayTransaction->hasBasketZeroSum()}]
        [{assign var="aCrefopayAdditionalData" value=$oCrefopayTransaction->getPaymentAdditionalData()}]
        [{if count($aCrefopayAdditionalData)>0}]
            <div class="cp-additional-payment-info">
                [{if $oOxidPayment->getId() == 'cpprepaid'}]
                <p>
                    <br>[{oxmultilang ident="CREFOPAY_PREPAID_INFO" args=$oOxidPayment->getCrefopayPrepaidPeriod()}]<br>
                </p>
                [{elseif $oOxidPayment->getId() == 'cpbill'}]
                <p>
                    <br>[{oxmultilang ident="CREFOPAY_BILL_INFO" args=$oOxidPayment->getCrefopayBillPeriod()}]<br>
                </p>
                [{/if}]
                <table class="table table-striped">
                    <tbody>
                    [{foreach key=cpKey item=cpValue from=$aCrefopayAdditionalData}]
                        <tr>
                            <th>[{oxmultilang ident=$oViewConf->crefopayGetTranslationIdent($cpKey)}]:</th>
                            <td>[{$cpValue}]</td>
                        </tr>
                    [{/foreach}]
                    </tbody>
                </table>
            </div>
        [{/if}]
    [{/if}]

    [{* Force new crefopay transaction when thankyou was rendered *}]
    [{$oViewConf->crefopayResetSession()}]
[{/if}]