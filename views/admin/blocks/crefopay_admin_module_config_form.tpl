[{assign var="sModuleId" value=$oModule->getInfo('id')}]

[{if $sModuleId === "cppayments"}]
    [{capture assign="crefopayjs"}]
        document.addEventListener("DOMContentLoaded", function() {
            var transferForm=document.getElementById('transfer');
            transferForm.cl.value='crefopay_admin_settings';
            transferForm.submit();
        });
    [{/capture}]
    [{oxscript add=$crefopayjs}]

    <div style="display:none;">
        [{$smarty.block.parent}]
    </div>
[{else}]
    [{$smarty.block.parent}]
[{/if}]