<?php

// Moduloptionen
$aLang = array(
    'charset' => 'UTF-8',

    'crefopay_admin_ordertab'           => 'CrefoPay',
    'crefopay_admin_transactions_frame' => 'CrefoPay-Transactions',
    'crefopay_admin_transactions_main'  => 'Details',
    'crefopay_admin_usertab'            => 'CrefoPay',


    'CREFOPAY_TRANSACTION_CREATED' => 'Transaction created',
    'CREFOPAY_TRANSACTION_UPDATED' => 'Last update',
    'CREFOPAY_NONCPORDER'          => 'This order did not use a CrefoPay paymentmethod or no data available',
    'CREFOPAY_INVALIDCREDENTIALS'  => 'The credentials seem to be invalid',
    'CREFOPAY_VALID_CREDENTIALS'   => 'The credentials could be validated',


    'CREFOPAY_SELECT_CPORDER'       => 'Please select a transaction above to see details',
    'CREFOPAY_TRANSACTIONSTATUS'    => 'State',
    'CREFOPAY_ORDERID'              => 'Crefopay Order-ID',
    'CREFOPAY_OXORDERNR'            => 'Oxid Ordernumber',
    'CREFOPAY_PAYMENTMETHOD'        => 'Payment-Method',
    'CREFOPAY_CUSTOMER'             => 'Customer',
    'CREFOPAY_ALLSTATES'            => 'Show all',
    'CREFOPAY_CREATE_ORDER_BTT'     => 'Create oxid order now despite incomplete payment process',
    'CREFOPAY_CREATE_ORDER_BTT_DISABLED' => 'Corresponding oxid order already exists',
    'CREFOPAY_POSITIONS'            => 'Positions',
    'CREFOPAY_NOTPROGRESSED_ENOUGH' => 'For this order, the order data has not yet been serialized. This means that usually the order process was not yet sufficiently advanced to start a real payment attempt.',
    'CREFOPAY_ORDERDATA'            => 'Order data',

    'CREFOPAY_TRANSACTIONS_MEANING'                    => 'What the respective transaction status means:',
    'CREFOPAY_TRANSACTIONSTATEDESC_NEW'                => 'The transaction was created in the CrefoPay system after the customer accessed the payment methods page',
    'CREFOPAY_TRANSACTIONSTATEDESC_ACKNOWLEDGEPENDING' => 'The customer has triggered the "Pay now" button.',
    'CREFOPAY_TRANSACTIONSTATEDESC_FRAUDPENDING'       => 'The payment transaction was classified as suspicious and is checked manually',
    'CREFOPAY_TRANSACTIONSTATEDESC_MERCHANTPENDING'    => 'The payment process has been successfully completed. CrefoPay is waiting for dispatch notification',
    'CREFOPAY_TRANSACTIONSTATEDESC_CIAPENDING'         => 'The payment process was successful. CrefoPay is waiting for the receipt of money (for prepayment/Paypal)',
    'CREFOPAY_TRANSACTIONSTATEDESC_EXPIRED'            => 'The customer has not completed the payment process within the shopping cart validity period (see Crefopay settings)',
    'CREFOPAY_TRANSACTIONSTATEDESC_CANCELLED'          => 'The payment process was cancelled',
    'CREFOPAY_TRANSACTIONSTATEDESC_FRAUDCANCELLED'     => 'The payment process was cancelled due to suspected fraud',
    'CREFOPAY_TRANSACTIONSTATEDESC_DONE'               => 'The payment process has been completed',
    'CREFOPAY_RISKCLASS'                               => 'Risk class',
    'CREFOPAY_RISKCLASS_DEFAULT'                       => 'Standard',
    'CREFOPAY_RISKCLASS_TRUSTED'                       => 'Trusted',
    'CREFOPAY_RISKCLASS_HIGHRISK'                      => 'High risk',
    'CREFOPAY_RISKCLASS_INFOTEXT'                      => "The risk class specified here is used as the default value when creating a new CrefoPay transaction<br><br>The customer's risk class has already been determined and you are using an ERP-/import interface?<br>You can write the risk-class to the field oxuser.crefopay_userriskclass (possible values: 1=standard, 0=trusted, 2=high risk)",
    'CREFOPAY_SAVED_DATA'                              => 'Data stored at CrefoPay',
    'CREFOPAY_VIEWS_HINT'                              => 'Warning: The database schema does not seem to match the module version. Has the module not been reactivated after an update?',
    'CREFOPAY_DELETE_LOGFILES_OLDERTHAN'               => 'Delete logfiles older than (days)',
    'CREFOPAY_DELETE_EXPIREDTRANSACTIONS_OLDERTHAN'    => 'Delete expired transactions older than (days)',

    'CREFOPAY_CREATESOURCE'                            => 'Order was created via',
    'CREFOPAY_CREATEDBYUSER'                           => 'Order was created by',
    'CREFOPAY_NOTSET'                                  => 'not set',
);
