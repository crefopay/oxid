[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

<script type="text/javascript">
    <!--
    function editThis( sID )
    {
        var oTransfer = top.basefrm.edit.document.getElementById("transfer");
        oTransfer.oxid.value = sID;
        oTransfer.cl.value = top.basefrm.list.sDefClass;

        //forcing edit frame to reload after submit
        top.forceReloadingEditFrame();

        var oSearch = top.basefrm.list.document.getElementById("search");
        oSearch.oxid.value = sID;
        oSearch.actedit.value = 0;
        oSearch.submit();
    }
    [{if !$oxparentid}]
    window.onload = function ()
    {
        [{if $updatelist == 1}]
        top.oxid.admin.updateList('[{$oxid}]');
        [{/if}]
        var oField = top.oxid.admin.getLockTarget();
        oField.onchange = oField.onkeyup = oField.onmouseout = top.oxid.admin.unlockSave;
    }
        [{/if}]
    //-->
</script>

<style type="text/css">
    .crefopay-postable > thead > tr > td{
        background-color: black;
        color:white;
        padding-left:5px;
        padding-top: 5px;
        padding-right: 15px;
        padding-bottom: 5px;
    }
</style>

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
    [{else}]
    [{assign var="readonly" value=""}]
    [{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="oxidCopy" value="[{$oxid}]">
    <input type="hidden" name="cl" value="crefopay_admin_transactions_main">
    <input type="hidden" name="editlanguage" value="[{$editlanguage}]">
</form>

<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post" style="padding: 0px;margin: 0px;height:0px;">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="crefopay_admin_transactions_main">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="oxid" value="[{$oxid}]">
</form>

[{if !$cpTransaction}][{oxmultilang ident="CREFOPAY_SELECT_CPORDER"}][{/if}]

[{if $cpTransaction}]
    <table>
        <tr>
            <td style="vertical-align: top;">
                <!-- left column -->
                <table>
                    <tr>
                        <td>[{oxmultilang ident="CREFOPAY_TRANSACTION_CREATED" suffix="COLON"}]</td>
                        <td>&nbsp;</td>
                        <td>[{$cpTransaction->getCrefopayCreated()|escape}]</td>
                    </tr>
                    <tr>
                        <td>CrefoPay Order-ID:</td>
                        <td>&nbsp;</td>
                        <td>[{$cpTransaction->getCrefopayOrderId()|escape}]</td>
                    </tr>
                    <tr>
                        <td>[{oxmultilang ident="CREFOPAY_TRANSACTION_UPDATED" suffix="COLON"}]</td>
                        <td>&nbsp;</td>
                        <td>[{$cpTransaction->getCrefopayUpdated()|escape}]</td>
                    </tr>
                    <tr>
                        <td>CrefoPay OrderState:</td>
                        <td>&nbsp;</td>
                        <td>
                            [{if $cpTransaction->getCrefopayOrderStatus()}]
                                [{$cpTransaction->getCrefopayOrderStatus()|escape}]
                            [{else}]
                            -
                            [{/if}]
                        </td>
                    </tr>
                    <tr>
                        <td>CrefoPay TransactionState:</td>
                        <td>&nbsp;</td>
                        <td>[{$cpTransaction->getCrefopayTransactionStatus()|escape}]</td>
                    </tr>
                    <tr>
                        <td>CrefoPay Payment Method:</td>
                        <td>&nbsp;</td>
                        <td>
                            [{assign var="paymentMethod" value=$cpTransaction->getCrefopayPaymentMethod()}]
                            [{$paymentMethod|escape}]
                            [{if ($paymentMethod eq 'CC') or ($paymentMethod eq 'CC3D')}]
                                [{assign var="ccIssuer" value=$cpTransaction->getCrefopayCCIssuer()}]
                                [{if $ccIssuer}] (Issuer: [{$ccIssuer|escape}])[{/if}]
                            [{/if}]
                        </td>
                    </tr>
                    <tr>
                        <td>CrefoPay CC-Issuer:</td>
                        <td>&nbsp;</td>
                        <td>[{$cpTransaction->getCrefopayCCIssuer()|escape}]</td>
                    </tr>
                    <tr>
                        <td>CrefoPay Risk Class:</td>
                        <td>&nbsp;</td>
                        <td>
                            [{assign var="iRiskClass" value=$cpTransaction->getCrefopayRiskClass()}]
                            [{if $iRiskClass eq 0}]Trusted[{/if}]
                            [{if $iRiskClass eq 1}]Default[{/if}]
                            [{if $iRiskClass eq 2}]HighRisk[{/if}]
                        </td>
                    </tr>
                </table>

                <div>&nbsp;</div>

                [{assign var="bHasAdditionalData" value=false}]
                [{capture assign="additionalData"}]
                    [{assign var="aPaymentData" value=$cpTransaction->getCrefopayAdditionalData()}]
                    [{if is_array($aPaymentData)}]
                        <div><b>Payment-Info:</b></div>
                        <table>
                            [{foreach from=$aPaymentData key="paramName" item="paramValue"}]
                                [{if $paramValue && ($paramName!='CPORDERID' && ($paramName!='CPCREATEDAT'))}]
                                    [{assign var="bHasAdditionalData" value=true}]
                                    <tr>
                                        <td>[{$paramName|escape}]:</td>
                                        <td>&nbsp;</td>
                                        <td>[{$paramValue|escape}]</td>
                                    </tr>
                                [{/if}]
                            [{/foreach}]
                        </table>
                    [{/if}]
                [{/capture}]

                [{if $bHasAdditionalData}][{$additionalData}][{/if}]

                <div><b>[{oxmultilang ident="CREFOPAY_TRANSACTIONS_MEANING"}]</b></div>
                [{assign var="aTransactionStates" value=$oView->getTransactionStates()}]
                <table>
                    [{foreach from=$aTransactionStates item="sTransactionState"}]
                        <tr>
                            <td style="vertical-align: top;">[{$sTransactionState|escape}]</td>
                            <td>&nbsp;</td>
                            <td style="vertical-align: top;">[{oxmultilang ident="CREFOPAY_TRANSACTIONSTATEDESC_$sTransactionState"}]</td>
                        </tr>
                    [{/foreach}]
                </table>
            </td>
            <td style="width:50px;">
                <!-- spacer -->
            </td>
            <td style="vertical-align: top;">
                <!-- right column -->

                [{if $cpTransaction->getOxidOrderNr()}]
                    <div style="margin-bottom: 10px;">
                        [{oxmultilang ident="CREFOPAY_OXORDERNR" suffix="COLON"}] <a class="jumplink" href="[{$oViewConf->getSelfLink()}]cl=admin_order&oxid=[{$cpTransaction->getOxidOrderId()|rawurlencode}]" target="basefrm">[{$cpTransaction->getOxidOrderNr()}]</a><br>
                    </div>
                [{/if}]

                [{assign var="oOrder" value=$oView->getDeserializedOrderFromTransaction($cpTransaction)}]
                [{if $oOrder}]
                    [{assign var="oOrderCurrency" value=$oOrder->getOrderCurrency()}]

                    [{if $cpTransaction->getOrderFromDb()}]
                        <div>
                            <button type="button" disabled>[{oxmultilang ident="CREFOPAY_CREATE_ORDER_BTT_DISABLED"}]</button>
                        </div>
                    [{else}]
                        <div>
                            <form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post" style="padding: 0px;margin: 0px;">
                                [{$oViewConf->getHiddenSid()}]
                                <input type="hidden" name="cl" value="crefopay_admin_transactions_main">
                                <input type="hidden" name="fnc" value="createOrder">
                                <input type="hidden" name="oxid" value="[{$oxid}]">

                                <button type="submit">[{oxmultilang ident="CREFOPAY_CREATE_ORDER_BTT"}]</button>
                            </form>
                        </div>
                    [{/if}]
                    <div style="margin-top:20px;">
                        <div><b>[{oxmultilang ident="GENERAL_BILLADDRESS"}]</b></div>
                        <div>[{$oOrder->oxorder__oxcompany->value}]</div>
                        <div>[{$oOrder->oxorder__oxbillfname->value}] [{$oOrder->oxorder__oxbilllname->value}]</div>
                        <div>[{$oOrder->oxorder__oxbillstreet->value}] [{$oOrder->oxorder__oxbillstreetnr->value}]</div>
                        <div>[{$oOrder->oxorder__oxbillzip->value}] [{$oOrder->oxorder__oxbillcity->value}]</div>
                        <div>[{$oOrder->oxorder__oxusername->value}]</div>

                        <div>&nbsp;</div>
                        <div><b>[{oxmultilang ident="CREFOPAY_ORDERDATA" suffix="COLON"}]</b></div>
                        <table>
                            <tr>
                                <td>[{oxmultilang ident="CREFOPAY_OXORDERNR" suffix="COLON"}]</td>
                                <td>[{$oOrder->oxorder__oxordernr->value}]</td>
                            </tr>
                            <tr>
                                <td>[{oxmultilang ident="GENERAL_DISCOUNT" suffix="COLON"}]</td>
                                <td>[{oxprice price=$oOrder->oxorder__oxdiscount->value currency=$oOrderCurrency}]</td>
                            </tr>
                            <tr>
                                <td>[{oxmultilang ident="GENERAL_DELIVERYCOST" suffix="COLON"}]</td>
                                <td>[{oxprice price=$oOrder->oxorder__oxdelcost->value currency=$oOrderCurrency}]</td>
                            </tr>
                            [{if $oOrder->oxorder__oxpaycost->value}]
                            <tr>
                                <td>[{oxmultilang ident="GENERAL_PAYCOST" suffix="COLON"}]</td>
                                <td>[{oxprice price=$oOrder->oxorder__oxpaycost->value currency=$oOrderCurrency}]</td>
                            </tr>
                            [{/if}]
                            [{if $oOrder->oxorder__oxwrapcost->value}]
                            <tr>
                                <td>[{oxmultilang ident="WRAPPING_COSTS" suffix="COLON"}]</td>
                                <td>[{oxprice price=$oOrder->oxorder__oxwrapcost->value currency=$oOrderCurrency}]</td>
                            </tr>
                            [{/if}]
                            <tr>
                                <td>[{oxmultilang ident="GIFTCARD_COSTS" suffix="COLON"}]</td>
                                <td>[{oxprice price=$oOrder->oxorder__oxgiftcardcost->value currency=$oOrderCurrency}]</td>
                            </tr>
                            <tr>
                                <td><b>[{oxmultilang ident="GENERAL_SUMTOTAL" suffix="COLON"}]</b></td>
                                <td><b>[{oxprice price=$oOrder->oxorder__oxtotalordersum->value currency=$oOrderCurrency}]</b></td>
                            </tr>
                        </table>

                        <div>&nbsp;</div>
                        <div><b>[{oxmultilang ident="CREFOPAY_POSITIONS" suffix="COLON"}]</b></div>
                        [{assign var="aPositions" value=$oOrder->getOrderArticles()}]
                        <table class="crefopay-postable">
                            <thead>
                                <tr>
                                    <td>[{oxmultilang ident="amount"}]</td>
                                    <td>[{oxmultilang ident="GENERAL_ARTICLE_OXARTNUM"}]</td>
                                    <td>[{oxmultilang ident="GENERAL_ARTICLE_OXTITLE"}]</td>
                                    <td style="text-align: right;">[{oxmultilang ident="GENERAL_ARTICLE_OXBPRICE"}]</td>
                                </tr>
                            </thead>
                            <tbody>
                                [{foreach from=$aPositions key="itemKey" item="oPosition"}]
                                    <tr>
                                        <td>[{$oPosition->oxorderarticles__oxamount->value}]</td>
                                        <td>[{$oPosition->oxorderarticles__oxartnum->value}]</td>
                                        <td>[{$oPosition->oxorderarticles__oxtitle->value}]</td>
                                        <td style="text-align: right;">[{oxprice price=$oPosition->oxorderarticles__oxbprice->value currency=$oOrderCurrency}]</td>
                                    </tr>
                                [{/foreach}]
                            </tbody>
                        </table>
                    </div>
                [{else}]
                    [{oxmultilang ident="CREFOPAY_NOTPROGRESSED_ENOUGH"}]
                [{/if}]
            </td>
        </tr>
    </table>
[{/if}]

<div style="height:50px;">
    <!-- spacer -->
</div>

[{include file="bottomnaviitem.tpl"}]

[{include file="bottomitem.tpl"}]
