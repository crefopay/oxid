[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
    [{else}]
    [{assign var="readonly" value=""}]
    [{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="order_main">
</form>

[{if !$cpTransaction}]
    [{oxmultilang ident="CREFOPAY_NONCPORDER"}]
    [{else}]

    <table>
        <tr>
            <td>[{oxmultilang ident="CREFOPAY_TRANSACTION_CREATED" suffix="COLON"}]</td>
            <td>&nbsp;</td>
            <td>[{$cpTransaction->getCrefopayCreated()|escape}]</td>
        </tr>
        <tr>
            <td>CrefoPay Order-ID:</td>
            <td>&nbsp;</td>
            <td>[{$cpTransaction->getCrefopayOrderId()|escape}]</td>
        </tr>
        <tr>
            <td>[{oxmultilang ident="CREFOPAY_TRANSACTION_UPDATED" suffix="COLON"}]</td>
            <td>&nbsp;</td>
            <td>[{$cpTransaction->getCrefopayUpdated()|escape}]</td>
        </tr>
        <tr>
            <td>CrefoPay OrderState:</td>
            <td>&nbsp;</td>
            <td>
                [{if $cpTransaction->getCrefopayOrderStatus()}]
                [{$cpTransaction->getCrefopayOrderStatus()|escape}]
                [{else}]
                -
                [{/if}]
            </td>
        </tr>
        <tr>
            <td>CrefoPay TransactionState:</td>
            <td>&nbsp;</td>
            <td>[{$cpTransaction->getCrefopayTransactionStatus()|escape}]</td>
        </tr>
        <tr>
            <td>CrefoPay Payment Method:</td>
            <td>&nbsp;</td>
            <td>[{$cpTransaction->getCrefopayPaymentMethod()|escape}]</td>
        </tr>
        <tr>
            <td>CrefoPay Risk Class:</td>
            <td>&nbsp;</td>
            <td>
                [{assign var="iRiskClass" value=$cpTransaction->getCrefopayRiskClass()}]
                [{if $iRiskClass eq 0}]Trusted[{/if}]
                [{if $iRiskClass eq 1}]Default[{/if}]
                [{if $iRiskClass eq 2}]HighRisk[{/if}]
            </td>
        </tr>
        <tr>
            <td>[{oxmultilang ident="CREFOPAY_CREATESOURCE" suffix="COLON"}]</td>
            <td>&nbsp;</td>
            <td>
                [{assign var="sCreateSource" value=$oOrder->getCrefopayCreateSource()}]
                [{if $sCreateSource eq 'backend'}]OXID-Backend[{/if}]
                [{if $sCreateSource eq 'MNS'}]Crefopay-Cronjob[{/if}]
                [{if $sCreateSource eq 'thankyou'}]Frontend (With Redirect)[{/if}]
                [{if $sCreateSource eq 'regular'}]Frontend (Without Redirect)[{/if}]
                [{if !$sCreateSource}]([{oxmultilang ident="CREFOPAY_NOTSET"}])[{/if}]
            </td>
        </tr>
        <tr>
            <td>[{oxmultilang ident="CREFOPAY_CREATEDBYUSER" suffix="COLON"}]</td>
            <td>&nbsp;</td>
            <td>
                [{assign var="oCreatedByUser" value=$oOrder->getCrefopayCreatedByUser()}]
                [{if $oCreatedByUser}][{$oCreatedByUser->oxuser__oxusername->value}][{/if}]
                [{if !$oCreatedByUser}]([{oxmultilang ident="CREFOPAY_NOTSET"}])[{/if}]
            </td>
        </tr>
    </table>

    <div>&nbsp;</div>

    [{assign var="bHasAdditionalData" value=false}]
    [{capture assign="additionalData"}]
    [{assign var="aPaymentData" value=$cpTransaction->getCrefopayAdditionalData()}]
    [{if is_array($aPaymentData)}]
    <div><b>Payment-Info:</b></div>
    <table>
        [{foreach from=$aPaymentData key="paramName" item="paramValue"}]
        [{if $paramValue && ($paramName!='CPORDERID' && ($paramName!='CPCREATEDAT'))}]
        [{assign var="bHasAdditionalData" value=true}]
        <tr>
            <td>[{$paramName|escape}]:</td>
            <td>&nbsp;</td>
            <td>[{$paramValue|escape}]</td>
        </tr>
        [{/if}]
        [{/foreach}]
    </table>
    [{/if}]
    [{/capture}]

    [{if $bHasAdditionalData}][{$additionalData}][{/if}]
    [{/if}]

[{include file="bottomnaviitem.tpl"}]

[{include file="bottomitem.tpl"}]
