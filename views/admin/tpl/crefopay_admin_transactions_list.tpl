[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]
[{assign var="where" value=$oView->getListFilter()}]

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
    [{else}]
    [{assign var="readonly" value=""}]
    [{/if}]

<script type="text/javascript">
    <!--
    window.onload = function ()
    {
        top.reloadEditFrame();
        [{if $updatelist == 1}]
        top.oxid.admin.updateList('[{$oxid}]');
        [{/if}]
    }
    //-->
</script>


<div id="liste">

    <form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
        [{include file="_formparams.tpl" cl="crefopay_admin_transactions_list" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="" language=$actlang editlanguage=$actlang}]
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <colgroup>
                <col width="16%">
                <col width="16%">
                <col width="16%">
                <col width="16%">
                <col width="16%">
                <col width="16%">
            </colgroup>

            <!-- Filter -->
            <tr class="listitem">
                <td valign="top" class="listfilter" align="left">
                    <div class="r1"><div class="b1">
                            <input class="listedit" type="text" size="9" maxlength="128" name="where[crefopay_transactions][cpcreated]" value="[{$where.crefopay_transactions.cpcreated}]">
                        </div></div>
                </td>
                <td height="20" valign="middle" class="listfilter" nowrap>
                    <div class="r1"><div class="b1">
                            [{assign var="aTransactionStates" value=$oView->getTransactionStates()}]
                            <select name="where[crefopay_transactions][cptransactionstatus]" class="editinput" onChange="Javascript:document.search.submit();">
                                <option value="" [{if $where.crefopay_transactions.cptransactionstatus eq ''}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_ALLSTATES"}]</option>
                                [{foreach from=$aTransactionStates item="sTransactionState"}]
                                    <option value="[{$sTransactionState}]" [{if $where.crefopay_transactions.cptransactionstatus eq $sTransactionState}]selected[{/if}]>[{$sTransactionState}]</option>
                                [{/foreach}]
                            </select>
                        </div></div>
                </td>
                <td valign="top" class="listfilter" align="left">
                    <div class="r1"><div class="b1">
                            <input class="listedit" type="text" size="9" maxlength="128" name="where[crefopay_transactions][cporderid]" value="[{$where.crefopay_transactions.cporderid}]">
                        </div></div>
                </td>
                <td valign="top" class="listfilter" align="left">
                    <div class="r1"><div class="b1">
                            <input class="listedit" type="text" size="9" maxlength="128" name="where[crefopay_transactions][oxordernr]" value="[{$where.crefopay_transactions.oxordernr}]">
                        </div></div>
                </td>
                <td height="20" valign="middle" class="listfilter" nowrap>
                    <div class="r1"><div class="b1">
                            <select name="where[crefopay_transactions][cppaymentmethod]" class="editinput" onChange="Javascript:document.search.submit();">
                                <option value="" [{if $where.crefopay_transactions.cppaymentmethod eq ''}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_ALLSTATES"}]</option>
                                [{assign var="cpPaymentMethods" value=$oView->getCrefopayPaymentMethods()}]
                                [{foreach from=$cpPaymentMethods item="cpPaymentMethod"}]
                                    <option value="[{$cpPaymentMethod.crefopay_paymentmethodid}]" [{if $where.crefopay_transactions.cppaymentmethod eq $cpPaymentMethod.crefopay_paymentmethodid}]selected[{/if}]>[{$cpPaymentMethod.crefopay_paymentmethodid}] - [{$oView->getPaymentMethodTitle($cpPaymentMethod)|escape}]</option>
                                [{/foreach}]
                            </select>
                        </div></div>
                </td>
                <td valign="top" class="listfilter" align="left">
                    <div class="r1"><div class="b1">
                            <input class="listedit" type="text" size="9" maxlength="128" name="where[crefopay_transactions][cptransactionjson]" value="[{$where.crefopay_transactions.cptransactionjson}]">
                        </div></div>
                    <div class="find" style="display:none;"><input class="listedit" type="submit" name="submitit" value="Suchen"></div>
                </td>
            </tr>

            <!-- Column Names -->
            <tr class="listitem">
                <td class="listheader first" height="15">
                    <a href="Javascript:top.oxid.admin.setSorting( document.search, 'crefopay_transactions', 'cpcreated', 'desc');document.search.submit();" class="listheader">
                        [{oxmultilang ident="CREFOPAY_TRANSACTION_CREATED"}]
                    </a>
                </td>
                <td class="listheader">
                    <a href="Javascript:top.oxid.admin.setSorting( document.search, 'crefopay_transactions', 'cptransactionstatus', 'asc');document.search.submit();" class="listheader">
                        [{oxmultilang ident="CREFOPAY_TRANSACTIONSTATUS"}]
                    </a>
                </td>
                <td class="listheader">
                    <a href="Javascript:top.oxid.admin.setSorting( document.search, 'crefopay_transactions', 'cporderid', 'asc');document.search.submit();" class="listheader">
                        [{oxmultilang ident="CREFOPAY_ORDERID"}]
                    </a>
                </td>
                <td class="listheader">
                    <a href="Javascript:top.oxid.admin.setSorting( document.search, 'crefopay_transactions', 'oxordernr', 'asc');document.search.submit();" class="listheader">
                        [{oxmultilang ident="CREFOPAY_OXORDERNR"}]
                    </a>
                </td>
                <td class="listheader">
                    <a href="Javascript:top.oxid.admin.setSorting( document.search, 'crefopay_transactions', 'cppaymentmethod', 'asc');document.search.submit();" class="listheader">
                        [{oxmultilang ident="CREFOPAY_PAYMENTMETHOD"}]
                    </a>
                </td>
                <td class="listheader">
                    <a href="Javascript:top.oxid.admin.setSorting( document.search, 'crefopay_transactions', 'cppaymentmethod', 'asc');document.search.submit();" class="listheader">
                        [{oxmultilang ident="CREFOPAY_CUSTOMER"}]
                    </a>
                </td>
            </tr>
            <!-- /Column Names -->

            [{assign var="blWhite" value=""}]
            [{assign var="_cnt" value=0}]
            [{foreach from=$mylist item=listitem}]
            [{assign var="_cnt" value=$_cnt+1}]
            <tr id="row.[{$_cnt}]">

                [{if $listitem->blacklist == 1}]
                [{assign var="listclass" value=listitem3}]
                [{else}]
                [{assign var="listclass" value=listitem$blWhite}]
                [{/if}]
                [{if $listitem->oxarticles__oxid->value == $oxid}]
                [{assign var="listclass" value=listitem4}]
                [{/if}]
                <td valign="top" class="[{$listclass}]"><div class="listitemfloating"><a href="Javascript:top.oxid.admin.editThis('[{$listitem->getId()}]');" class="[{$listclass}]">[{$listitem->getCrefopayCreated()|escape}]</a></div></td>
                <td valign="top" class="[{$listclass}]"><div class="listitemfloating"><a href="Javascript:top.oxid.admin.editThis('[{$listitem->getId()}]');" class="[{$listclass}]">[{$listitem->getCrefopayTransactionStatus()|escape}]</a></div></td>
                <td valign="top" class="[{$listclass}]"><div class="listitemfloating"><a href="Javascript:top.oxid.admin.editThis('[{$listitem->getId()}]');" class="[{$listclass}]">[{$listitem->getCrefopayOrderId()|escape}]</a></div></td>
                <td valign="top" class="[{$listclass}]"><div class="listitemfloating"><a href="Javascript:top.oxid.admin.editThis('[{$listitem->getId()}]');" class="[{$listclass}]">[{$listitem->getOxidOrderNr()|escape}]</a></div></td>
                <td valign="top" class="[{$listclass}]"><div class="listitemfloating"><a href="Javascript:top.oxid.admin.editThis('[{$listitem->getId()}]');" class="[{$listclass}]">[{$listitem->getCrefopayPaymentMethod()|escape}]</a></div></td>
                <td valign="top" class="[{$listclass}]"><div class="listitemfloating"><a href="Javascript:top.oxid.admin.editThis('[{$listitem->getId()}]');" class="[{$listclass}]">[{$listitem->getCustomerNameForBackend()|escape}]</a></div></td>
            </tr>
            [{if $blWhite == "2"}]
            [{assign var="blWhite" value=""}]
            [{else}]
            [{assign var="blWhite" value="2"}]
            [{/if}]
            [{/foreach}]
            [{include file="pagenavisnippet.tpl" colspan="5"}]
        </table>
    </form>
</div>

[{include file="pagetabsnippet.tpl"}]

<script type="text/javascript">
    if (parent.parent)
    {   parent.parent.sShopTitle   = "[{$actshopobj->oxshops__oxname->getRawValue()|oxaddslashes}]";
        parent.parent.sMenuItem    = "[{oxmultilang ident="GENERAL_MENUITEM"}]";
        parent.parent.sMenuSubItem = "[{oxmultilang ident="ARTICLE_LIST_MENUSUBITEM"}]";
        parent.parent.sWorkArea    = "[{$_act}]";
        parent.parent.setTitle();
    }
</script>
</body>
</html>