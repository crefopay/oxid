[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="box"}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="cppayments">
    <input type="hidden" name="cl" value="module_config">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="actshop" value="[{$oViewConf->getActiveShopId()}]">
    <input type="hidden" name="updatenav" value="">
    <input type="hidden" name="editlanguage" value="[{$editlanguage}]">
</form>

<form name="module_configuration" id="moduleConfiguration" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="crefopay_admin_settings">
    <input type="hidden" name="fnc" value="save">
    <input type="hidden" name="actshop" value="[{$oViewConf->getActiveShopId()}]">

    <style type="text/css">
        a {
            text-decoration: underline;
            color: blue;
        }
    </style>

    <div><img src="[{$oViewConf->getModuleUrl('cppayments','/out/src/img/logo.png')}]"></div>
    <div>Vielen Dank für die Nutzung von CrefoPay!</div>
    <div>Sie sind noch kein Kunde bei CrefoPay? Lassen Sie sich jetzt anbinden: <a target="_blank"
                                                                                   href="https://www.crefopay.de/kontaktformular">Hier
            klicken</a></div>

    <div>&nbsp;</div>
    <table>
        <tr>
            <td style="vertical-align: top;"><b>Modus:</b></td>
            <td style="vertical-align: top;">
                <table>
                    <tr>
                        <td style="vertical-align: top;"><input type="radio" name="cpSetting[CrefoPaySystemMode]" value="0" [{if $cpSetting.CrefoPaySystemMode eq 0}]checked[{/if}]></td>
                        <td style="vertical-align: top;"><b>Sandbox</b><br>Nutzen Sie diesen Modus, um das Modul vor
                            Liveschaltung zu testen, ohne reale Buchungen zu erzeugen
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><input type="radio" name="cpSetting[CrefoPaySystemMode]" value="1" [{if $cpSetting.CrefoPaySystemMode eq 1}]checked[{/if}]></td>
                        <td style="vertical-align: top;"><b>Live</b><br>Das Modul ist scharf geschaltet</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><b>Zugangsdaten:</b></td>
            <td style="vertical-align: top;">
                Sie können die hier einzutragenden Zugangsdaten über das <a href="mailto:service@crefopay.de">Crefopay-Integrationsteam</a>
                erhalten:
                <table>
                    <tr>
                        <td style="vertical-align: top;">Merchant ID:</td>
                        <td style="vertical-align: top;">
                            <input autocomplete="off" type="text" name="cpSetting[CrefoPayMerchantId]" value="[{$cpSetting.CrefoPayMerchantId}]">
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Shop ID:</td>
                        <td style="vertical-align: top;">
                            <input autocomplete="off" type="text" name="cpSetting[CrefoPayStoreId]" value="[{$cpSetting.CrefoPayStoreId}]">
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Privater Schlüssel:</td>
                        <td style="vertical-align: top;">
                            <input autocomplete="off" type="password" name="cpSetting[CrefoPayPrivateKey]" value="[{$cpSetting.CrefoPayPrivateKey}]">
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Öffentlicher Schlüssel:</td>
                        <td style="vertical-align: top;">
                            <input autocomplete="off" style="width:500px;" type="text" name="cpSetting[CrefoPayShopPublicKey]" value="[{$cpSetting.CrefoPayShopPublicKey}]"></td>
                    </tr>
                </table>
                [{if $apiTestResult==='OK'}]
                    <div style="font-weight: bold;color:darkgreen;">[{oxmultilang ident="CREFOPAY_VALID_CREDENTIALS"}]</div>
                [{else}]
                    <div style="font-weight: bold;color:red;">[{$apiTestResult}]</div>
                [{/if}]
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><b>Cronjob:</b></td>
            <td style="vertical-align: top;">
                <div>[{$sCronjobUrl}]</div>
                <div>Rufen Sie diese URL bitte regelmäßig auf (z.B. alle 5min). Dieser Cronjob finalisiert Transaktionen, die ein Versanddatum in Oxid erhalten haben und räumt die Crefopay-Transaktionstabelle auf</div>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><b>Bestell-Einstellungen:</b></td>
            <td style="vertical-align: top;">
                <table>
                    <tr>
                        <td style="vertical-align: top;"><input autocomplete="off" type="checkbox" name="cpSetting[CrefoPayAutoCapture]" [{if $cpSetting.CrefoPayAutoCapture}]checked[{/if}] value="1"></td>
                        <td style="vertical-align: top;">Zahlungstransaktionen bei Bestellabschluss sofort ausführen
                            (Auto-Capture).<br>Achtung: Nur in Absprache mit dem Integrations-Team aktivieren! Regulär
                            wird die Zahlungstransaktion erst bei Versand durchgeführt.
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><input autocomplete="off" type="checkbox" name="cpSetting[CrefoPayB2BEnabled]" [{if $cpSetting.CrefoPayB2BEnabled}]checked[{/if}] value="1"></td>
                        <td style="vertical-align: top;">Business-Transaktionen<br>Überträgt Kundendaten von Firmen als
                            'Company' statt als 'Person'.<br>Achtung: Diese Funktion sollten Sie nur aktivieren, wenn
                            die Abwicklung von B2B Transaktionen Bestandteil Ihres CrefoPay Händlervertrages ist.
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><input autocomplete="off" type="checkbox" name="cpSetting[CrefoPayPreAssignOrderNr]" [{if $cpSetting.CrefoPayPreAssignOrderNr}]checked[{/if}] value="1"></td>
                        <td style="vertical-align: top;">Auftragnr schon vor Bestellabschluss vergeben. Dadurch kann die Bestellnummer schon mit nach CrefoPay übergeben werden. Es können aber durch abgebrochene Bestellungen Lücken im Bestellnummernkreis entstehen.</td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <td style="vertical-align: middle;">jQuery:</td>
                        <td style="vertical-align: top;">
                            <select autocomplete="off" name="cpSetting[CrefoPayUseModuleJQuery]">
                                <option value="0" [{if $cpSetting.CrefoPayUseModuleJQuery eq 0}]selected[{/if}]>jQuery-Library des Themes verwenden</option>
                                <option value="1" [{if $cpSetting.CrefoPayUseModuleJQuery eq 1}]selected[{/if}]>Eigene jQuery-Library verwenden (Auswählen, wenn das Theme kein jQuery verwendet)</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">Geburtsdatum:</td>
                        <td style="vertical-align: top;">
                            <select autocomplete="off" name="cpSetting[CrefoPayDOBUsage]">
                                <option value="0" [{if $cpSetting.CrefoPayDOBUsage eq 0}]selected[{/if}]>Falls nicht vorhanden abfragen und im Kundenstamm speichern</option>
                                <option value="1" [{if $cpSetting.CrefoPayDOBUsage eq 1}]selected[{/if}]>Falls nicht vorhanden abfragen, aber nicht im Kundenstamm speichern</option>
                                <option value="2" [{if $cpSetting.CrefoPayDOBUsage eq 2}]selected[{/if}]>Nicht abfragen, falls nicht vorhanden</option>
                            </select>
                        </td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <td style="vertical-align: top;">Bestellnummer-Präfix</td>
                        <td style="vertical-align: top;"><input type="text" name="cpSetting[CrefoPayOrderPrefix]" value="[{$cpSetting.CrefoPayOrderPrefix}]"><br>Den
                            Crefopay-Referenznummern wird dieser Präfix vorangestellt.
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>Zahlungsziel bei Vorkasse</td>
                        <td><input type="text" name="cpSetting[CrefoPayPrepaidPeriod]" value="[{$cpSetting.CrefoPayPrepaidPeriod}]"></td>
                        <td>Tage</td>
                    </tr>
                    <tr>
                        <td>Zahlungsziel bei Rechnung</td>
                        <td><input type="text" name="cpSetting[CrefoPayBillPeriod]" value="[{$cpSetting.CrefoPayBillPeriod}]"></td>
                        <td>Tage</td>
                    </tr>
                    <tr>
                        <td>Warenkorb-Gültigkeit</td>
                        <td><input type="text" name="cpSetting[CrefoPayBasketVal]" value="[{$cpSetting.CrefoPayBasketVal}]"></td>
                        <td>
                            <select name="cpSetting[CrefoPayBasketValUnit]">
                                <option value="h" [{if $cpSetting.CrefoPayBasketValUnit eq 'h'}]selected[{/if}]>Stunden</option>
                                <option value="m" [{if $cpSetting.CrefoPayBasketValUnit eq 'm'}]selected[{/if}]>Minuten</option>
                                <option value="d" [{if $cpSetting.CrefoPayBasketValUnit eq 'd'}]selected[{/if}]>Tage</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><b>Kreditkarten-Logos:</b></td>
            <td style="vertical-align: top;">
                <table>
                    <tr>
                        <td style="vertical-align: top;"><input type="checkbox" name="cpSetting[CrefoPayCvvLogo]" [{if $cpSetting.CrefoPayCvvLogo}]checked[{/if}] value="1"></td>
                        <td style="vertical-align: top;">CVV-Hilfe<br>Diese Option blendet ein Bild der Rückseite einer
                            Kreditkarte ein und zeigt, wo die CVV zu finden ist.
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><input type="checkbox" name="cpSetting[CrefoPayMcLogo]" [{if $cpSetting.CrefoPayMcLogo}]checked[{/if}] value="1">
                        </td>
                        <td style="vertical-align: top;">MasterCard-Logo<br>Blendet das MasterCard-Logo bei der Auswahl
                            der Kreditkarten-Zahlart ein, um zu visualisieren, dass ihr Shop MasterCard unterstützt
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><input type="checkbox" name="cpSetting[CrefoPayVisaLogo]" [{if $cpSetting.CrefoPayVisaLogo}]checked[{/if}] value="1"></td>
                        <td style="vertical-align: top;">VISA-Logo<br>Blendet das VISA-Logo bei der Auswahl der
                            Kreditkarten-Zahlart ein, um zu visualisieren, dass ihr Shop VISA-Kreditkarten unterstützt
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td style="vertical-align: top;"><b>Weitere Einstellungen:</b></td>
            <td style="vertical-align: top;">
                <table>
                    <tr>
                        <td style="vertical-align: top;"><input type="checkbox" name="cpSetting[CrefoPayTransmitBasket]" [{if $cpSetting.CrefoPayTransmitBasket}]checked[{/if}] value="1"></td>
                        <td style="vertical-align: top;">Warenkorb-Positionen übermitteln<br>Falls nicht angehakt wird nur eine Position mit der Gesamtsumme übermittelt. Falls angehakt werden die Einzelpositionen und (sofern nötig) Positionen für Gutscheine/Rabatte, Versandkosten, etc. Dabei können jedoch potentiell gegenüber dem Oxid-Warenkorb Rundungsabweichungen entstehen, die das Modul versucht durch Anpassung der letzten Position auszugleichen.
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><input type="checkbox" name="cpSetting[CrefoPayAllowOtherPayments]" [{if !$cpSetting.CrefoPayAllowOtherPayments}]checked[{/if}] value="0"></td>
                        <td style="vertical-align: top;">Nur CrefoPay-Bezahlarten anzeigen<br>Blendet alle anderen im
                            Shop aktivierten Zahlarten im Checkout aus
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <input type="hidden" name="cpSetting[CrefoPayAllowOtherUserInstruments]" value="0">
                            <input type="checkbox" name="cpSetting[CrefoPayAllowOtherUserInstruments]" [{if $cpSetting.CrefoPayAllowOtherUserInstruments}]checked[{/if}] value="1">
                        </td>
                        <td style="vertical-align: top;">Bei Crefopay für den User gespeicherte Zahlungsmittel anzeigen</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>Bonitätsscore in Oxid-Kundenkonto übernehmen: </td>
                        <td>
                            <select name="cpSetting[CrefoPayApplyRiskData]">
                                <option value="0" [{if $cpSetting.CrefoPayApplyRiskData eq '0'}]selected[{/if}]>Nein</option>
                                <option value="1" [{if $cpSetting.CrefoPayApplyRiskData eq '1'}]selected[{/if}]>Ja</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>Log-Level</td>
                        <td>
                            <select name="cpSetting[CrefoPayLogLevel]">
                                <option value="2" [{if $cpSetting.CrefoPayLogLevel eq 2}]selected[{/if}]>Nur Fehler</option>
                                <option value="1" [{if $cpSetting.CrefoPayLogLevel eq 1}]selected[{/if}]>Fehler und Warnungen</option>
                                <option value="0" [{if $cpSetting.CrefoPayLogLevel eq 0}]selected[{/if}]>Fehler, Warnungen und Debug-Ausgaben loggen</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>[{oxmultilang ident="CREFOPAY_DELETE_LOGFILES_OLDERTHAN" suffix="COLON"}]</td>
                        <td>
                        <td><input type="text" name="cpSetting[CrefoPayDeleteLogFilesAfterDays]" value="[{$cpSetting.CrefoPayDeleteLogFilesAfterDays}]"></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>[{oxmultilang ident="CREFOPAY_DELETE_EXPIREDTRANSACTIONS_OLDERTHAN" suffix="COLON"}]</td>
                        <td>
                        <td><input type="text" name="cpSetting[CrefoPayDeleteExpiredTransactionsAfterDays]" value="[{$cpSetting.CrefoPayDeleteExpiredTransactionsAfterDays}]"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right;">
                <button type="submit">[{oxmultilang ident="GENERAL_SAVE"}]</button>
            </td>
        </tr>
    </table>

</form>
</div>

<div class="actions">
    <ul>
        <li><a class="firstitem" href="https://www.crefopay.de/" target="_blank">https://www.crefopay.de/</a></li>
    </ul>
</div>

[{include file="bottomitem.tpl"}]