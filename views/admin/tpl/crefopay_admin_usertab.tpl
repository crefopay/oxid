[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
    [{else}]
    [{assign var="readonly" value=""}]
    [{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="[{$oViewConf->getActiveClassName()}]">
</form>

[{if $oCrefopayUser}]

    [{if $bShowViewsHint}]
        <div style="background-color: red;padding:10px;margin:10px;color:white;">[{oxmultilang ident="CREFOPAY_VIEWS_HINT"}]</div>
    [{/if}]

    <form action="[{$oViewConf->getSelfLink()}]" method="post">
        [{$oViewConf->getHiddenSid()}]
        <input type="hidden" name="oxid" value="[{$oxid}]">
        <input type="hidden" name="cl" value="[{$oViewConf->getActiveClassName()}]">
        <input type="hidden" name="fnc" value="save">
        <table>
            <tr>
                <td>[{oxmultilang ident="CREFOPAY_RISKCLASS" suffix="COLON"}]</td>
                <td>
                    <select name="crefopay_userriskclass" autocomplete="off">
                        <option value="1" [{if $oCrefopayUser->oxuser__crefopay_userriskclass->value eq 1}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_RISKCLASS_DEFAULT"}] (ID: 1)</option>
                        <option value="0" [{if $oCrefopayUser->oxuser__crefopay_userriskclass->value eq 0}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_RISKCLASS_TRUSTED"}] (ID: 0)</option>
                        <option value="2" [{if $oCrefopayUser->oxuser__crefopay_userriskclass->value eq 2}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_RISKCLASS_HIGHRISK"}] (ID: 2)</option>
                    </select>
                </td>
                <td><button type="submit" [{if !$oxid}]disabled[{/if}] [{$readonly}] class="edittext">[{oxmultilang ident="GENERAL_SAVE"}]</button></td>
            </tr>
        </table>
    </form>
    <div>[{oxmultilang ident="CREFOPAY_RISKCLASS_INFOTEXT"}]</div>

    <div>&nbsp;</div>
    <div style="font-weight: bold;">[{oxmultilang ident="CREFOPAY_SAVED_DATA"}]</div>
    <div style="padding:10px;display:inline-block;margin-left:10px;background-color: #f6f6f6">[{$crefopayData}]</div>

[{/if}]

[{include file="bottomnaviitem.tpl"}]

[{include file="bottomitem.tpl"}]
