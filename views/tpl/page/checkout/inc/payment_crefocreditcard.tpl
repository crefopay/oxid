[{assign var="dynvalue" value=$oView->getDynValue()}]
<dl>
    <dt>
        <input class="js-crefopay-paymentmethod" id="payment_[{$sPaymentID}]" type="radio" name="paymentid" value="[{$sPaymentID}]" [{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]checked[{/if}]>
        <label for="payment_[{$sPaymentID}]"><b>[{$paymentmethod->oxpayments__oxdesc->value}]
        [{if $paymentmethod->getPrice()}]
            [{assign var="oPaymentPrice" value=$paymentmethod->getPrice() }]
            [{if $oViewConf->isFunctionalityEnabled('blShowVATForPayCharge') }]
                [{if $oPaymentPrice->getNettoPrice() != 0 }]
                    ( [{oxprice price=$oPaymentPrice->getNettoPrice() currency=$currency}]
                    [{if $oPaymentPrice->getVatValue() != 0}]
                        [{oxmultilang ident="PLUS_VAT" }] [{oxprice price=$oPaymentPrice->getVatValue() currency=$currency }]
                    [{/if}])
                [{/if}]
            [{else}]
                [{if $oPaymentPrice->getBruttoPrice() != 0 }]
                    ([{oxprice price=$oPaymentPrice->getBruttoPrice() currency=$currency}])
                [{/if}]
            [{/if}]
        [{/if}]
        </b></label>
    </dt>
    <dd class="[{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]activePayment[{/if}]">
        <div class="cp-payment-logos">
            <div class="cp-ccissuer-logos">
            [{if ($paymentmethod->isCrefopayCreditCardLogoEnabled('mc'))}]
                [{if ($sPaymentID == 'cpcredit3d')}]
                    <img class="img-responsive img-thumbnail" src="[{$oViewConf->getCrefopayPaymentLogo('CC3D_MC.png')}]" alt="master_card_3ds_logo" />
                [{else}]
                    <img class="img-responsive img-thumbnail" src="[{$oViewConf->getCrefopayPaymentLogo('CC_MC.png')}]" alt="master_card_logo" />
                [{/if}]
            [{/if}]
            [{if ($paymentmethod->isCrefopayCreditCardLogoEnabled('visa'))}]
                [{if ($sPaymentID == 'cpcredit3d')}]
                    <img class="img-responsive img-thumbnail" src="[{$oViewConf->getCrefopayPaymentLogo('CC3D_VISA.png')}]" alt="visa_card_3ds_logo" />
                [{else}]
                    <img class="img-responsive img-thumbnail" src="[{$oViewConf->getCrefopayPaymentLogo('CC_VISA.png')}]" alt="visa_logo" />
                [{/if}]
            [{/if}]
            </div>
        [{if ($paymentmethod->isCrefopayCreditCardLogoEnabled('cvv'))}]
            <br>
            <img class="img-responsive img-thumbnail" src="[{$oViewConf->getCrefopayPaymentLogo('CVV.png')}]" alt="cvv_logo" />
        [{/if}]
        </div>
        <input style="display: none;" data-crefopay="paymentMethod" id="cppayment_[{$sPaymentID}]" type="radio" name="cppaymentid" value="[{$paymentmethod->getCrefopayPaymentMethodId()}]">
        <div style="margin-left:20px;">
            <div class="crefopay-payment-group js-crefopay-payment-group">
                [{if $oView->crefopayShowUserInstruments()}]
                    <div style="display:inline-block;vertical-align: top;padding-right: 5px;"><input type="radio" class="js-crefopay-paymentinstrumentid" name="paymentInstrument-[{$paymentmethod->getCrefopayPaymentMethodId()}]" value=""></div>
                [{else}]
                    <div style="display:none;"><input type="radio" class="js-crefopay-paymentinstrumentid" name="paymentInstrument-[{$paymentmethod->getCrefopayPaymentMethodId()}]" value=""></div>
                [{/if}]
                <div style="display:inline-block;vertical-align: top;width:40%;">
                    [{if $oView->crefopayShowUserInstruments()}]
                        <div><b>[{oxmultilang ident="CREFOPAY_CC_NEW" suffix="COLON"}]</b></div>
                    [{/if}]
                    <label data-crefopay-field="paymentInstrument.number" class="req crefopay-label" for="ccnumber">[{oxmultilang ident="NUMBER" suffix="COLON" }] <span class="crefopay-additional-error"></span></label>
                    <div id="ccnumber" class="cp-placeholder" data-crefopay-placeholder="paymentInstrument.number" required="required"></div>

                    <label data-crefopay-field="paymentInstrument.accountHolder" class="req control-label crefopay-label" for="ccholder">[{oxmultilang ident="BANK_ACCOUNT_HOLDER" suffix="COLON" }] <span class="crefopay-additional-error"></span></label>
                    <div id="ccholder" class="cp-placeholder" data-crefopay-placeholder="paymentInstrument.accountHolder" required="required"></div>

                    <label data-crefopay-field="paymentInstrument.validity" class="req control-label crefopay-label" for="ccvalidity">[{oxmultilang ident="VALID_UNTIL" suffix="COLON" }] <span class="crefopay-additional-error"></span></label>
                    <div id="ccvalidity" class="cp-placeholder" data-crefopay-placeholder="paymentInstrument.validity" required="required"></div>

                    <label data-crefopay-field="paymentInstrument.cvv" class="req control-label crefopay-label" for="cccvv">[{oxmultilang ident="CARD_SECURITY_CODE" suffix="COLON"}] <span class="crefopay-additional-error"></span></label>
                    <div id="cccvv" class="cp-placeholder cvv" data-crefopay-placeholder="paymentInstrument.cvv" required="required"></div>
                    <span class="help-block">[{oxmultilang ident="CARD_SECURITY_CODE_DESCRIPTION"}]</span>
                </div>
            </div>

            [{if $oView->crefopayShowUserInstruments()}]
                [{assign var="aUserPaymentInstruments" value=$oCrefopayTransaction->getUserPaymentInstrumentsForPayment($paymentmethod->getCrefopayPaymentMethodId())}]
                [{if count($aUserPaymentInstruments) > 0}]
                <div style="margin: 20px 0 10px;"><b>[{oxmultilang ident="CREFOPAY_SELECT_SAVED_PAYMENTINSTRUMENT"}]</b></div>

                <div style="display:flex; flex-flow:row wrap;">
                    [{foreach from=$aUserPaymentInstruments item="oUserPaymentInstrument"}]
                        [{assign var="ccValidity" value=$oUserPaymentInstrument->getValidity()}]
                        <div class="crefopay-payment-group js-crefopay-payment-group js-crefopay-saved-payment" style="background: white; padding: 20px; margin: 10px 20px 10px 0; display: flex; flex: 1 1 auto;">
                            <div style="display: flex; padding-right: 20px; align-items: center;">
                                <input type="radio" class="js-crefopay-paymentinstrumentid" name="paymentInstrument-[{$paymentmethod->getCrefopayPaymentMethodId()}]" value="[{$oUserPaymentInstrument->getPaymentInstrumentID()}]">
                            </div>

                            <div style="">
                                <table>
                                    <tr>
                                        <td>[{oxmultilang ident="CREFOPAY_CC_HOLDER" suffix="COLON"}]</td>
                                        <td>&nbsp;</td>
                                        <td>[{$oUserPaymentInstrument->getAccountHolder()|escape}]</td>
                                    </tr>
                                    <tr>
                                        <td>[{oxmultilang ident="CREFOPAY_CC_NUMBER" suffix="COLON"}]</td>
                                        <td>&nbsp;</td>
                                        <td>[{$oUserPaymentInstrument->getNumber()|escape}]</td>
                                    </tr>
                                    <tr>
                                        <td>[{oxmultilang ident="CREFOPAY_CC_ISSUER" suffix="COLON"}] </td>
                                        <td>&nbsp;</td>
                                        <td>[{$oView->getCrefopayCreditCardTypeTitle($oUserPaymentInstrument->getIssuer())}]</td>
                                    </tr>
                                    <tr>
                                        <td>[{oxmultilang ident="CREFOPAY_CC_VALIDITY" suffix="COLON"}] </td>
                                        <td>&nbsp;</td>
                                        <td>[{$ccValidity->format('m/Y')}]</td>
                                    </tr>
                                </table>


                                <div>
                                    <label data-crefopay-field="paymentInstrument.cvv" class="req control-label crefopay-label" for="cccvv">[{oxmultilang ident="CARD_SECURITY_CODE" suffix="COLON"}] 
                                        <span class="crefopay-additional-error"></span>
                                    </label>
                                    <div class="crefopay-additional-cvv cp-placeholder" data-crefopay-placeholder="paymentInstrument.cvv" style="border: 1px solid #d2d2d2; padding-left: 5px;"></div>
                                </div>

                                <div><a href="#" class="js-crefopay-remove-userpaymentinstrument" data-crefopay-paymentinstrumentid="[{$oUserPaymentInstrument->getPaymentInstrumentID()|escape}]">[{oxmultilang ident="DD_DELETE"}]</a></div>
                            </div>
                        </div>
                    [{/foreach}]
                </div>
                [{/if}]

            [{/if}]
        </div>

        [{block name="checkout_payment_longdesc"}]
            [{if $paymentmethod->oxpayments__oxlongdesc->value}]
                <div class="desc">
                    [{$paymentmethod->oxpayments__oxlongdesc->getRawValue()}]
                </div>
            [{/if}]
        [{/block}]
    </dd>
</dl>