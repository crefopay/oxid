[{assign var="dynvalue" value=$oView->getDynValue()}]
[{assign var="iPayError" value=$oView->getPaymentError() }]

<dl>
    <dt>
        <input class="js-crefopay-paymentmethod" id="payment_[{$sPaymentID}]" type="radio" name="paymentid" value="[{$sPaymentID}]" [{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]checked[{/if}]>
        <label for="payment_[{$sPaymentID}]"><b>[{$paymentmethod->oxpayments__oxdesc->value}]
        [{if $paymentmethod->getPrice()}]
            [{assign var="oPaymentPrice" value=$paymentmethod->getPrice() }]
            [{if $oViewConf->isFunctionalityEnabled('blShowVATForPayCharge') }]
                [{if $oPaymentPrice->getNettoPrice() != 0 }]
                    ( [{oxprice price=$oPaymentPrice->getNettoPrice() currency=$currency}]
                    [{if $oPaymentPrice->getVatValue() != 0}]
                        [{oxmultilang ident="PLUS_VAT" }] [{oxprice price=$oPaymentPrice->getVatValue() currency=$currency}]
                    [{/if}])
                [{/if}]
            [{else}]
                [{if $oPaymentPrice->getBruttoPrice() != 0 }]
                    ([{oxprice price=$oPaymentPrice->getBruttoPrice() currency=$currency}])
                [{/if}]
            [{/if}]
        [{/if}]
        </b></label>
    </dt>
    <dd class="[{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]activePayment[{/if}]">
        <input style="display: none;" data-crefopay="paymentMethod" id="cppayment_[{$sPaymentID}]" type="radio" name="cppaymentid" value="[{$paymentmethod->getCrefopayPaymentMethodId()}]">

        <div style="margin-left:20px;">
            <div class="crefopay-payment-group js-crefopay-payment-group">
                [{if $oView->crefopayShowUserInstruments()}]
                    <div style="display:inline-block;vertical-align: top;padding-right: 5px;"><input type="radio" class="js-crefopay-paymentinstrumentid" name="paymentInstrument-[{$paymentmethod->getCrefopayPaymentMethodId()}]" value=""></div>
                [{else}]
                    <div style="display:none;"><input type="radio" class="js-crefopay-paymentinstrumentid" name="paymentInstrument-[{$paymentmethod->getCrefopayPaymentMethodId()}]" value=""></div>
                [{/if}]

                <div style="display:inline-block;vertical-align: top;width:40%;">
                    [{if $oView->crefopayShowUserInstruments()}]
                        <div><b>[{oxmultilang ident="CREFOPAY_NEW_BANKACCOUNT" suffix="COLON"}]</b></div>
                    [{/if}]

                    <label data-crefopay-field="paymentInstrument.bankAccountHolder" class="req control-label crefopay-label" for="bankAccountHolder">[{oxmultilang ident="BANK_ACCOUNT_HOLDER" suffix="COLON"}] <span class="crefopay-additional-error"></span></label>
                    <input id="bankAccountHolder" type="text" class="cp-placeholder form-control js-oxValidate js-oxValidate_notEmpty" data-crefopay="paymentInstrument.bankAccountHolder" required="required">

                    <label data-crefopay-field="paymentInstrument.iban" class="req control-label crefopay-label" for="iban">[{oxmultilang ident="IBAN" suffix="COLON"}]  <span class="crefopay-additional-error"></span></label>
                    <input id="iban" type="text" class="cp-placeholder form-control js-oxValidate js-oxValidate_notEmpty" data-crefopay="paymentInstrument.iban" required="required">

                    <!--
                    <label data-crefopay-field="paymentInstrument.bic" class="req control-label crefopay-label" for="bic">[{oxmultilang ident="BIC" suffix="COLON"}]  <span class="crefopay-additional-error"></span></label>
                    <input id="bic" type="text" class="cp-placeholder form-control js-oxValidate js-oxValidate_notEmpty" data-crefopay="paymentInstrument.bic">
                    -->
                </div>
            </div>

            [{if $oView->crefopayShowUserInstruments()}]
                [{assign var="aUserPaymentInstruments" value=$oCrefopayTransaction->getUserPaymentInstrumentsForPayment($paymentmethod->getCrefopayPaymentMethodId())}]
                [{if count($aUserPaymentInstruments) > 0}]
                    <div style="margin: 20px 0 10px;"><b>[{oxmultilang ident="CREFOPAY_SELECT_SAVED_PAYMENTINSTRUMENT"}]</b></div>

                    <div style="display:flex; flex-flow:row wrap;">
                        [{foreach from=$aUserPaymentInstruments item="oUserPaymentInstrument"}]
                            <div class="crefopay-payment-group js-crefopay-payment-group js-crefopay-saved-payment" style="background: white; padding: 20px; margin: 10px 20px 10px 0; display: flex; flex: 1 1 auto;">
                                <div style="display: flex; padding-right: 20px; align-items: center;">
                                <input type="radio" class="js-crefopay-paymentinstrumentid" name="paymentInstrument-[{$paymentmethod->getCrefopayPaymentMethodId()}]" value="[{$oUserPaymentInstrument->getPaymentInstrumentID()}]"></div>

                                <div style="">
                                    <table>
                                        <tr>
                                            <td>[{oxmultilang ident="BANK_ACCOUNT_HOLDER" suffix="COLON"}]</td>
                                            <td>&nbsp;</td>
                                            <td>[{$oUserPaymentInstrument->getAccountHolder()|escape}]</td>
                                        </tr>
                                        <tr>
                                            <td>[{oxmultilang ident="IBAN" suffix="COLON"}] </td>
                                            <td>&nbsp;</td>
                                            <td>[{$oUserPaymentInstrument->getIban()|escape}]</td>
                                        </tr>
                                    </table>
                                    <div><a href="#" class="js-crefopay-remove-userpaymentinstrument" data-crefopay-paymentinstrumentid="[{$oUserPaymentInstrument->getPaymentInstrumentID()|escape}]">[{oxmultilang ident="DD_DELETE"}]</a></div>
                                </div>
                            </div>
                        [{/foreach}]
                    </div>
                [{/if}]
            [{/if}]
        </div>

        [{include file="crefopay_salutation.tpl"}]

        [{block name="checkout_payment_longdesc"}]
            [{if $paymentmethod->oxpayments__oxlongdesc->value}]
                <div class="desc">
                    [{$paymentmethod->oxpayments__oxlongdesc->getRawValue()}]
                </div>
            [{/if}]
        [{/block}]
    </dd>
</dl>