[{if $oView->crefopayIsBirthdateMissing() && $oView->crefopayIsBirthdateQuestionEnabled()}]
    <div class="form-group col-xs-12 col-lg-3">
        <label class="req control-label" for="crefoPayDOBBILL[[{$sPaymentID}]]">[{oxmultilang ident="CREFOPAY_DATE_OF_BIRTH"}]: </label>
        <input name="crefoPayDOB[[{$sPaymentID}]]"
               type="date"
               id="crefoPayDOBBILL"
               placeholder="YYYY-MM-DD"
               {literal}pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"{/literal}
               class="form-control js-oxValidate js-oxValidate_notEmpty"
               required="required" />
    </div>
    <div class="clearfix"></div>
[{/if}]

[{if $oView->crefopayIsSalutationMissing()}]
    [{assign var="cpPreselectedSalutation" value=$oView->crefopayGetPreselectedSalutation()}]
    <div class="form-group col-xs-12 col-lg-3">
        <label class="req control-label" for="crefoPaySalutation[[{$sPaymentID}]]">[{oxmultilang ident="CREFOPAY_SALUTATION"}]: </label>
        <select class="js-crefopaysalutation form-control" data-paymentid="[{$sPaymentID}]" name="crefoPaySalutation[[{$sPaymentID}]]">
            <option value="" disabled [{if $cpPreselectedSalutation eq ''}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_SALUTATION_SELECT"}]</option>
            <option value="MRS" [{if $cpPreselectedSalutation eq 'MRS'}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_SALUTATION_MRS"}]</option>
            <option value="MR" [{if $cpPreselectedSalutation eq 'MR'}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_SALUTATION_MR"}]</option>
            <option value="DIV" [{if $cpPreselectedSalutation eq 'DIV'}]selected[{/if}]>[{oxmultilang ident="CREFOPAY_SALUTATION_DIV"}]</option>
        </select>
    </div>
    <div class="clearfix"></div>
[{/if}]