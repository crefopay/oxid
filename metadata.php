<?php

const PAYMENT_TEMPLATE = 'page/checkout/payment.tpl';

$sMetadataVersion = '2.1';

$aModule = array(
    'id' => 'cppayments',
    'title' => array(
        'de' => 'CrefoPay - Bezahlarten',
        'en' => "CrefoPay - Paymentmethods"
    ),
    'description' => array(
        'de' => 'CrefoPay - Bezahlarten',
        'en' => "CrefoPay - Paymentmethods"
    ),
    'thumbnail' => 'out/src/img/logo.png',
    'version' => '2.4.3',
    'author' => 'CrefoPay',
    'url' => 'https://www.crefopay.de',
    'email' => 'info@crefopay.de',
    'extend' => array(
        // Controller
        \OxidEsales\Eshop\Application\Controller\PaymentController::class =>
            \Crefopay\Payments\Extend\Controller\CrefopayPaymentController::class,
        \OxidEsales\Eshop\Application\Controller\ThankYouController::class =>
            \Crefopay\Payments\Extend\Controller\CrefopayThankYouController::class,
        \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration::class =>
            \Crefopay\Payments\Extend\Controller\Admin\CrefopayModulConfiguration::class,
        \OxidEsales\Eshop\Application\Controller\OrderController::class =>
            \Crefopay\Payments\Extend\Controller\CrefopayOrderController::class,

        // Model
        \OxidEsales\Eshop\Application\Model\PaymentGateway::class =>
            \Crefopay\Payments\Extend\Model\CrefopayPaymentGateway::class,
        \OxidEsales\Eshop\Application\Model\Order::class =>
            \Crefopay\Payments\Extend\Model\CrefopayOrder::class,
        \OxidEsales\Eshop\Application\Model\Payment::class =>
            \Crefopay\Payments\Extend\Model\CrefopayPayment::class,
        \OxidEsales\Eshop\Application\Model\PaymentList::class =>
            \Crefopay\Payments\Extend\Model\CrefopayPaymentList::class,
        \OxidEsales\Eshop\Application\Model\User::class =>
            \Crefopay\Payments\Extend\Model\CrefopayUser::class,

        // Core
        \OxidEsales\Eshop\Core\ViewConfig::class => \Crefopay\Payments\Extend\Core\CrefopayViewConfig::class,
        \OxidEsales\Eshop\Core\Session::class => \Crefopay\Payments\Extend\Core\CrefopaySession::class
    ),
    'controllers' => array(
        'crefopay_internal' => \Crefopay\Payments\Controller\CrefopayNotificationController::class,

        'crefopay_admin_settings' => \Crefopay\Payments\Controller\Admin\CrefopaySettingsController::class,
        'crefopay_admin_ordertab' => \Crefopay\Payments\Controller\Admin\CrefopayOrderTabController::class,
        'crefopay_admin_usertab'  => \Crefopay\Payments\Controller\Admin\CrefopayUserTabController::class,

        'crefopay_admin_transactions_frame' =>
            \Crefopay\Payments\Controller\Admin\CrefopayTransactionsFrameController::class,
        'crefopay_admin_transactions_list' =>
            \Crefopay\Payments\Controller\Admin\CrefopayTransactionsListController::class,
        'crefopay_admin_transactions_main' =>
            \Crefopay\Payments\Controller\Admin\CrefopayTransactionsMainController::class
    ),
    'events' => array(
        'onActivate' => '\Crefopay\Payments\Core\CrefopayEvents::onActivate',
        'onDeactivate' => '\Crefopay\Payments\Core\CrefopayEvents::onDeactivate'
    ),
    'blocks' => array(
        array(
            'template' => 'module_config.tpl',
            'block' => 'admin_module_config_form',
            'file' => '/views/admin/blocks/crefopay_admin_module_config_form.tpl'
        ),
        array(
            'template' => PAYMENT_TEMPLATE,
            'block' => 'change_payment',
            'file' => '/views/blocks/crefopay_change_payment.tpl'
        ),
        array(
            'template' => PAYMENT_TEMPLATE,
            'block' => 'select_payment',
            'file' => '/views/blocks/crefopay_select_payment.tpl'
        ),
        array(
            'template' => PAYMENT_TEMPLATE,
            'block' => 'checkout_payment_nextstep',
            'file' => '/views/blocks/crefopay_checkout_payment_nextstep.tpl'
        ),
        array(
            'template' => 'page/checkout/thankyou.tpl',
            'block' => 'checkout_thankyou_info',
            'file' => '/views/blocks/crefopay_checkout_thankyou_info.tpl'
        ),
        array(
            'template'  => 'email/html/order_cust.tpl',
            'block'     => 'email_html_order_cust_paymentinfo_top',
            'file'      => '/views/blocks/crefopay_email_html_order_cust_paymentinfo_top.tpl'
        ),
        array(
            'template'  => 'email/plain/order_cust.tpl',
            'block'     => 'email_plain_order_cust_paymentinfo',
            'file'      => '/views/blocks/crefopay_email_plain_order_cust_paymentinfo.tpl'
        ),
        array(
            'template'  => 'page/checkout/order.tpl',
            'block'     => 'checkout_order_btn_confirm_bottom',
            'file'      => '/views/blocks/crefopay_checkout_order_btn_confirm_bottom.tpl'
        )
    ),
    // available Types: str | bool | arr | aarr | select | password
    'settings' => array(
        array(
            'group' => '',
            'name' => 'CrefoPayMerchantId',
            'type' => 'str',
            'value' => ''
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayStoreId',
            'type' => 'str',
            'value' => ''
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayPrivateKey',
            'type' => 'password',
            'value' => ''
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayShopPublicKey',
            'type' => 'password',
            'value' => ''
        ),
        array(
            'group' => '',
            'name' => 'CrefoPaySystemMode',
            'type' => 'select',
            'value' => '0',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayDefaultLang',
            'type' => 'select',
            'value' => 'DE',
            'constraints' => 'DE|EN|ES|FR|IT|NL'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayAllowOtherPayments',
            'type' => 'select',
            'value' => '1',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayTransmitBasket',
            'type' => 'select',
            'value' => '0',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayAllowOtherUserInstruments',
            'type' => 'select',
            'value' => '1',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayLogLevel',
            'type' => 'select',
            'value' => '1',
            'constraints' => '0|1|2'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayPreAssignOrderNr',
            'type' => 'bool',
            'value' => false
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayAutoCapture',
            'type' => 'bool',
            'value' => false
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayOrderPrefix',
            'type' => 'str',
            'value' => 'CP_'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayB2BEnabled',
            'type' => 'select',
            'value' => '0',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayPrepaidPeriod',
            'type' => 'str',
            'value' => '14'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayBillPeriod',
            'type' => 'str',
            'value' => '30'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayBasketVal',
            'type' => 'str',
            'value' => ''
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayBasketValUnit',
            'type' => 'select',
            'value' => 'h',
            'constraints' => 'h|m|d'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayCvvLogo',
            'type' => 'select',
            'value' => '1',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayMcLogo',
            'type' => 'select',
            'value' => '1',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayVisaLogo',
            'type' => 'select',
            'value' => '1',
            'constraints' => '0|1'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayDeleteLogFilesAfterDays',
            'type' => 'str',
            'value' => '14'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayDeleteExpiredTransactionsAfterDays',
            'type' => 'str',
            'value' => '7'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayUseModuleJQuery',
            'type' => 'bool',
            'value' => false
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayDOBUsage',
            'type' => 'select',
            'value' => 0,
            'constraints' => '0|1|2'
        ),
        array(
            'group' => '',
            'name' => 'CrefoPayApplyRiskData',
            'type' => 'select',
            'value' => 0,
            'constraints' => '0|1'
        )
    ),
    'templates' => array(
        'crefopay_salutation.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/crefopay_salutation.tpl',
        'payment_crefobill.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefobill.tpl',
        'payment_crefocashondelivery.tpl' =>
            'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefocashondelivery.tpl',
        'payment_crefocreditcard.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefocreditcard.tpl',
        'payment_crefodebit.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefodebit.tpl',
        'payment_crefopaypal.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefopaypal.tpl',
        'payment_crefoprepaid.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefoprepaid.tpl',
        'payment_crefosofort.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefosofort.tpl',
        'payment_crefoideal.tpl' => 'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefoideal.tpl',
        'payment_crefoinstallment.tpl' =>
            'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefoinstallment.tpl',
        'payment_crefogooglepay.tpl' =>            'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefogooglepay.tpl',
        'payment_crefoapplepay.tpl' =>            'crefopay/cppayments/views/tpl/page/checkout/inc/payment_crefoapplepay.tpl',
        'crefopay_admin_ordertab.tpl' => 'crefopay/cppayments/views/admin/tpl/crefopay_admin_ordertab.tpl',
        'crefopay_admin_usertab.tpl' => 'crefopay/cppayments/views/admin/tpl/crefopay_admin_usertab.tpl',
        'crefopay_admin_settings.tpl' => 'crefopay/cppayments/views/admin/tpl/crefopay_admin_settings.tpl',
        'crefopay_admin_transactions_frame.tpl' =>
            'crefopay/cppayments/views/admin/tpl/crefopay_admin_transactions_frame.tpl',
        'crefopay_admin_transactions_list.tpl' =>
            'crefopay/cppayments/views/admin/tpl/crefopay_admin_transactions_list.tpl',
        'crefopay_admin_transactions_main.tpl' =>
            'crefopay/cppayments/views/admin/tpl/crefopay_admin_transactions_main.tpl',
    )
);