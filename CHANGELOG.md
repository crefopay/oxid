# Changelog

Last changes in the crefopay oxid module

## [2.4.3] - 2024-12-19

- Added new payment method ApplePay

## [2.4.2] - 2024-05-28

- Fixed an invalid behavior for 3rd gender
- Added new payment method GooglePay&trade;

## [2.4.1] - 2023-12-06

- Added support for custom payment logos
- Added translation for "Entfernen" button in frontend templates

## [2.4.0] - 2023-09-07

- Added PHP 8.x requirement
- Added new payment method installment by Zinia
- Added separate function for getting customer email address
- Changed some files because of improving code style

## [2.3.5] - 2023-07-24

- Added option to import risk score into oxid user
- Change ID-field collations to match oxid defaults
- Added support for custom payment logos

## [2.3.4] - 2023-04-20

- The settings page always loaded the default values instead of configured values for some settings
- Added handling for crefopay payment methods with surcharge

## [2.3.3] - 2022-10-20

- Added setting for asking/saving date of birth
- Added setting for usage of theme's jQuery
- Allow order creation from backend if oxorderid for transaction was not found
- The user who created the crefopay order is now saved
- Added detection for securefields-library not loaded
- Added timeout for registerPayment-callback

## [2.3.2] - 2022-07-27

- Name and Surname are not longer required for B2B transactions

## [2.3.1] - 2022-07-06

- Setting of CrefoPay's merchantReference has been improved

## [2.3.0] - 2022-06-24

- Also serialize non-redirect orders before executing the order
- Added 'Various'-salutation
- Updated PHP ClientLibrary dependency
- Delay creation of orders triggered from MNS
- Skip asking for birthdate/salutation for b2b customers

## [2.2.1] - 2022-05-16

- addded iDeal as new CrefoPay payment gateway

## [2.2.0] - 2022-05-09

- Added option to transmit the basket items to crefopay instead of one global basket item for the basket
- Added additional logging
- Various small improvements
- Updated PHP ClientLibrary

## [2.1.6] - 2021-07-22

- Added php/oxid version restrictions to composer.json
- Settings were not saved to yaml
- Force initialization of oxorderdate (if not already set)

## [2.1.5] - 2021-07-08

- Added support for PayPal2 Workflow
- Admin option to create order from transaction is now available in more cases

## [2.1.4] - 2021-06-14

- 2.1.3 introduced a bug so that advancing the checkout with non-crefopay payment methods was impossible

## [2.1.3] - 2021-06-10

- Do not call setAdditionalInformation() for empty array
- Updated dependency-version for 'crefopay/php-clientlibrary' to v1.2.4
- Logfiles are now stored in subfolders per day
- Redirect '#orderStep'-click to '#paymentNextStepBottom' to prevent bypassing of validation steps
- Added generic frontend error message for the 'wrong domain config'-case (#4)
- Removing sess_challenge and regenerating sid when payment failed or was canceled (#11)
- Added option to pre-assign oxordernr (#11)
- Successful reserve()-calls are now emulated if the basket total is zero (#19)
- Error messages improvement (#20)
- Added translation for resultCode 2017 (#21)
- Filtering transactions by customer name caused an exception (#22)

## [2.1.2] - 2021-04-22

- Additional logging
- Do not re-initialize paymenttype settings if module was already activated before
- Added settings for removing old transactions / removing old log files
- Added support for stored user payment-instruments

## [2.1.1] - 2021-03-15

- Improved detection of invalid birthdates
- Add lockfile-usage to prevent parallel order creation for the same transaction

## [2.1.0] - 2021-03-04

- Removed fax number transmission to prevent validation issues
- Improved log and error handling for salutation mapping
- Removed BIC as required field for SEPA transactions

## [2.0.9] - 2021-02-26

- (This update requires module re-activation)
- Start with a fresh crefopay-transaction after reserve() has returned an error response
- Update birthdate and salutation directly in crefopay if set during checkout
- Various small enhancements

## [2.0.8] - 2021-02-18

- timestamp was saved incorrectly to the crefopay_mns table

## [2.0.7] - 2021-02-12

- (This update requires module re-activation)
- A users default risk-class can now be set via oxid-backend
- createTransaction(): Fixed wrong visibility function modifier
- In one object firstname and lastname were swapped

## [2.0.6] - 2021-01-19

- Wrong shipping-address could get transmitted to CrefoPay
- Improved api-response logging

## [2.0.5] - 2021-01-07

- Wrong url for securefields.js was used
- Enter in search-fields in oxid-backend at 'Crefopay Transactions' now working
- Added failsafe in case used transaction is seen in payment-selection

## [2.0.4] - 2021-01-06

- Multiple creditcard-fields could be available in the same page
  and the wrong one could be read in registerPayment
- Changed securefields endpoint-urls to new urls

## [2.0.3] - 2020-11-11

- (This update requires module re-activation)
- The creditcard-issuer is now saved to crefopay_transactions

## [2.0.2] - 2020-11-11

- Make finalizeCrefopayOrder() overridable
- Introduced CHANGELOG.md
