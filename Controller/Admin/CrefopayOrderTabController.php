<?php

namespace Crefopay\Payments\Controller\Admin;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Application\Model\Order;

class CrefopayOrderTabController extends AdminDetailsController
{
    protected $_sThisTemplate = 'crefopay_admin_ordertab.tpl';

    public function render()
    {
        CrefopayHelpers::getLogger()->setLoggingIdent('oxidadmin');

        $oxorderid = $this->getEditObjectId();

        $cpTransaction = null;
        $oOrder = oxNew(Order::class);

        if ($oxorderid && $oxorderid !== '-1') {
            $oOrder->load($oxorderid);

            /** @var CrefopayTransaction $cpTransaction */
            $cpTransaction = CrefopayTransaction::getTransactionByOxidOrderId($oxorderid);
            if ($cpTransaction) {
                $cpTransaction->refreshTransaction();
            }
        }

        $this->_aViewData['oOrder'] = $oOrder;
        $this->_aViewData['cpTransaction'] = $cpTransaction;

        return parent::render();
    }
}
