<?php

namespace Crefopay\Payments\Controller\Admin;

use Crefopay\Payments\Core\CrefopayHelpers;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Registry;
use CrefoPay\Library\Api\Exception\ApiError;
use CrefoPay\Library\Api\Exception\MacValidation;

class CrefopaySettingsController extends AdminDetailsController
{
    protected $_sThisTemplate = 'crefopay_admin_settings.tpl';

    public function render()
    {
        $oConfig = Registry::getConfig();

        $cpSetting = [
            'CrefoPaySystemMode' => CrefopayHelpers::getConfigParam('CrefoPaySystemMode'),
            'CrefoPayMerchantId' => CrefopayHelpers::getConfigParam('CrefoPayMerchantId'),
            'CrefoPayStoreId' => CrefopayHelpers::getConfigParam('CrefoPayStoreId'),
            'CrefoPayPrivateKey' => CrefopayHelpers::getConfigParam('CrefoPayPrivateKey'),
            'CrefoPayShopPublicKey' => CrefopayHelpers::getConfigParam('CrefoPayShopPublicKey'),
            'CrefoPayDefaultLang' => CrefopayHelpers::getConfigParam('CrefoPayDefaultLang'),
            'CrefoPayAllowOtherPayments' => CrefopayHelpers::getConfigParam('CrefoPayAllowOtherPayments'),
            'CrefoPayLogLevel' => CrefopayHelpers::getConfigParam('CrefoPayLogLevel'),
            'CrefoPayAutoCapture' => CrefopayHelpers::getConfigParam('CrefoPayAutoCapture'),
            'CrefoPayOrderPrefix' => CrefopayHelpers::getConfigParam('CrefoPayOrderPrefix'),
            'CrefoPayB2BEnabled' => CrefopayHelpers::getConfigParam('CrefoPayB2BEnabled'),
            'CrefoPayPrepaidPeriod' => (int)CrefopayHelpers::getConfigParam('CrefoPayPrepaidPeriod'),
            'CrefoPayBillPeriod' => (int)CrefopayHelpers::getConfigParam('CrefoPayBillPeriod'),
            'CrefoPayBasketVal' => (int)CrefopayHelpers::getConfigParam('CrefoPayBasketVal'),
            'CrefoPayBasketValUnit' => CrefopayHelpers::getConfigParam('CrefoPayBasketValUnit'),
            'CrefoPayCvvLogo' => CrefopayHelpers::getConfigParam('CrefoPayCvvLogo'),
            'CrefoPayMcLogo' => CrefopayHelpers::getConfigParam('CrefoPayMcLogo'),
            'CrefoPayVisaLogo' => CrefopayHelpers::getConfigParam('CrefoPayVisaLogo'),
            'CrefoPayDeleteLogFilesAfterDays' => (int)CrefopayHelpers::getConfigParam('CrefoPayDeleteLogFilesAfterDays', 14),
            'CrefoPayDeleteExpiredTransactionsAfterDays' => (int)CrefopayHelpers::getConfigParam('CrefoPayDeleteExpiredTransactionsAfterDays', 7),
            'CrefoPayAllowOtherUserInstruments' => (int)CrefopayHelpers::getConfigParam('CrefoPayAllowOtherUserInstruments', 1),
            'CrefoPayPreAssignOrderNr' => (bool)CrefopayHelpers::getConfigParam('CrefoPayPreAssignOrderNr'),
            'CrefoPayTransmitBasket' => (bool)CrefopayHelpers::getConfigParam('CrefoPayTransmitBasket'),
            'CrefoPayDOBUsage' => (int)CrefopayHelpers::getConfigParam('CrefoPayDOBUsage'),
            'CrefoPayUseModuleJQuery' => (int)CrefopayHelpers::getConfigParam('CrefoPayUseModuleJQuery'),
            'CrefoPayApplyRiskData' => (int)CrefopayHelpers::getConfigParam('CrefoPayApplyRiskData')
        ];

        // these values should be >=0
        $cpSetting['CrefoPayPrepaidPeriod'] = max(0, $cpSetting['CrefoPayPrepaidPeriod']);
        $cpSetting['CrefoPayBillPeriod'] = max(0, $cpSetting['CrefoPayBillPeriod']);
        $cpSetting['CrefoPayDeleteExpiredTransactionsAfterDays'] = max(0, $cpSetting['CrefoPayDeleteExpiredTransactionsAfterDays']);

        // these values should be >0
        $cpSetting['CrefoPayBasketVal'] = max(1, $cpSetting['CrefoPayBasketVal']);
        $cpSetting['CrefoPayDeleteLogFilesAfterDays'] = max(1, $cpSetting['CrefoPayDeleteLogFilesAfterDays']);

        $this->_aViewData['cpSetting'] = $cpSetting;

        $sCronjobUrl = $oConfig->getCurrentShopUrl(false);
        $sCronjobUrl = Registry::getUtilsUrl()->appendUrl($sCronjobUrl, ['cl' => 'crefopay_internal', 'fnc' => 'capture'], true, true);

        $this->_aViewData['sCronjobUrl'] = $sCronjobUrl;

        $apiTestResult = '';
        $oLang = Registry::getLang();

        if ($cpSetting['CrefoPayPrivateKey']) {
            try {
                $getUserRequest = new \CrefoPay\Library\Request\GetUser(CrefopayHelpers::getAPIConfig());
                $getUserRequest->setUserID('oxid-ping@example.org');
                $getUserRequest->setReturnRiskData(false);

                $getUserApi = new \CrefoPay\Library\Api\GetUser(CrefopayHelpers::getAPIConfig(), $getUserRequest);
                $response = $getUserApi->sendRequest();

                $apiTestResult = $response->getErrorStatusMessage();
            } catch (MacValidation $macEx) {
                $apiTestResult = $oLang->translateString('CREFOPAY_INVALIDCREDENTIALS') . ': ' . $macEx->getMessage();
            } catch (ApiError $apiError) {
                if ($apiError->getCode() === 2015) {
                    // User does not exist - This is the expected response
                    $apiTestResult = 'OK';
                } else {
                    $apiTestResult = $apiError->getCode() . ' - ' . get_class($apiError) . ': ' . $apiError->getCode() . ' ' . $apiError->getMessage();
                }
            } catch (\Exception $ex) {
                $apiTestResult = get_class($ex) . ': ' . $ex->getMessage();
            }
        }

        $this->_aViewData['apiTestResult'] = $apiTestResult;

        return parent::render();
    }

    private function saveSettings($cpSettings)
    {
        $oConfig = Registry::getConfig();
        $sCfgModuleId = "module:" . CrefopayHelpers::getModuleId();

        if (is_array($cpSettings)) {
            // Use these values if not present in $cpSettings:
            $cpDefaultForMissingValues = [
                'CrefoPaySystemMode' => 0,
                'CrefoPayAllowOtherPayments' => 1,
                'CrefoPayAutoCapture' => 0,
                'CrefoPayB2BEnabled' => 0,
                'CrefoPayCvvLogo' => 0,
                'CrefoPayMcLogo' => 0,
                'CrefoPayVisaLogo' => 0,
                'CrefoPayPreAssignOrderNr' => 0,
                'CrefoPayTransmitBasket' => 0
            ];

            foreach ($cpDefaultForMissingValues as $cpVarName => $cpVarDefaultValue) {
                if (!isset($cpSettings[$cpVarName])) {
                    $cpSettings[$cpVarName] = $cpVarDefaultValue;
                }
            }

            $sMetadataFilename = $this->getViewConfig()->getModulePath(CrefopayHelpers::getModuleId(), '/metadata.php');

            $aModule = false;
            include($sMetadataFilename);
            if (is_array($aModule)) {
                $metaSettings = $aModule['settings'];
                foreach ($metaSettings as $metaSetting) {
                    $sVarName = $metaSetting['name'];
                    $sVarType = $metaSetting['type'];
                    if (isset($cpSettings[$sVarName])) {
                        $saveValue = $cpSettings[$sVarName];

                        if (($sVarType === 'str') || ($sVarType === 'password')) {
                            $saveValue = trim($saveValue);
                            $oConfig->saveShopConfVar('str', $sVarName, trim($saveValue), null, $sCfgModuleId);
                        } elseif ($sVarType === 'select') {
                            $oConfig->saveShopConfVar('select', $sVarName, trim($saveValue), null, $sCfgModuleId);
                        } elseif ($sVarType === 'bool') {
                            $sBool = trim((string)$saveValue);
                            $bBool = 0;
                            if (($sBool === '1') || ($sBool === 'true')) {
                                $bBool = 1;
                            }
                            $oConfig->saveShopConfVar('bool', $sVarName, $bBool, null, $sCfgModuleId);
                        } else {
                            die("Unhandled var-type: '" . $sVarType . "' for '" . $sVarName . "'");
                        }
                    }
                }

                CrefopayHelpers::updateModuleSettingsYAML();
            }
        }
    }

    public function save()
    {
        $cpSetting = Registry::getRequest()->getRequestParameter('cpSetting');
        $this->saveSettings($cpSetting);
    }
}
