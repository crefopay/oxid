<?php

namespace Crefopay\Payments\Controller\Admin;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Models\CrefopayTransaction;
use Crefopay\Payments\Models\CrefopayTransactionList;
use OxidEsales\Eshop\Application\Controller\Admin\AdminListController;
use OxidEsales\Eshop\Core\Registry;

class CrefopayTransactionsListController extends AdminListController
{
    protected $_sThisTemplate = 'crefopay_admin_transactions_list.tpl';

    /**
     * Name of chosen object class (default null).
     *
     * @var string
     */
    protected $_sListClass = CrefopayTransaction::class;

    protected $_sDefSortField = 'cpcreated';

    protected $_blDesc = true;

    /**
     * Type of list.
     *
     * @var string
     */
    protected $_sListType = CrefopayTransactionList::class;

    public function getCrefopayPaymentMethods()
    {
        return CrefopayHelpers::getPaymentMethodsFromJSON();
    }

    public function getPaymentMethodTitle($cpPaymentMethod)
    {
        $oLang = Registry::getLang();

        $checkLang = [];
        $checkLang[] = $oLang->getLanguageAbbr();
        // Fallback:
        $checkLang[] = 'en';
        $checkLang[] = 'de';

        foreach ($checkLang as $langIso2) {
            if (isset($cpPaymentMethod['oxdesc'][$langIso2])) {
                return $cpPaymentMethod['oxdesc'][$langIso2];
            }
        }

        return '';
    }

    public function getTransactionStates()
    {
        return CrefopayHelpers::getTransactionStates();
    }
}
