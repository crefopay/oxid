<?php

namespace Crefopay\Payments\Controller\Admin;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Core\CrefopayRiskClass;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\Field;
use OxidEsales\Eshop\Core\Registry;
use CrefoPay\Library\Request\Objects\AbstractObject;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Response\SuccessResponse;

class CrefopayUserTabController extends AdminDetailsController
{
    protected $_sThisTemplate = 'crefopay_admin_usertab.tpl';

    public function render()
    {
        $oxidUserId = $this->getEditObjectId();

        $crefopayData = '';

        $oCrefopayUser = null;
        $bShowViewsHint = false;

        if (($oxidUserId) && ($oxidUserId != '-1')) {
            $oUser = oxNew(User::class);
            if ($oUser->load($oxidUserId)) {
                $oCrefopayUser = $oUser;

                if (!($oCrefopayUser->oxuser__crefopay_userriskclass instanceof Field)) {
                    $bShowViewsHint = true;
                    // Fallback:
                    $oCrefopayUser->oxuser__crefopay_userriskclass = new Field(CrefopayRiskClass::RiskDefault);
                }

                $crefopayResponse = $this->getCrefopayUserInfo($oUser);
                if ($crefopayResponse instanceof SuccessResponse) {
                    /** @var Person $userData */
                    $allData = $crefopayResponse->getAllData();
                    foreach ($allData as $itemKey => $itemData) {
                        $itemValue = $crefopayResponse->getData($itemKey);
                        if ($itemValue instanceof AbstractObject) {
                            $itemValueArray = $itemValue->toArray();
                            if (count($itemValueArray) > 0) {
                                $crefopayData .= '<div style="font-weight: bold;">' . htmlentities($itemKey, ENT_QUOTES, 'UTF-8') . "</div>";
                                $crefopayData .= $this->formatCrefopayArray($itemValueArray, 1);
                                $crefopayData .= '<div>&nbsp;</div>';
                            }
                        }
                    }
                } else {
                    $crefopayData = $crefopayResponse->getErrorStatusMessage();
                }
            }
        }

        $this->_aViewData['bShowViewsHint'] = $bShowViewsHint;
        $this->_aViewData['oCrefopayUser'] = $oCrefopayUser;
        $this->_aViewData['crefopayData'] = $crefopayData;

        return parent::render();
    }

    public function save()
    {
        $oxidUserId = $this->getEditObjectId();

        if (($oxidUserId) && ($oxidUserId != '-1')) {
            $oUser = oxNew(User::class);
            if ($oUser->load($oxidUserId)) {
                $riskClass = (int) Registry::getRequest()->getRequestParameter('crefopay_userriskclass');
                $prevRiskClass = (int) $oUser->oxuser__crefopay_userriskclass->value;

                if ($prevRiskClass !== $riskClass) {
                    $oUser->oxuser__crefopay_userriskclass = new Field($riskClass);
                    $oUser->save();

                    // Add remark:
                    $remarkText = 'Crefopay-Risk-Class was changed from ' . $prevRiskClass . ' to ' . $riskClass . "\r\n";
                    $remarkText .= 'Changed by: ' . Registry::getSession()->getUser()->oxuser__oxusername->rawValue;

                    $oRemark = oxNew(\OxidEsales\Eshop\Application\Model\Remark::class);
                    $oRemark->oxremark__oxtext = new \OxidEsales\Eshop\Core\Field($remarkText);
                    $oRemark->oxremark__oxparentid = new \OxidEsales\Eshop\Core\Field($oxidUserId);
                    $oRemark->oxremark__oxshopid = new \OxidEsales\Eshop\Core\Field(Registry::getConfig()->getShopId());
                    $oRemark->oxremark__oxtype = new \OxidEsales\Eshop\Core\Field("r");
                    $oRemark->save();
                }
            }
        }
    }

    private function formatCrefopayArray($values, $lvl = 0)
    {
        $rv = '';
        foreach ($values as $key => $val) {
            $rv .= "<span style='white-space:pre; '>";
            $rv .= str_pad('', $lvl, ' ', STR_PAD_LEFT);
            $rv .= htmlentities($key, ENT_QUOTES, 'UTF-8');
            $rv .= ': ';
            $rv .= htmlentities($val, ENT_QUOTES, 'UTF-8');
            $rv .= "</span>";
            $rv .= "<br>\n";
        }

        return $rv;
    }

    public function getCrefopayUserInfo($oUser)
    {
        CrefopayHelpers::getLogger()->setLoggingIdent('oxidadmin');

        $request = new \CrefoPay\Library\Request\GetUser(CrefopayHelpers::getAPIConfig());
        $request->setReturnRiskData(true);
        $request->setUserID(CrefopayHelpers::getCrefopayUserIdFromOxidUser($oUser));

        return CrefopayHelpers::executeApiCall(\CrefoPay\Library\Api\GetUser::class, $request, true);
    }
}
