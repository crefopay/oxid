<?php

namespace Crefopay\Payments\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminController;

class CrefopayTransactionsFrameController extends AdminController
{
    protected $_sThisTemplate = 'crefopay_admin_transactions_frame.tpl';
}
