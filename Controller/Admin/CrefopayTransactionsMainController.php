<?php

namespace Crefopay\Payments\Controller\Admin;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Core\CrefopayOrderCreator;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Application\Model\Order;

class CrefopayTransactionsMainController extends AdminDetailsController
{
    protected $_sThisTemplate = 'crefopay_admin_transactions_main.tpl';

    public function render()
    {
        $this->_aViewData['cpTransaction'] = false;

        $oxid = $this->getEditObjectId();
        if ($oxid) {
            CrefopayHelpers::getLogger()->setLoggingIdent('oxidadmin');

            $cpTransaction = oxNew(CrefopayTransaction::class);
            if ($cpTransaction->load($oxid)) {
                $cpTransaction->refreshTransaction();

                $this->_aViewData['cpTransaction'] = $cpTransaction;
            }
        }

        return parent::render();
    }

    /**
     * @param $cpTransaction CrefopayTransaction
     */
    public function getDeserializedOrderFromTransaction($cpTransaction)
    {
        $aSerObjects = $cpTransaction->getSerializedObjects();
        if (isset($aSerObjects['order'])) {
            /** @var Order $oOrder */
            $oOrder = $aSerObjects['order'];

            return $oOrder;
        }

        return null;
    }

    public function createOrder()
    {
        $bSkipCreateOrder = false;

        $oxid = $this->getEditObjectId();
        if ($oxid) {
            $cpTransaction = oxNew(CrefopayTransaction::class);
            if ($cpTransaction->load($oxid)) {
                $oxorderid = $cpTransaction->getOxidOrderId();
                if ($oxorderid) {
                    // Load order to test if the order actually exists:
                    $oOrderTest = oxNew(Order::class);
                    if ($oOrderTest->load($oxorderid)) {
                        // Order is already saved
                        $bSkipCreateOrder = true;
                    }
                }

                if (!$bSkipCreateOrder) {
                    $logger = CrefopayHelpers::getLogger();

                    $logger->debug('Creating order from oxid-backend for cp-order-id: ' . $cpTransaction->getCrefopayOrderId());

                    try {
                        $oOrderCreator = oxNew(CrefopayOrderCreator::class);
                        $oOrderCreator->finalizeCrefopayOrder($cpTransaction, 'backend');
                    } catch (\Exception $ex) {
                        header("Content-Type: text/plain;charset=UTF-8");

                        $msg = "Failed creating order from oxid-backend for cp-order-id: '";
                        $msg .= $cpTransaction->getCrefopayOrderId() . "'\r\n";
                        $msg .= $ex->getMessage() . "\r\n";
                        $msg .= $ex->getTraceAsString();

                        $logger->error($msg);
                        echo $msg;
                        exit;
                    }
                }
            }
        }
    }

    public function getTransactionStates()
    {
        return CrefopayHelpers::getTransactionStates();
    }
}
