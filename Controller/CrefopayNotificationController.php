<?php

namespace Crefopay\Payments\Controller;

use Crefopay\Payments\Core\CrefopayHelpers;
use Crefopay\Payments\Core\CrefopayMnsProcessor;
use Crefopay\Payments\Core\CrefopayTransactionStatus;
use Crefopay\Payments\Models\CrefopayTransaction;
use OxidEsales\Eshop\Application\Controller\FrontendController;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use CrefoPay\Library\Mns\Handler;

class CrefopayNotificationController extends FrontendController
{
    public function render()
    {
        error_404_handler($_SERVER['REQUEST_URI']);
        exit;
    }

    /**
     * CrefoPay notification handler
     */
    public function notify()
    {
        CrefopayHelpers::getLogger()->setLoggingIdent('mns');
        CrefopayHelpers::getLogger()->debug('notify() called with:' . json_encode($_POST));
        try {
            /** @var CrefopayMnsProcessor $mnsProcessor */
            $mnsProcessor = oxNew(CrefopayMnsProcessor::class);
            $mnsHandler = new Handler(CrefopayHelpers::getAPIConfig(), $_POST, $mnsProcessor);
            $mnsHandler->run();
            http_response_code(200);
            CrefopayHelpers::getLogger()->debug('notify() finished');
            exit;
        } catch (\Exception $t) {
            // Log error but still return 200 to prevent the queue from stopping
            CrefopayHelpers::getLogger()->error($t);
            http_response_code(200);
            exit;
        }
    }

    /**
     * Removes expired transactions older than 30 days
     * Checks transaction-state of pending transactions
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseErrorException
     */
    private function cleanup()
    {
        $CrefoPayDeleteExpiredTransactionsAfterDays = (int)CrefopayHelpers::getConfigParam('CrefoPayDeleteExpiredTransactionsAfterDays', 7);
        $CrefoPayDeleteExpiredTransactionsAfterDays = max(0, $CrefoPayDeleteExpiredTransactionsAfterDays);

        $expireTimestamp = strtotime("-" . $CrefoPayDeleteExpiredTransactionsAfterDays . " days");

        $checkTransactionStates = [
            CrefopayTransactionStatus::TransactionStatusNEW,
            CrefopayTransactionStatus::MERCHANTPENDING,
            CrefopayTransactionStatus::INPROGRESS,
            CrefopayTransactionStatus::CIAPending,
            CrefopayTransactionStatus::ACKNOWLEDGEPENDING,
            CrefopayTransactionStatus::FRAUDPENDING
        ];

        $checkTransactionStates = DatabaseProvider::getDb()->quoteArray($checkTransactionStates);

        $sql = "SELECT OXID FROM crefopay_transactions WHERE ((CPTRANSACTIONSTATUS IS NULL) OR (CPTRANSACTIONSTATUS IN (" . implode(",", $checkTransactionStates) . "))) ORDER BY RAND() LIMIT 10";
        $rows = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->getAll($sql);
        foreach ($rows as $row) {
            CrefopayHelpers::getLogger()->debug('[cleanup] Checking transaction-status for: ' . $row['OXID']);

            $oTransaction = oxNew(CrefopayTransaction::class);
            if ($oTransaction->load($row['OXID'])) {
                $oldState = $oTransaction->getCrefopayTransactionStatus();
                $oTransaction->refreshTransaction();
                $newState = $oTransaction->getCrefopayTransactionStatus();
                if ($oldState !== $newState) {
                    CrefopayHelpers::getLogger()->debug('[cleanup] transaction-status changed from ' . $oldState . ' to ' . $newState . ' for: ' . $row['OXID']);
                }
            } else {
                // Set broken transactions to 'EXPIRED':
                CrefopayHelpers::getLogger()->debug('[cleanup] Changing transaction-status to EXPIRED for: ' . $row['OXID']);
                DatabaseProvider::getDb()->execute("UPDATE crefopay_transactions SET CPTRANSACTIONSTATUS='EXPIRED' WHERE OXID=?;", [$row['OXID']]);
            }
        }

        // Remove old expired transactions:
        $sql = "DELETE FROM crefopay_transactions WHERE CPTRANSACTIONSTATUS='EXPIRED' AND CPCREATED<=?";
        DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->execute($sql, [date("Y-m-d H:i:s", $expireTimestamp)]);

        $this->cleanupLogFiles();
    }

    private function cleanupLogFiles()
    {
        // Cleanup log files:
        {
            $CrefoPayDeleteLogFilesAfterDays = (int)CrefopayHelpers::getConfigParam('CrefoPayDeleteLogFilesAfterDays', 14);
            $CrefoPayDeleteLogFilesAfterDays = max(1, $CrefoPayDeleteLogFilesAfterDays);
            $minLogfileTimestamp = strtotime("-" . $CrefoPayDeleteLogFilesAfterDays . " days");

            CrefopayHelpers::getLogger()->removeLogsOlderThan($minLogfileTimestamp);
        }
    }

    public function capture()
    {
        $sLockfileName = Registry::getConfig()->getConfigParam('sCompileDir');
        $sLockfileName .= '/crefopay_capture.lock';

        $fp = fopen($sLockfileName, 'wb+');
        if (!flock($fp, LOCK_EX | LOCK_NB)) {
            echo "cronjob already running!";
            exit;
        }

        $logger = CrefopayHelpers::getLogger();
        $logger->setLoggingIdent('capture');

        try {
            $this->cleanup();

            // autofix null-values:
            DatabaseProvider::getDb()->execute("UPDATE crefopay_transactions SET CPISCAPTURED=0 WHERE CPISCAPTURED IS NULL");

            $minDate = strtotime("-30 days");

            $sql = "SELECT ct.OXID FROM crefopay_transactions ct, oxorder o WHERE ct.CPISCAPTURED=0 AND (ct.OXORDERID=o.OXID) AND DATE(o.OXSENDDATE)>=?";
            $rows = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->getAll($sql, [
                date("Y-m-d H:i:s", $minDate)
            ]);

            foreach ($rows as $row) {
                $cpTransaction = oxNew(CrefopayTransaction::class);
                if ($cpTransaction->load($row['OXID'])) {
                    CrefopayHelpers::getLogger()->debug("Trying to capture cp-order-id: " . $row['OXID']);
                    try {
                        $cpTransaction->capture();
                    } catch (\Exception $ex) {
                        CrefopayHelpers::getLogger()->error("Failed to capture cp-order-id: " . $row['OXID']);
                        CrefopayHelpers::getLogger()->error($ex);

                        // Mark with error state:
                        DatabaseProvider::getDb()->execute("UPDATE crefopay_transactions SET CPISCAPTURED=2 WHERE OXID=?", [$row['OXID']]);
                    }
                }
            }
        } catch (\Exception $e) {
            $logger->error($e);
            http_response_code(500);
        }

        $logger->setLoggingIdent('mns-process');
        try {
            $mnsProcessor = oxNew(CrefopayMnsProcessor::class);
            $mnsProcessor->processMNS(); // process notifications
            $mnsProcessor->checkForMissingOrders();
        } catch (\Exception $e) {
            $logger->error($e);
            http_response_code(500);
        }

        echo "-DONE-";

        fclose($fp);
        exit();
    }
}
