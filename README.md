# Quick Start Guide CrefoPay Plugin "Payments" für Oxid eShop  

## 1. Einleitung  

Dieser Quick Start Guide soll die Installation und Konfiguration des CrefoPay Payment Plugin für den Oxid eShop 6 vereinfachen. Die Schritt f&uuml;r Anleitung beinhaltet alle notwenidgen Schritte zur Inbetriebnahme. Weiterf&uuml;rende Informationen sind in dem zugeh&ouml;rigen [Wiki](https://repo.crefopay.de/crefopay/oxid/-/wikis/home) enthalten.

### 1.1 Bevor es los geht  

Bevor es mit der Installation und Konfiguration des CrefoPay Payment Plugin los gehen kann, sollten die folgenden Informationen bereit gehalten werden:  

* Shell-Zugang zu Ihrer Oxid Installation  
* CrefoPay Payment Plugin f&uuml;r Oxid eShop 6
  * Unterstützte Oxid-Versionen: <= 6.1 < 7.0
* CrefoPay Zugangsdaten
  * Merchant ID  
  * Store ID(s)  
  * Public Key  
  * Private Key  

### 1.2 Haftungsausschluss  

Die CrefoPayment GmbH & Co. KG als Herausgeber der Software &uuml;bernimmt keinerlei Haftung f&uuml;r etwaige Schäden, die durch die Verwendung des CrefoPay Payment Plugin entstehen. Um vor der Inbetriebnahme im Produktivbetrieb ausf&uuml;hliche Funktions-Tests durchf&uuml;ren zu k&ouml;nnen, kann beim CrefoPay Service Team unter [service@crefopay.de](mailto:service@crefopay.de) ein Zugang zur CrefoPay [Sandbox](https://sandbox.crefopay.de) angefragt werden.  

### 2 Backup  

Das CrefoPay Payment Plugin ist ausf&uuml;hrlich getestet und von der Qaulit&auml;tssicherung der CrefoPayment GmbH & Co. KG gepr&uuml;ft und freigegeben worden. Dennoch kann es bei jeder Software unter ung&uuml;stigen Umst&auml;nden zu unerwarteten Fehlern bei der Installation bzw. der Inbetriebnahme kommen. Aus diesem Grund wird *dringend* dazu geraten eine Systemsicherung (Backup) zu erstellen, bevor die Software installiert und in Betrieb genommen wird.  
  
## 3. Installation  

Das CrefoPay Payment Plugin wird als composer-Paket via packagist.org bereitgestellt und wird über composer installiert:

1. Wechseln Sie in das Hauptverzeichnis der Shop-Installation (das Verzeichnis in dem sich die 'source'- und 'vendor'-Ordner befinden)
2. Führen Sie `composer require crefopay/cppayments-oxid6-module` aus, um das Modul zu installieren
3. Das Modul ist nun im Oxid-Backend unter Erweiterungen > Module sichtbar und kann dort aktiviert werden

Falls der composer-Befehl nicht verfügbar ist, müssen Sie ggf. Ihren Hoster um Support bitten.
Falls composer require erfolgreich durchgeführt wurde, das Modul im Oxid-Backend aber nicht sichtbar wird: Führen sie folgendes aus:
`vendor/bin/oe-console oe:module:install-configuration source/modules/crefopay/cppayments` um das Modul in die yaml eintragen zu lassen

## 4. Aktivierung  

### 4.1 Eintragen der Zugangsdaten  

F&uuml;r die Aktivierung des CrefoPay Payment Plugin werden die CrefoPay Zugangsdaten ben&ouml;tigt. Diese k&ouml;nnen bequem &uuml;ber den CrefoPay Händler Service Bereich im Abschnitt Shop Details unter API Zugangsdaten abgerufen werden ([Sandbox](https://sandbox.crefopay.de/msa3/#/authorized/settings/shopData)|[Live](https://service.crefopay.de/msa3/#/authorized/settings/shopData)).
Die Aktivierung der CrefoPay Bezahlarten erfolgt &uuml;ber den Men&uuml;punkt *Erweiterungen > Module* im Oxid Backend.  

1. Zun&auml;st muss das Modul *CrefoPay Bezahlarten* angeklickt werden, um den Stamm-Tab zu &ouml;ffnen.  
2. Als n&auml;stes muss auf den *Einstell.* Tab gewechselt und der Bereich Zugangsdaten ge&ouml;ffnet werden.  
3. Dort sind nun die bereitgestellten CrefoPay Zugangsdaten einzutragen und anschlie&szlig;end mit *Speichern* zu best&auml;tigen. **Hinweis:** Die Fragezeichen-Symbole liefern weitere Informationen zu den zugeh&ouml;rigen Eingabefeldern.  
4. Zuletzt kann das Plugin durch einen Klick auf *Aktivieren* aktiviert werden. **Hinweis:** Sollten bei der Aktivierung Fehler auftreten werden diese angezeigt. Der angezeigten Fehler sollte unbedingt DOkumentiert werden, damit dass das [CrefoPay Service Team](mailto:service@crefopay.de) bei der L&ouml;sung etwaiger Fehler bestm&ouml;glich unterst&uuml;tzen kann.  

### 4.2 Aktivierung der CrefoPay Bezahlarten  

1. Zun&auml;chst muss die zu aktivierende Zahlungsart in der Liste angeklickt werden. **Hinweis:** Sind sehr viele Zahlungsarten im Shop verf&uuml;gbar, kann die Suchfunktion genutzt werden, in dem der Begriff *CrefoPay* in die Suchleiste eingtragen wird.  
2. Nach dem anklicken einer Bezahlart kann diese individuell konfiguriert werden.  
3. Zur Aktivierung der Bezahlart muss nun die Checkbox *Aktiv* gesetzt werden.  
4. Im Feld *Name* ist der im Shop Frontend anzuzeigende Name der Bezahlart zu definieren.  
5. Zuletzt werden die Einstellungen mit einem Klick auf *Speichern* &uuml;bernommen.  
*Die Schritte 1 bis 5 m&uuml;ssen f&uuml;r alle zu aktivierenden CrefoPay Bezahlarten wiederholt werden.*
6. Ordnen Sie die Crefopay-Zahlarten Ihren  Versandarten zu, siehe dazu: <https://docs.oxid-esales.com/eshop/de/6.2/einrichtung/versandarten/registerkarte-zahlungsarten.html>  

## 5. Konfiguration  

Detaillierte Informationen zu den m&ouml;glichen Oxid eShop Konfigutationsoptionen k&ouml;nnen der allgemeinen Oxid Dokumentation entnommen werden. Dieser Quick Start Guide beschr&auml;kt sich ausschlie&szlig;lich auf die f&uuml;r die Aktivierung von CrefoPay relevanten Informationen.

Die Konfiguration des Moduls erfolgt, ebenso wie bereits die Aktivierung, &uuml;ber den *Einstell.* Tab. Im Folgenden sind die m&ouml;glichen Optionen tabellarisch zusammengefasst.

| **#** | **Abschnitt**         | **Option**                                                                 | **Bemerkung**                                                                                                                                                                                                                                                                                                                                                                        |
|-------|-----------------------|----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1     | Modus                 | Sandbox \| Live                                                            | Hier kann zwischen Testmodus (Sandbox) und Live umgeschaltet werden. Achtung: Die Sandbox Zugangsdaten und die Live Zugangsdaten sind i.d.R. abweichend!                                                                                                                                                                                                                             |
| 2.1   | Zugangsdaten          | Merchant ID                                                                | API Zugangsdaten: Händler ID                                                                                                                                                                                                                                                                                                                                                         |
| 2.2   | Zugangsdaten          | Shop ID                                                                    | API Zugangsdaten: Shop ID                                                                                                                                                                                                                                                                                                                                                            |
| 2.3   | Zugangsdaten          | Privater Schlüssel                                                         | API Zugangsdaten: Privater Schlüssel                                                                                                                                                                                                                                                                                                                                                 |
| 2.4   | Zugangsdaten          | Öffentlicher Schlüssel                                                     | API Zugangsdaten: Öffentlicher Schlüssel                                                                                                                                                                                                                                                                                                                                             |
| 3     | CronJob               | -                                                                          | Wichtig für die Verarbeitung der CrefoPay Notifications                                                                                                                                                                                                                                                                                                                              |
| 4.1   | Bestell-Einstellungen | Zahlungstransaktionen bei Bestellabschluss sofort ausführen (Auto-Capture) | Der Auto Capture sorgt dafür, dass CrefoPay Bestellungen direkt bei Authorisierung durch den Endkunden bereits gebucht, also in Rechnung gestellt werden. Diese Funktion sollte nur für den Verkauf digitaler Güter oder in Absprache mit dem CrefoPay Service Team aktiviert werden.                                                                                                |
| 4.2   | Bestell-Einstellungen | Business-Transaktionen                                                     | Ist diese Funktion deaktiviert, werden alle Bestellungen automatisch als Privatkundentransaktionen behandelt. Bei aktivierter Option, werden Transaktionen, bei deinen ein Firmenname angegeben wurde als Businesstransaktion behandelt. Für die Bezahlarten Rechnung und Lastschrift bedeutet das auch, dass die entsprechende Schnittstelle zur Bonitätsabfrage angesprochen wird. |
| 4.3   | Bestell-Einstellungen | Auftragnr schon vor Bestellabschluss vergeben                              | Ist diese Funktion aktiviert, wird die finale Oxid Bestellnummer bereits vor der Durchführung der Zahlung erstellt. Wird die Zahlung abgelehnt oder vom Endkunden nicht beendet, können Lücken in den normalerweise durchgängig aufsteigenden Oxid-Bestellnummern entstehen.                                                                                                         |
| 4.4   | Bestell-Einstellungen | jQuery                                                                     | Verwendet das eingesetzte Oxid-Frontend-Theme kein jQuery, muss hier die Option Eigene jQuery Library verwenden ausgewählt sein, da das CrefoPay Plugin jQuery Support benötigt.                                                                                                                                                                                                     |
| 4.5   | Bestell-Einstellungen | Geburtsdatum                                                               | Hier kann die Geburtsdatumsabfrage- und Speicherung konfiguriert werden. Das Geburtsdatum wird für die Bonitätsabfrage von Privatpersonen benötigt.                                                                                                                                                                                                                                  |
| 4.6   | Bestell-Einstellungen | Bestellnummer-Präfix                                                       | Hier kann ein Präfix für die von dem CrefoPay Plugin automatisch generierten OrderIDs angegeben werden. Dies kann für die Abgrenzung gegenüber anderen, nicht CrefoPay Zahlungsarten nützlich sein.                                                                                                                                                                                  |
| 4.7   | Bestell-Einstellungen | Zahlungsziel bei Vorkasse                                                  | Das Modul erweitert unter anderem die E-Mail zur Bestellbestätigung und ergänzt diese im Falle einer Bestellung mit der Bezahlart Vorkasse um die für den Endkunden wichtigen (Bankkonto-) Informationen. Mit dieser Option kann das Zahlungsziel (in Tagen) angegeben werden, zu dem eine Überweisung des Käufers spätestens erwartet wird.                                         |
| 4.8   | Bestell-Einstellungen | Zahlungsziel bei Rechnung                                                  | Achtung: Diese Option steuert NICHT das bei CrefoPay hinterlegte Zahlungsziel für die Zahlungsüberwachung und das autom. Mahnwesen, sondern dient lediglich der Anzeige für Oxid-Shop-Kunden. Um das Zahlungsziel für die Zahlungsüberwachung durch CrefoPay anzupassen, kontaktieren Sie Ihren Ansprechpartner bei CrefoPay.                                                        |
| 4.9   | Bestell-Einstellungen | Warenkorb-Gültigkeit                                                       | CrefoPay Transaktionen haben eine Gültigkeit zwischen Erstellung und erfolgreichem Abschluss. Mit dieser Option kann dieser Zeitraum definiert werden. Hier **muss** eine Zahl angegeben werden da es sonst zu Kommunikationsfehlern mit der CrefoPay API kommt.                                                                                                                     |
| 5.1   | Kreditkarten-Logos    | CVV Hilfe                                                                  | Mit dieser Option wird bei Auswahl der Bezahlart Kreditkarte im Checkout ein Bild eingeblendet, in dem markiert ist, wo der Endkunde auf seiner Kreditkarte die CVV findet.                                                                                                                                                                                                          |
| 5.2   | Kreditkarten-Logos    | MasterCard-Logo                                                            | Mit dieser Option wird bei Auswahl der Bezahlart Kreditkarte im Checkout das MasterCard Logo angezeigt.                                                                                                                                                                                                                                                                              |
| 5.3   | Kreditkarten-Logos    | VISA-Logo                                                                  | Mit dieser Option wird bei Auswahl der Bezahlart Kreditkarte im Checkout das VISA Logo angezeigt.                                                                                                                                                                                                                                                                                    |
| 6.1   | Weitere Einstellungen | Warenkorb-Positionen übermitteln                                           | Falls nicht angehakt wird nur eine Position mit der Gesamtsumme übermittelt. Falls angehakt werden die Einzelpositionen und (sofern nötig) Positionen für Gutscheine/Rabatte, Versandkosten, etc. Dabei können jedoch potentiell gegenüber dem Oxid-Warenkorb Rundungsabweichungen entstehen, die das Modul versucht durch Anpassung der letzten Position auszugleichen.             |
| 6.2   | Weitere Einstellungen | Nur CrefoPay-Bezahlarten anzeigen                                          | Das CrefoPay Modul ist in der Lage, konkurrierende Bezahlarten auszublenden. Ist diese Option aktiv werden ausschließlich CrefoPay Bezahlarten im Checkout angezeigt.                                                                                                                                                                                                                |
| 6.3   | Weitere Einstellungen | Bei Crefopay für den User gespeicherte Zahlungsmittel anzeigen             | Ist diese Option aktiv, erhalten angemeldete Kunden ihre früher bei CrefoPay verwendeten Kreditkarten und Bankkonten für einen vereinfachten Checkout angeboten. Die Zahlungs-Daten werden dabei nicht in Oxid gespeichert, sondern liegen sicher bei CrefoPay und werden dem Endkunden ausschließlich maskiert angezeigt.                                                           |
| 6.4   | Weitere Einstellungen | Bonitätsscore in Oxid-Kundenkonto übernehmen                               | Ist diese Option aktiv, werden die bei einer durchgeführten Bonitätsprüfung gelieferten Scores (sofern vorhanden) in dem dafür vorgesehenen Feld im Kundenkonto gespeichert.                                                                                                                                                                                                         |
| 6.5   | Weitere Einstellungen | Log-Level                                                                  | Das Modul verfügt über verschiedene Protokollierungslevel. Debug protokolliert alle relevanten Aktionen des Moduls. Warn protokolliert schwere und leichte Fehler des Moduls. Error protokolliert nur die schweren Fehler, die kritische Auswirkungen auf die Prozessabläufe der CrefoPay Anbindung haben.                                                                           |
| 6.6   | Weitere Einstellungen | Logfiles löschen nach (Tage)                                               | Das Plugin löscht nicht mehr benötigte Logdateien nach der hier angegebenen Zeit in Tagen automatisch, um nicht unnötig dauerhaft Speicherplatz auf dem Server zu belegen.                                                                                                                                                                                                           |
| 6.7   | Weitere Einstellungen | Abgelaufene Transaktionen löschen nach (Tage)                              | Vom Endkunden nicht abgeschlossene CrefoPay Transaktionen wechseln nach Ablauf der Warenkorbgültigkeit in den Status Expired. Die nicht mehr benötigten Daten in der Oxid-Datenbank werden nach Ablauf der hier festgelegten Frist in Tagen ebenfalls gelöscht.                                                                                                                      |
