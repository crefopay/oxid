/* jshint strict: true */
/* jshint esversion: 6 */

(function() {
    'use strict';

    /* This file gets only loaded on cl=order for paypal */

    var crefopayInitializationDone=false;
    var crefopayRegisterPaymentTimeoutInMS=10800000; // 3h
    var initializationCompleteCallbackTimeoutInMS=15000;
    var secureFieldsClientInstance=null;
    var crefopayPaymentRegistered=false;
    var skipOrderSubmitHandler=false;
    var crefopayTimeoutTimerId=null;
    var crefopayPaymentRegisteredCallbackReceived=false;

    var submitButton=CrefopaySlimjQuery(".submitButton");
    var orderConfirmAgbBottomForm=CrefopaySlimjQuery("#orderConfirmAgbBottom");
    if (orderConfirmAgbBottomForm.length===0)
    {
        console.log("[crefopay] Warning: form #orderConfirmAgbBottom is missing!");
    }
    if (submitButton.length===0)
    {
        console.log("[crefopay] Warning: .submitButton in form #orderConfirmAgbBottom is missing!");
    }

    let paymentRegisteredCallbackResponse={};

    function updateCrefopayCallbackData()
    {
        let crefopaycallbackdataField = orderConfirmAgbBottomForm.find("input[name=crefopaycallbackdata]");
        if (crefopaycallbackdataField.length === 0) {
            crefopaycallbackdataField = CrefopaySlimjQuery("<input type='hidden' name='crefopaycallbackdata'>");
            crefopaycallbackdataField.appendTo(orderConfirmAgbBottomForm);
        }
        crefopaycallbackdataField.val(JSON.stringify(paymentRegisteredCallbackResponse));
    }

    function paymentRegisteredCallback(response) {
        console.log("[crefopay] paymentRegisteredCallback called!");

        if (typeof response.resultCode === 'undefined' || response.resultCode === null) {
            console.log("[crefopay] ",response);
            alert(window.crefopayTranslations['CREFOPAY_JSERROR_UNKNOWNERROR']);
            submitButton.prop("disabled", false);
        } else {
            if (response.resultCode === 0) {

                paymentRegisteredCallbackResponse=response;

                crefopayPaymentRegistered=true;
                console.log("[crefopay] paymentRegisteredCallback success");
                console.log("[crefopay] ",response);

                updateCrefopayCallbackData();

                if (response.paymentMethod==='GOOGLE_PAY') {
                    console.log("[crefopay] show google pay layer");
                    CrefopaySlimjQuery(".js-crefopay-onemoment").show();
                    window.onGooglePaymentButtonClickedContinue();
                }
                else
                {
	                console.log("[crefopay] submitting form");
	                skipOrderSubmitHandler=true;
	                orderConfirmAgbBottomForm.submit();
	            }
            }
            else
            {
                console.log("[crefopay] ",response);
                submitButton.prop("disabled", false);

                var errorIdent='CREFOPAY_JSERROR_'+response.resultCode;
                if (window.crefopayTranslations[errorIdent])
                {
                    alert(window.crefopayTranslations[errorIdent]);
                }
                else{
                    console.log("[crefopay] ",response);
                    alert(window.crefopayTranslations['CREFOPAY_JSERROR_UNKNOWNERROR']);
                }
            }
        }
    }

    function initializationCompleteCallback(response) {
        var initResultCode = -1;
        if (response) {
            initResultCode = parseInt(response.resultCode, 10);
        }

        // Mark actual result received:
        if (initResultCode>=-1) {
            crefopayInitializationDone = true;
        }

        if (initResultCode === 0) {
            console.log("[crefopay] session created successfully");

            const event = new Event('crefopaySessionCreated')
            document.dispatchEvent(event)

        } else {
            console.log("[crefopay] session was not created!");
            console.log("[crefopay] response",response);
        }

        CrefopaySlimjQuery(".js-crefopay-onemoment").hide();
    }

    // Catch default next button and trigger registerPayment() instead
    CrefopaySlimjQuery(document).ready(function() {

        if (typeof SecureFieldsClient === "undefined")
        {
            console.log("[crefopay] secure-fields.js seems to be blocked or is included in the wrong order?");
            crefopayInitializationDone=true;
            initializationCompleteCallback({
                'resultCode':-2
            });
        }
        else if (!window.crefopayConfig.orderID)
        {
            // got no orderID - module configuration probably incomplete
            console.log("[crefopay] transaction was not started - check configuration");
            crefopayInitializationDone=true;
            initializationCompleteCallback({
                'resultCode':-2
            });
        }
        else {
            secureFieldsClientInstance = new SecureFieldsClient(
	        window.crefopayConfig.shopPublicKey,
	        window.crefopayConfig.orderID,
	        paymentRegisteredCallback,
	        initializationCompleteCallback,
	        window.crefopayConfig.configuration
	    );

            // Fallback: If no initializationCompleteCallback() is called in time:
            window.setTimeout(function()
            {
                if (!crefopayInitializationDone)
                {
                    console.log("[crefopay] secure-fields not initialized (timeout/cors issue in SecureFieldsClient)");
                    initializationCompleteCallback({
                        'resultCode':-3
                    });
                }
            },initializationCompleteCallbackTimeoutInMS);
        }

        // Check if execute() would fail due to missing agb-checkbox and other reasons
        // before showing the customer the paypal-layer
        function crefopayPrevalidate()
        {
            var orderConfirmAgbBottomFormFnc=orderConfirmAgbBottomForm.find("input[name=fnc]")

            orderConfirmAgbBottomFormFnc.val("crefopayvalidateorder");
            var formData=orderConfirmAgbBottomForm.serialize();
            orderConfirmAgbBottomFormFnc.val("execute");

            CrefopaySlimjQuery.ajax({
                'url': orderConfirmAgbBottomForm.attr("action"),
                'data':formData,
                'method':'POST',
                'success':function(data)
                {
                    if (parseInt(data.success,10)===1)
                    {
                        let paymentMethod=CrefopaySlimjQuery("input[type=radio][data-crefopay='paymentMethod']").val();

                        if (paymentMethod!=='GOOGLE_PAY') {
                        CrefopaySlimjQuery(".js-crefopay-onemoment").hide();
                        }

                        crefopayOpenPaypalOrGooglePay();
                    }
                    else{
                        // Oxid will show some error message
                        skipOrderSubmitHandler=true;
                        orderConfirmAgbBottomForm.submit();
                    }
                },
                'error':function(xhr)
                {
                    // Oxid will probably show some error message
                    skipOrderSubmitHandler=true;
                    orderConfirmAgbBottomForm.submit();
                }
            });
        }
        window.crefopayPrevalidate=crefopayPrevalidate;

        function crefopayFinalizeGooglePay(token)
        {
            console.log("[crefopay] crefopayFinalizeGooglePay reached");
            CrefopaySlimjQuery(".js-crefopay-onemoment").show();

            // Add payment-token:
            paymentRegisteredCallbackResponse['paymentToken']=token;
            updateCrefopayCallbackData();

            // slightly delay the submit to let browser not skip showing the onemoment-layer
            window.setTimeout(function() {
                skipOrderSubmitHandler = true;             // prevalidate already checked terms-and-conditions, ...
                orderConfirmAgbBottomForm.submit();
            },0);
        }
        window.crefopayFinalizeGooglePay=crefopayFinalizeGooglePay;

        window.crefopayGooglePayCanceled=function(){
            // Reload site without POST (re-initialization needed to be able to open google-pay again):
            location.replace(location.href);
        };

        function crefopayOpenPaypalOrGooglePay()
        {
            if (!crefopayPaymentRegistered)
            {
                console.log("[crefopay] is crefopay payment-method, calling registerPayment");

                // Start timeout timer in case registerPayment() does not return in time:
                {
                    if (crefopayTimeoutTimerId) {
                        // Stop existing timer:
                        clearTimeout(crefopayTimeoutTimerId);
                        crefopayTimeoutTimerId = null;
                    }
                    crefopayPaymentRegisteredCallbackReceived = false;
                    crefopayTimeoutTimerId = window.setTimeout(function () {
                        if (!crefopayPaymentRegisteredCallbackReceived) {
                            console.log("[crefopay] registerPayment() did not fire paymentRegisteredCallback() in time");
                            var errorResponse = {
                                'result': 1000,
                                'message': 'registerPayment() did not fire paymentRegisteredCallback() in time'
                            };
                            paymentRegisteredCallback(errorResponse);
                        }
                    }, crefopayRegisterPaymentTimeoutInMS);
                }

                secureFieldsClientInstance.registerPayment();
            }
        }

        orderConfirmAgbBottomForm.on("submit", function(evt) {

            if (!crefopayInitializationDone) {
                alert("Bitte probieren Sie es in wenigen Sekunden erneut. Die Paypal-Anbindung lädt gerade noch...");
                evt.preventDefault();
                return false;
            }

            if (skipOrderSubmitHandler) {
                skipOrderSubmitHandler = false; // Reset
                CrefopaySlimjQuery(".js-crefopay-onemoment").show();
            } else {
                submitButton.prop("disabled", true);
                CrefopaySlimjQuery(".js-crefopay-onemoment").show();
                crefopayPrevalidate();
                evt.preventDefault();
            }
        });
    });
})();