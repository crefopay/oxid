(function() {
    'use strict';

    var crefopayInitializationDone=false;
    var crefopayRegisterPaymentTimeoutInMS=10000;
    var initializationCompleteCallbackTimeoutInMS=15000;
    var secureFieldsClientInstance=null;
    var paypal2Enabled=true;
    var crefopayTimeoutTimerId=null;
    var crefopayPaymentRegisteredCallbackReceived=false;

    function getCrefopayErrorIdent(str)
    {
        if (!str)
        {
            return '';
        }

        var str2=str.toUpperCase();

        var allowedChars="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        var rv='';
        for(var x=0; x<str2.length; x++)
        {
            if (allowedChars.indexOf(str2[x])>=0)
            {
                rv+=str2[x];
            }
        }

        return 'CREFOPAY_JSERROR_'+rv;
    }

    function setNextStepBlocked(setBlocked)
    {
        CrefopaySlimjQuery("#paymentNextStepBottom").prop("disabled",setBlocked);
        if (setBlocked) {
            CrefopaySlimjQuery(".crefopay_onemoment_layer").show();
        }
        else{
            CrefopaySlimjQuery(".crefopay_onemoment_layer").hide();
        }
    }

    function paymentRegisteredCallback(response) {
        crefopayPaymentRegisteredCallbackReceived=true;
        console.log("[crefopay] paymentRegisteredCallback called!");

        // Restore PaymentGroups:
        CrefopaySlimjQuery(".js-crefopay-payment-group").show();

        if (typeof response.resultCode === 'undefined' || response.resultCode === null) {
            console.log("Unerwarteter Fehler: fehlender responseCode - Bitte senden Sie diese Fehlermeldung unter service@crefopay.de direkt an den CrefoPay Support");

            // Restore button state:
            setNextStepBlocked(false);
            console.log("[crefopay] Unblocking #paymentNextStepBottom button. paymentRegisteredCallback() returned with unknown error");
            alert(window.crefopayTranslations['CREFOPAY_JSERROR_UNKNOWNERROR']);

        } else {

            if (response.resultCode === 0) {

                console.log("[crefopay] paymentRegisteredCallback success");
                var paymentForm=CrefopaySlimjQuery("#payment");

                var crefopaycallbackdataField=paymentForm.find("input[name=crefopaycallbackdata]");
                if (crefopaycallbackdataField.length===0) {
                    crefopaycallbackdataField=CrefopaySlimjQuery("<input type='hidden' name='crefopaycallbackdata'>");
                    crefopaycallbackdataField.appendTo(paymentForm);
                }
                crefopaycallbackdataField.val(JSON.stringify(response));

                console.log("[crefopay] submitting form");
                paymentForm.submit();
            } else {

                var hasInvalidField = false;
                var firstInvalidFieldTop = -1;
                // Restore button state:
                setNextStepBlocked(false);
                console.log("[crefopay] Unblocking #paymentNextStepBottom button. paymentRegisteredCallback() returned with error.");

                CrefopaySlimjQuery('label.crefopay-label').removeClass('text-danger');
                CrefopaySlimjQuery(".crefopay-additional-error").text("");

                // Mark invalid fields:
                if (response.errorDetails !== undefined) {
                    for (var index = 0; index < response.errorDetails.length; index++) {
                        var detail = response.errorDetails[index];
                        var labelElement = CrefopaySlimjQuery("label[data-crefopay-field='" + detail.field + "']");
                        if (labelElement.length>0) {
                            labelElement.addClass('text-danger');
                            labelElement.attr("data-crefopay-error-description", detail.description);

                            var jsErrorIdent = getCrefopayErrorIdent(detail.description);
                            if (window.crefopayTranslations[jsErrorIdent]) {
                                labelElement.find(".crefopay-additional-error").text(window.crefopayTranslations[jsErrorIdent]);
                            } else {
                                labelElement.find(".crefopay-additional-error").attr("data-crefopay-error-ident", jsErrorIdent);
                            }

                            // scroll into view:
                            var eleOffset = labelElement.offset();
                            hasInvalidField = true;

                            if ((eleOffset.top < firstInvalidFieldTop) || (firstInvalidFieldTop < 0)) {
                                firstInvalidFieldTop = eleOffset.top;
                            }
                        } else {
                            console.log("[crefopay]: No label found for: ", detail.field);
                        }
                    }
                }

                if (hasInvalidField) {
	                CrefopaySlimjQuery('html, body').animate({
	                        scrollTop: firstInvalidFieldTop-150
	                    },100);

                    alert(window.crefopayTranslations['CREFOPAY_JSERROR_PLEASEVALIDATEINPUTS']);
                } else {
                    if (response.message) {
                        console.log("[crefopay] ",response.message);
                    }
                    alert(window.crefopayTranslations['CREFOPAY_JSERROR_UNKNOWNERROR']);
                }
            }
        }
    }

    function initializationCompleteCallback(response) {
        var initResultCode = -1;
        if (response) {
            initResultCode = parseInt(response.resultCode, 10);
        }

        // Mark actual result received:
        if (initResultCode>=-1) {
            crefopayInitializationDone = true;
        }

        if (initResultCode === 0) {
            console.log("[crefopay] session created successfully");

            // Remove not allowed payment methods:
            CrefopaySlimjQuery("input[data-crefopay=paymentMethod]").each(function()
            {
                var inpEle=CrefopaySlimjQuery(this);
                var cpPaymentMethodId=inpEle.val();
                if (window.crefopayAllowedPaymentMethods.indexOf(cpPaymentMethodId)<0)
                {
                    inpEle.closest("dl").remove();
                }
            });

            // Show payment methods:
            CrefopaySlimjQuery(".js-crefopay-change-payment-container").show();
            CrefopaySlimjQuery(".js-crefopay-change-payment-loader").hide();
        } else {
            console.log("[crefopay] session was not created!");
            console.log("[crefopay] response",response);

            // Show payment methods, but hide crefopay payment methods:
            CrefopaySlimjQuery("input.js-crefopay-paymentmethod").closest("dl").hide();

            CrefopaySlimjQuery(".js-crefopay-change-payment-container").show();
            CrefopaySlimjQuery(".js-crefopay-change-payment-loader").hide();

            if (CrefopaySlimjQuery("input[name=paymentid]:visible").length===0)
            {
                CrefopaySlimjQuery(".js-crefopay-change-payment-container").hide();
                CrefopaySlimjQuery(".js-crefopay-change-payment-error").show();
                CrefopaySlimjQuery(".js-crefopay-change-payment-error-no").text(initResultCode);
            }
        }
    }

    // Catch default next button and trigger registerPayment() instead
    CrefopaySlimjQuery(document).ready(function() {

        if (typeof SecureFieldsClient === "undefined")
        {
            console.log("[crefopay] secure-fields.js seems to be blocked or is included in the wrong order?");
            crefopayInitializationDone=true;
            initializationCompleteCallback({
                'resultCode':-2
            });
        }
        else if (!window.crefopayConfig.orderID)
        {
            // got no orderID - module configuration probably incomplete
            console.log("[crefopay] transaction was not started - check configuration");
            crefopayInitializationDone=true;
            initializationCompleteCallback({
                'resultCode':-2
            });
        }
        else {
            secureFieldsClientInstance = new SecureFieldsClient(
	        window.crefopayConfig.shopPublicKey,
	        window.crefopayConfig.orderID,
	        paymentRegisteredCallback,
	        initializationCompleteCallback,
	        window.crefopayConfig.configuration
	    );

            // Fallback: If no initializationCompleteCallback() is called in time:
            window.setTimeout(function()
            {
                if (!crefopayInitializationDone)
                {
                    console.log("[crefopay] secure-fields not initialized (timeout/cors issue in SecureFieldsClient)");
                    initializationCompleteCallback({
                        'resultCode':-3
                    });
                }
            },initializationCompleteCallbackTimeoutInMS);
        }

        function switchPaymentMethod(newPaymentMethodElement)
        {
            CrefopaySlimjQuery("input[data-crefopay='paymentMethod']").prop("checked", false);

            CrefopaySlimjQuery("#cp" + newPaymentMethodElement.attr("id")).prop("checked", newPaymentMethodElement.prop("checked"));
            var savedPaymentInstruments=newPaymentMethodElement.closest("dl").find(".js-crefopay-paymentinstrumentid");

            var selectedPaymentInstrumentsCount=0;
            savedPaymentInstruments.each(function() {
                if (CrefopaySlimjQuery(this).prop("checked")){
                    selectedPaymentInstrumentsCount++;
                }
            });

            // Select the default (=first) if none or more than one paymentinstruments are checked:
            if ((selectedPaymentInstrumentsCount===0) || (selectedPaymentInstrumentsCount>1)) {
                savedPaymentInstruments.first().prop("checked", true);
            }
        }

        // Switch hidden crefopay radio-input:
        var inPaymentChange=0;
        CrefopaySlimjQuery("input[name='paymentid']").on("change", function(event) {

            if (inPaymentChange>0)
            {
                return;
            }

            inPaymentChange++;
            {
                switchPaymentMethod(CrefopaySlimjQuery(this));
            }
            inPaymentChange--;
        });

        // init current selection:
        var ccheckedElement = CrefopaySlimjQuery("input[name='paymentid']:checked");
        switchPaymentMethod(ccheckedElement);

        // Redirect '#orderStep'-click to #paymentNextStepBottom to prevent bypassing validation steps:
        var orderStepButton=CrefopaySlimjQuery("#orderStep");
        if (orderStepButton.length===0) {
            console.log("[crefopay] step4 with id #orderStep not found! template/block broken or steps not visible?");
        }
        else{
            CrefopaySlimjQuery("#orderStep").on("click", function(e)
            {
                var nextBtt=CrefopaySlimjQuery("#paymentNextStepBottom");
                if (!nextBtt.prop("disabled"))
                {
                    console.log("[crefopay] Forwarding next step button to #paymentNextStepBottom button");
                    nextBtt.trigger("click");
                }
                else{
                    console.log("[crefopay] Ignoring click to step-button (#paymentNextStepBottom is disabled)");
                }

                e.preventDefault();
                return false;
            });
        }

        var bttEle=CrefopaySlimjQuery("#paymentNextStepBottom");
        if (bttEle.length===0)
        {
            console.log("[crefopay] button with id #paymentNextStepBottom not found! template/block broken?");
        }
        else {
            bttEle.click(function (evt) {

                if (bttEle.prop("disabled"))
                {
                    // Button click handler already running...
                    console.log("[crefopay] button click ignored - click handler already running");
                    evt.preventDefault();
                    return false;
                }

                var clickedPaymentMethod = CrefopaySlimjQuery("input[name='paymentid']:checked");
                if (clickedPaymentMethod.length > 0) {
                    var paymentMethodId=clickedPaymentMethod.val();

                    var salutationSelectName=".js-crefopaysalutation[data-paymentid='"+paymentMethodId+"']";
                    var salutationSelect=CrefopaySlimjQuery(salutationSelectName);
                    if (salutationSelect.length>0)
                    {
                        var salutationValue=salutationSelect.val();
                        if (!salutationValue)
                        {
                            alert(window.crefopayTranslations['CREFOPAY_JSERROR_CHOOSE_SALUTATION']);
                            bttEle.prop("disabled",false);
                            console.log("[crefopay] Enabling button. Choose salutation error shown");
                            evt.preventDefault();
                            return false;
                        }
                    }

                    var activePaymentDD=clickedPaymentMethod.closest("dl");
                    if (activePaymentDD.length===0)
                    {
                        console.log("[crefopay] input[name='paymentid'] is not inside 'dl'-element - template/block broken?");
                    }

                    var paymentInstrumentId='';
                    var paymentInstrumentIdElement=activePaymentDD.find("input.js-crefopay-paymentinstrumentid:checked");
                    if (paymentInstrumentIdElement.length>0) {
                        paymentInstrumentId = paymentInstrumentIdElement.val();
                    }

                    console.log("[crefopay] selected payment-method:",paymentMethodId);
                    console.log("[crefopay] selected payment-instrumentid:",paymentInstrumentId);
                    CrefopaySlimjQuery(".js-crefopay-selected-paymentinstrumentid").val(paymentInstrumentId);

                    // Hide irrelevant paymentgroups so that the input fields inside are not getting involved:
                    CrefopaySlimjQuery(".js-crefopay-payment-group").each(function()
                    {
                        var paymentGroup=CrefopaySlimjQuery(this);
                        if (paymentGroup.find(paymentInstrumentIdElement).length===0)
                        {
                            paymentGroup.hide();
                        }
                    });

                    if (CrefopaySlimjQuery("#cppayment_" + paymentMethodId).prop("checked")) {

                            setNextStepBlocked(true);
                            console.log("[crefopay] Blocking #paymentNextStepBottom button");
                            console.log("[crefopay] is crefopay payment-method, calling registerPayment");
                            evt.preventDefault();
                            evt.stopPropagation();

                            // Start timeout timer in case registerPayment() does not return in time:
                            {
                                if (crefopayTimeoutTimerId) {
                                    // Stop existing timer:
                                    clearTimeout(crefopayTimeoutTimerId);
                                    crefopayTimeoutTimerId = null;
                                }
                                crefopayPaymentRegisteredCallbackReceived = false;
                                crefopayTimeoutTimerId = window.setTimeout(function () {
                                    if (!crefopayPaymentRegisteredCallbackReceived) {
                                        console.log("[crefopay] registerPayment() did not fire paymentRegisteredCallback() in time");
                                        var errorResponse = {
                                        'resultCode': 1000,
                                            'message': 'registerPayment() did not fire paymentRegisteredCallback() in time'
                                        };
                                        paymentRegisteredCallback(errorResponse);
                                    }
                                }, crefopayRegisterPaymentTimeoutInMS);
                            }

                        var f=CrefopaySlimjQuery("#crefopay_refresh_ajaxform");
                        f.find("input[name=paymentid]").val(paymentMethodId);

                        console.log("[crefopay] calling crefopay_refresh()");
                        CrefopaySlimjQuery.ajax({
                            "url":f.attr("action"),
                            "method":"POST",
                            "data":f.serialize(),
                            'success':function(data)
                            {
                                if (crefopayConfig.orderID!==data.cporderid)
                                {
                                    // The transaction-id has changed
                                    // Reload the payment-page to re-initialize secured fields
                                    location.reload();
                                }
                                else {
                                    if ((paymentMethodId === 'cppaypal') && (paypal2Enabled)) {
                                        // Skip registerPayment() here
                                        // will get called in ?cl=order instead
                                        console.log("[crefopay] Skipping registerPayment() due to paypal2");

                                        console.log("[crefopay] submitting form");
                                        CrefopaySlimjQuery("#payment").submit();
                                    } else {
                                        console.log("[crefopay] calling registerPayment()");
                            secureFieldsClientInstance.registerPayment();
                        }
                                }
                            },
                            'error':function(xhr)
                            {
                                console.log("[crefopay] Failed with: ",xhr.responseText);

                                var errorResponse = {
                                    'resultCode': 1000,
                                    'message': 'updating transaction before registerPayment() failed'
                                };
                                paymentRegisteredCallback(errorResponse);
                            }
                        });
                    }
                    else
                    {
                        console.log("[crefopay] is not a crefopay payment-method, not handling button click");
                    }
                }
            });
            console.log("[crefopay] button-handler for #paymentNextStepBottom activated!");
        }

        CrefopaySlimjQuery(".js-crefopay-remove-userpaymentinstrument").click(function(e)
        {
            var clickedButton=CrefopaySlimjQuery(this);
            clickedButton.prop("disabled",true);
            console.log("[crefopay] Disabling remove userpaymentinstrument button and calling crefopay_remove_userpaymentinstrument-function");
            var userpaymentinstrumentId=clickedButton.attr("data-crefopay-paymentinstrumentid");

            var formParam=CrefopaySlimjQuery("<input name='crefopay_userpaymentinstrumentid'>");
            formParam.val(userpaymentinstrumentId);

            var f=CrefopaySlimjQuery(this).closest("form");
            f.find("input[name=fnc]").val("crefopay_remove_userpaymentinstrument");
            f.append(formParam);
            f.submit();

            e.preventDefault();
            return false;
        });

        CrefopaySlimjQuery(".js-crefopay-payment-group").click(function(e)
        {
            var radioInput=CrefopaySlimjQuery(this).find(".js-crefopay-paymentinstrumentid");
            var clickTarget=CrefopaySlimjQuery(e.target);
            if (!clickTarget.is(radioInput))
            {
                radioInput.prop('checked',true);
            }
        });
    });

})();